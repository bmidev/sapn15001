#!/bin/bash

rsync -rvihtz --exclude-from='./.rsync-filter' --rsh=ssh ./ $1:~/