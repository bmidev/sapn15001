<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nutella - Chief Ambassador - World Nutella Day - Febuary 5, 2016</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width"/>
<style type="text/css">
body {
	margin: 0;
	padding: 0 10px 0;
	background: #fff;
	font-size: 13px;
}
table {
	border-collapse: collapse;
}
td {
	font-family: arial, sans-serif;
	color: #333333;
}
a {
	color:#ed1f24;
}
 @import url(https://fonts.googleapis.com/css?family=Sue+Ellen+Francisco);
.container {
	width:100% !important;
	max-width:500px !important;
}
.img-responsive {
	width:100%;
	height:auto;
}
.img-a {
	max-width:451px;
}
.img-b {
	max-width:484px;
}
.footer {
	font-family:Arial, sans-serif;
	font-size:12px;
	color:#b4b4b5;
	padding:36px;
	text-align:justify
}
.main {
	font-family:Arial, sans-serif;
	font-size:16px;
	color:#000000;
	line-height:1.7;
	font-weight:700;
}
h2 {
	font-family:Arial, sans-serif;
	font-size:28px;
}
 @media only screen and (max-width: 580px) {
 body, table, td, p, a, li, blockquote {
 -webkit-text-size-adjust:none !important;
}
 table {
width: 100% !important;
}
}
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td scope="col" align="center"><table width="700" border="0" cellspacing="0" cellpadding="0" class="container">
        <tr>
          <th height="114" valign="top" scope="col"><img src="{{ url() }}/img/emails/top.jpg" alt="Approved" /></th>
        </tr>
        <tr>
          <td height="52" align="center" valign="top"><h2>Thanks for entering the <br />
              Chief Nutella Ambassador Contest</h2></td>
        </tr>
        <tr>
          <td align="center" class="main"><p> We’ve added your video to our site and wanted to send you a link <br />
              to share with your friends and family. Check it out right <a href="" target="_blank">here</a>!</p>
            <p>And don’t forget to come back to NutellaDay.com on November <br />
              16 when we’ll announce our five finalists!</p></td>
        </tr>
        <tr>
          <td height="36">&nbsp;</td>
        </tr>
        <tr>
          <td><table width="600" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" class="main"><p align="right">Good luck!</p>
                  <p align="right">-Nutella</p></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <p class="footer"><strong>NO PURCHASE NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT INCREASE YOUR CHANCES OF WINNING.</strong> Open only to legal residents of the 50 U.S. and D.C. who are at least 18 years of age with a valid email account as of the date of entry.  Void where prohibited.  Submission Period begins at 12:00:00 AM EDT on 9/21/2015 and ends at 11:59:59 PM EDT on 10/11/2015.  For complete details, including entry and prizing details, see <a href="{{ url() }}/rules">Official Rules</a> available at www.nutelladay.com. Sponsor: Ferrero U.S.A., Inc., 600 Cottontail Lane, Somerset, NJ 08873.</p></td>
  </tr>
</table>
</body>
</html>
