@extends('emails.layout')

@section('header') 
    <img src="<?php echo $message->embed(public_path().'/img/emails/rejected.png'); ?>" width="86" height="86" alt="Approved" class="img-responsive img-a" />
@stop

@section('title')
    <img src="<?php echo $message->embed(public_path().'/img/emails/rejecteded-title.png'); ?>" width="494" height="58" alt="Bummer. we couldn't approve your video :(" class="img-responsive img-b" />
@stop

@section('subheader')
@stop

@section('content')
  <p>It didn’t quite meet our <a href="{{ $link }}" target="_blank">criteria</a>. Please feel free to re-edit your submission or submit a new video. </p>
  <p>Come back on November 16 to meet our five finalists, follow their campaigns on social media, and vote for your favorite for Chief Nutella Ambassador.</p>
  <p>Thanks for sending your video!</p>
  <p>- Nutella</p>
@stop