@extends('emails.layout')

@section('title')
  Thanks for entering the <br />Chief Nutella Ambassador Contest
@stop

@section('content')
	<p> We’ve added your video to our site and wanted to send you a link <br />
        to share with your friends and family. Check it out right <a href="{{ $link }}" target="_blank">here</a>!</p>
    <p>And don’t forget to come back to NutellaDay.com on November <br />
              16 when we’ll announce our five finalists!</p>
@stop