@extends('layouts.admin')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading"><h4>Entries Summary</h4></div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
							<th>Total Entries</th>
							<th class="text-right">{{ number_format($totalEntries) }}</th>
						</thead>
						<tbody>
							<tr>
								<th>Approved Entries</th>
								<td class="text-right">{{ number_format($approvedEntries) }}</td>
							</tr>
							<tr>
								<th>Rejected Entries</th>
								<td class="text-right">{{ number_format($rejectedEntries) }}</td>
							</tr>
							<tr>
								<th>Pending Entries</th>
								<td class="text-right">{{ number_format($pendingEntries) }}</td>
							</tr>
						</tbody>
					</table>					
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading"><h4>Vote Summary</h4></div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
							<th>Name</th>
                            <th class="text-right">Total Votes</th>
                            <th class="text-right">Twitter Votes</th>
                            <th class="text-right">Website Votes</th>
						</thead>
						<tbody>
                        	<?php $total = 0; $tw_total=0; $ws_total=0; ?>
                        	@foreach($finalists as $entry)
                          	<?php $votes = $entry->votes()->count(); ?>
                            <?php $tw_votes = $entry->votes()->TwitterVote()->count(); ?>
                            <?php $ws_votes = $entry->votes()->WebsiteVote()->count(); ?>
							<tr>
								<th>{{ $entry->first_name.' '.$entry->last_name }}</th>
								<td class="text-right">{{ number_format($votes) }}</td>
                                <td class="text-right">{{ number_format($tw_votes) }}</td>
                                <td class="text-right">{{ number_format($ws_votes) }}</td>
							</tr>
                            <?php $total += intval($votes);  ?>
                            <?php $tw_total += intval($tw_votes);  ?>
                            <?php $ws_total += intval($ws_votes);  ?>
							@endforeach
                            <tr>
								<th><strong>Total</strong></th>
								<td class="text-right"><strong>{{ number_format($total) }}</strong></td>
                                <td class="text-right"><strong>{{ number_format($tw_total) }}</strong></td>
                                <td class="text-right"><strong>{{ number_format($ws_total) }}</strong></td>
							</tr>
						</tbody>
					</table>					
				</div>
			</div>
		</div>
	</div>
@stop


@section('scripts')
	<script>
	
		$(function() {
			<?php
			/*
			// Graph
			var data = [
			    @foreach( $entrySummary as $type => $count )
			      {
			      	value: {{ $m['total'] }},
			      	label: '{{ ucfirst($m['type']) }}',
			      	color: "{{ $chartColors['color'][$key] }}",
				    highlight: "{{ $chartColors['highlight'][$key] }}",
			      },
			    @endforeach
			];
			var options = {};
			
			var ctx = $('#methods-chart').get(0).getContext("2d");
			var donutChart = new Chart(ctx).Doughnut(data,options);
			
			$('#chart-legend').html(donutChart.generateLegend());
			*/
			?>
		})

	</script>
@stop
