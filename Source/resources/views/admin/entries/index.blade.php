@extends('layouts.admin')

@section('content')
	<div id="entries-list" class="media-posts panel panel-default">
		<div class="panel-heading">
			<?php
			$pending_css = '';
			$approve_css = '';
			$reject_css = '';
			$finalist_css = '';
			$winners_css = '';

			if($show == 'pending'){
				$pending_css = ' class=active';
			}elseif($show == 'approved'){
				$approve_css = ' class=active';
			}elseif($show == 'finalists'){
				$finalist_css = ' class=active';
			}elseif($show == 'winners'){
				$winners_css = ' class=active';
			}else{
				$reject_css = ' class=active';
			}
			?>
			<div style="float:left;"><b>Entries</b> - <a href="{{ url() }}/admin/entries"{{ $pending_css }}>Pending</a> | <a href="{{ url() }}/admin/entries?show=approved"{{ $approve_css }}>Approved</a> | <a href="{{ url() }}/admin/entries?show=rejected"{{ $reject_css }}>Rejected</a></div> <div style="float:right;"><a href="{{ url() }}/admin/entries?show=finalists"{{ $finalist_css }}>Finalists</a> | <a href="{{ url() }}/admin/entries?show=winners"{{ $winners_css }}>Winners</a></div><br style="clear:both;" />
		</div>
		
		<hr />

		<table class="table table-striped">
			<thead>
				<tr>
					<th class="col-xs-2" class="text-center">Created At</th>
                    <th class="col-xs-1" class="text-center">ID</th>
					<th class="col-xs-2" class="text-center">Video</th>
					<th class="col-xs-2">Entry By</th>
					<th class="col-xs-1">Category</th>
                    <th class="col-xs-1">Votes</th>
					<th class="col-xs-1 text-center">Finalist?</th>
                    <th class="col-xs-1 text-center">Winner?</th>
					<th class="col-xs-1 text-center">Status</th>
				</tr>
			</thead>
			<tbody>
				@foreach($entries as $entry)
					<tr>
						<td>{{ $entry->created_at }}</td>
                        <td>{{ $entry->id }}</td>
						<td class="admin-media">
							<a href="#" data-player="{{ $entry->video_url }}" data-id="{{ $entry->id }}" class="videoplay"><img class="submission" id="image_{{ $entry->id }}" data-id="{{ $entry->id }}" src="{{ $entry->video_thumbnail_url }}" alt="" /><img src="{{ url() }}/img/play_small.png" class="img-responsive play" alt="play"></a>
                            <div id="entry_data{{ $entry->id }}" class="entry_data_modal">
                            	<b>Name:</b> {{ $entry->first_name.' '.$entry->last_name }}<br />
                                <b>Email:</b> {{ $entry->email }}<br />
								<b>Type:</b> {{ $entry->type }}<br />
                                <b>Street 1:</b> {{ $entry->street1 }}<br />
								<b>Street 2:</b> {{ $entry->street2 }}<br />
                                <b>City:</b> {{ $entry->city }}<br />
                                <b>State:</b> {{ $entry->state }}<br />
                                <b>Zip:</b> {{ $entry->zip }}<br />
                                <b>Date of Birth:</b> {{ $entry->date_of_birth }}<br />
                            </div>
                            <a href="javascript:;" onclick="show_popup('entry_data{{ $entry->id }}')">Show Details</a>
						</td>
						<td>{{ $entry->first_name }} {{ $entry->last_name }}</td>
                        <td>{{ $entry->type }}</td>
                        <td>{{ $entry->votes()->count() }}</td>
                        <td class="commands finalist {{ $entry->finalist == 'yes' ? 'yes' : 'no' }}">
							<span class="btn btn-success approve" data-id="{{ $entry->id }}">Yes</span>
							<span class="btn btn-danger reject" data-id="{{ $entry->id }}">No</span>
						</td>
						<td class="commands winner {{ $entry->winner == 'yes' ? 'yes' : 'no' }}">
							<span class="btn btn-success approve" data-id="{{ $entry->id }}">Yes</span>
							<span class="btn btn-danger reject" data-id="{{ $entry->id }}">No</span>
						</td>
                        <td class="text-center">{{ $entry->status }}</td>
                        <td class="commands status {{ $entry->status }}">
                        	@if($entry->status == 'pending' || $entry->status == 'rejected')
							   <span class="btn btn-success approved" data-id="{{ $entry->id }}">Approve</span>
                            @endif
                            @if($entry->status == 'pending' || $entry->status == 'approved')
							<span class="btn btn-danger rejected" data-id="{{ $entry->id }}">Reject</span>
                            @endif
						</td>
					</tr>
				@endforeach
				@if(count($entries) == 0)
					<tr>
						<td colspan="50" class="no-matches">
							No Entries Found
						</td>
					</tr>
				@endif
			</tbody>
		</table>
		
		<div class="pagination-container">
			<?php echo $entries->appends(['show' => $show])->render(); ?>
		</div>
	</div>

    <!-- Modal -->
    <div class="modal_cont" id="modal-entry"> <a href="javascript:;" alt="Close" title="Close" class="close">X</a>
      <h2>&nbsp;</h2>
      <div class="modal_main">
        <div id="video_to_show" class="videoblock">
             
        </div>
        <form action="{{ url() }}/admin/entries/download_entry" method="post" target="_blank">
        	<input type="hidden" name="entry_id" value="" id="entryId" />
            <input type="hidden" name="entry_url" value="" id="entryUrl" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token" />
            <input type="submit" name="download" value="Download" />
        </form>
      </div>
    </div>

@stop
