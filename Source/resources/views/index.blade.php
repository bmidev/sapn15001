@include('partials.header')

@if ($page === 'pre-launch')
	@include('partials.headers.prelaunch-header')
@else
    @if ($page === 'entries' || $page === 'pre-finalists')
        @include('partials.headers.home-header')
        @include('partials.abbreviated_rules')
        @include('partials.looking')
    
        @if ($page === 'pre-finalists')
            @include('partials.dobform')
            @include('partials.entries')
        @else
            @include('partials.howtoenter')
            @include('partials.examples')
            @include('partials.dobform')
            @if ($page === 'entries')
                @include('partials.enterform')
            @endif
            @include('partials.entries')
        @endif
    @elseif ($page === 'finalists' || $page === 'pre-winner')
        @include('partials.headers.finalists-header')
        @include('partials.abbreviated_rules')
        @include('partials.looking')
    @elseif ($page === 'winner')
    	@if (isset($version) && $version == 1)
        	@include('partials.headers.winner-header-version1')
        @elseif (isset($country_code) && $country_code === 'US')
        	@include('partials.headers.winner-header')
            @if (isset($version) && $version != 1)
                @include('partials.about')
                @include('partials.winner')
                @include('partials.winners-statement')
                @include('partials.celebrate')
            @else
                @include('partials.winner-version1')
                @include('partials.about-version1')
            @endif
        @else
        	@include('partials.headers.winner-header-global')
            @if (isset($version) && $version != 1)
                @include('partials.about-global')
                @include('partials.winner-global')
                @include('partials.winners-statement')
                @include('partials.celebrate-global')
            @else
                @include('partials.winner-version1')
                @include('partials.about-version1')
            @endif
        @endif
    @endif
    
    @if ($page !== 'winner')
        @include('partials.prizes')
        @include('partials.timeline')
        @include('partials.about')
    @endif
    
    @if ($page === 'finalists' || $page === 'pre-winner' || ($page === 'winner' && isset($version) && $version == 1))
        @include('partials.entries')
    @endif
    
    @if ($page === 'winner')
        @include('partials.recipes')
        @if (isset($country_code) && $country_code === 'US' && isset($version) && $version == 2)
        	@include('partials.entries')
        @endif
    @else
        @include('partials.faq')
    @endif
@endif
@include('partials.footer')