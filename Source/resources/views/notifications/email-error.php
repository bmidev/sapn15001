@extends('emails.layout')

@section('title')
  There was a problem with the twitter cron
@stop

@section('content')
	<p>{{ $content }}</p>
@stop