<!-- winner Section -->
<section id="winner">
  <div class="container">
    <div class="row">
      <h2>meet your Chief Nutella Ambassador & host for World Nutella Day</h2>
      <div class="star"><h3>{{ $winner['first_name'].' '.$winner['last_name'] }}</h3></div><br />
      <div class="col-md-6 center">
          <p>{!! $winner['winner_biog'] !!}</p>
          <a href="#" class="videoplay" data-player="{{ $winner['winner_video'] }}"> <img src="{{ $winner['winner_image2'] }}" class="img-responsive img-centered" alt=""> </a>
      </div>
    </div>
  </div>
</section>