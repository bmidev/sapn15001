<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
@if ($is_tablet === true)
<meta name="viewport" content="width=980" id="myViewport">
@else
<meta name="viewport" content="width=device-width, initial-scale=0.5, user-scalable=no" id="myViewport">
@endif

<script>
@if ($is_tablet === true)
window.addEventListener("orientationchange", function() {											  
	//alert(window.orientation);
	
}, false);
@endif
/*if(window.innerWidth < 640){
	alert(window.innerWidth);
	document.getElementById('myViewport').setAttribute('content','width=640');
	alert(window.innerWidth);
}*/
var token = '{{$token}}';
var used = '{{ $used }}';
@if ($page == 'winner')
var recipes_used = '{{ implode("~",$recipes_used) }}';
@endif
var url = '{{ url() }}';
var servertime = '{{ time() }}';
var stage = '<?php
if(env('APP_ENV', 'production') != 'production'){
	echo '?stage='.$page;
}
?>';
</script>
<meta name="description" content="">
<meta name="keywords" value="" />
<meta name="author" content="">
<meta name="csrf-token" content="{{{ Session::token() }}}">
<title>World Nutella Day - February 5th</title>

<!-- Favicons -->
<!-- <link rel="apple-touch-icon" sizes="57x57" href="{{ url() }}/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="114x114" href="{{ url() }}/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="{{ url() }}/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="144x144" href="{{ url() }}/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="60x60" href="{{ url() }}/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="120x120" href="{{ url() }}/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="76x76" href="{{ url() }}/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="152x152" href="{{ url() }}/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="{{ url() }}/favicons/apple-touch-icon-180x180.png">

<link rel="shortcut icon" href="favicons/favicon.ico">
<link rel="icon" type="image/png" href="{{ url() }}/favicons/favicon-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="{{ url() }}/favicons/favicon-160x160.png" sizes="160x160">
<link rel="icon" type="image/png" href="{{ url() }}/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="{{ url() }}/favicons/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="{{ url() }}/favicons/favicon-32x32.png" sizes="32x32"> -->

<!-- Bootstrap Core CSS -->
<link href="{{ cdn('/css/bootstrap.min.css?v=20151113') }}" rel="stylesheet">
<!-- Bootstrap Select CSS -->
<link href="{{ cdn('/css/bootstrap-select.min.css?v=20151113') }}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{ url('/css/agency.css?v=201601281007') }}" rel="stylesheet">
<!-- Custom Fonts -->
<!--<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> <!-- contains social icon font -->
<link href='https://fonts.googleapis.com/css?family=Sue+Ellen+Francisco' rel='stylesheet' type='text/css'>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if IE 9]>
	
<![endif]-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if IE 9]>
<style>
    #enterform #dropzone_ie9, .dropzone.dz-browser-not-supported, .browse.desktopOnly, .uploadbtn.btn.mobileOnly.dz-clickable, #browsebtn {
    	display:none !important;
    }
    .videoarea {
    	text-align:center;
    }
    #hidden_iframe {
    	height:0px;
        border:0;
    }
</style>
<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66830997-1', 'auto');
  ga('send', 'pageview');
</script>
@if ($page != 'pre-launch')
	@if ($page == 'entries' || $page == 'pre-finalists')
    	<!-- Entries / Pre-finalists tracking code -->
        <!-- Facebook Conversion Code for Nutella Contest - KPV - Landing Page (1) -->
        <script id="fb_event">(function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
        }
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6039726825080', {'value':'0.00','currency':'USD'}]);
        </script>
        <noscript id="fb_event_noscript"><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6039726825080&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
        
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');
         
        fbq('init', '524871290980984');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=524871290980984&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
    @elseif ($page == 'finalists' || $page == 'pre-winner')
    	<!-- Finalists / Pre-Winner tracking code -->
    	<!-- Twitter Pixel Code -->
		<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
        <script type="text/javascript">twttr.conversion.trackPid('nts46', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
        <noscript>
        <img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nts46&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
        <img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nts46&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
        </noscript>
        
        <!-- Facebook Pixel Code -->
		<script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');
         
        fbq('init', '524871290980984');
        fbq('track', 'ViewContent');
        fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=524871290980984&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
    @else
    	<!-- Winner tracking code -->
        <script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
        <script type="text/javascript">twttr.conversion.trackPid('nts46', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
        <noscript>
        <img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nts46&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
        <img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nts46&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
        </noscript>
        
        <!-- Facebook Pixel Code -->
		<script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');
         
        fbq('init', '524871290980984');
        fbq('track', 'ViewContent');
        fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=524871290980984&ev=PageView&noscript=1"
        /></noscript>
    @endif
@endif
 
</head>
<body id="page-top" class="index">
@if ($page != 'pre-launch')
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Nutella_World Nutella Day_Landing page
URL of the webpage where the tag is expected to be placed: http://www.worldnutelladay.com
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 08/24/2015
-->
<script type="text/javascript" id="floodlight_event">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://4319270.fls.doubleclick.net/activityi;src=4319270;type=world0;cat=nutel0;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript id="floodlight_event_noscript">
<iframe src="https://4319270.fls.doubleclick.net/activityi;src=4319270;type=world0;cat=nutel0;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
@endif

<!--[if lt IE 9]>
<script type="text/javascript">
	alert('This site has not been designed for IE 8 or below. Please upgrade your browser if you wish to use it.');
</script>
<![endif]-->

<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll">
      @if ($page != 'pre-launch')
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar bar1"></span> <span class="icon-bar bar2"></span> <span class="icon-bar bar3"></span> </button>
      @endif
      @if ($page == 'winner' && isset($country_code) && $country_code !== 'US')
      <a class="navbar-brand page-scroll desktopOnly" href="#page-top"><img src="{{ url() }}/img/logo-winner-top.png" alt="World Nutella Day" class="logo"><span class="nutelladate">February 5th, 2016</span></a> <a class="navbar-brand page-scroll mobileOnly" href="#page-top"><img src="{{ url() }}/img/logo-winner-top-mobile.png" alt="World Nutella Day" class="logo"></a> 
      @else
      <a class="navbar-brand page-scroll desktopOnly" href="#page-top"><img src="{{ url() }}/img/logo.png" alt="World Nutella Day" class="logo"><span class="nutelladate">February 5, 2016</span></a> <a class="navbar-brand page-scroll mobileOnly" href="#page-top"><img src="{{ url() }}/img/logo_mobile.png" alt="World Nutella Day" class="logo"></a> 
      @endif
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
	@if ($page != 'pre-launch')
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      @if ($page == 'winner')
      <ul class="nav navbar-nav navbar-right">
        <li class="hidden"> <a href="#page-top"></a> </li>
        <li> <a class="page-scroll ucfirst" href="#about" data-tracker="nav,click,About">about World Nutella<sup>®</sup> Day</a> </li>
        <li> <a class="page-scroll ucfirst" data-tracker="nav,click,Meet Ambassador" href="#winner">meet the Chief Nutella<sup>®</sup> Ambassador</a> </li>
        <li> <a class="page-scroll" href="#recipes" data-tracker="nav,click,Recipes">inspiration</a> </li>
      </ul>
      @else
      <ul class="nav navbar-nav navbar-right">
        <li class="hidden"> <a href="#page-top"></a> </li>
        <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true" role="button" href="#" data-tracker="nav,click,Contest Details">Contest Details <span class="caret"></span></a>
          <ul class="dropdown-menu">
            @if ($page === 'entries')
            <li><a href="#howtoenter" class="page-scroll" data-tracker="nav,click,How Do I Enter">How Do I Enter?</a></li>
            @endif
            <li><a href="#prizes" class="page-scroll" data-tracker="nav,click,Prizes">Prizes</a></li>
            <li><a href="#timeline" class="page-scroll" data-tracker="nav,click,Timeline">Contest Timeline</a></li>
            <li><a href="#faq" class="page-scroll" data-tracker="nav,click,FAQ">Frequently Asked Questions</a></li>
          </ul>
        </li>
        <li> <a class="page-scroll" data-tracker="nav,click,Fan Video Submissions" href="#entries">Fan Video Submissions</a> </li>
        <li> <a class="ruleslnk" href="#" data-tracker="nav,click,Official Rules">Official Rules</a> </li>
        <li> <a class="page-scroll" href="#about" data-tracker="nav,click,About">About World Nutella Day</a> </li>
        @if ($page === 'entries')
        <li> <a class="page-scroll btn" href="#enter" data-tracker="nav,click,Enter Contest">Enter Contest</a> </li>
        @endif
      </ul>
      @endif
    </div>
    @endif
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>