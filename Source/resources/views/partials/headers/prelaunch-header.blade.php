<!-- Header -->
<header class="prelaunch">
  <div class="container">
    <div class="intro-text">
      <h1 class="intro-lead-in">Come back Monday, September 21st 2015 at 12:00 AM EDT</h1>
   	  <h2 class="intro-heading">that’s when we kick off the <br />Chief Nutella Ambassador Contest!</h2>
    </div>
  </div>
</header>