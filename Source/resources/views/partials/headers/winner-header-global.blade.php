<!-- Header -->

<header class="winner global">
  <div class="container">
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2"></div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8"> 
        <!-- sm still 8 to keep size even if not grid of 12 -->
        <h2>THE 10TH ANNIVERSARY</h2>
        <img src="{{ url() }}/img/global-wnd-global.jpg" class="img-responsive img-centered desktopOnly winlogo">
        <img src="{{ url() }}/img/global-wnd-mobile-global.jpg" class="img-responsive img-centered mobileOnly winlogo">
        <h3>February 5th, 2016</h3>
        <p>Nutella<sup>®</sup> lovers unite for just one day.<br />World Nutella<sup>®</sup> Day</p>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
        <div class="social-buttons"> <img src="{{ url() }}/img/winner-cele-with-us-global.png" class="img-responsive circle desktopOnly">
          <div class="sharerow"><h4 class="mobileOnly">celebrate with us on official<br />World Nutella<sup>®</sup> Day channels</h4><a target="_blank" data-tracker="footer,click,Facebook" href="https://www.facebook.com/worldnutelladay" class="fbr"> <i class="fa fa-facebook"></i> </a><a target="_blank" data-tracker="footer,click,Twitter" href="https://twitter.com/NutellaDay"> <i class="fa fa-twitter"></i> </a><h4 class="hashtag mobileOnly">#worldnutelladay</h4> </div>
        </div>
      </div>
  </div>
</header>
