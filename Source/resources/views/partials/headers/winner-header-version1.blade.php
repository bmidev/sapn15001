<!-- Header -->

<header class="winner version1">
  <div class="container">
  <div class="col-xs-12 col-sm-2 col-md-3"></div>
  <div class="col-xs-12 col-sm-8 col-md-6"> <img src="{{ url() }}/img/logo-winner.png" class="img-responsive img-centered winlogo">
    <!-- sm still 8 to keep size even if not grid of 12 -->
    <h2>THE 10TH ANNIVERSARY</h2>
    <h3>February 5, 2016</h3>
    <p>Nutella lovers unite for one delicious day a year</p>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-3">
    <div class="social-buttons"> <img src="{{ url() }}/img/winner-cele-with-us-version1.png" class="img-responsive circle desktopOnly">
      <div class="sharerow"><h4 class="mobileOnly">celebrate with us on</h4><a target="_blank" data-tracker="footer,click,Facebook" href="https://www.facebook.com/nutellausa" class="fbr"> <i class="fa fa-facebook"></i> </a><a target="_blank" data-tracker="footer,click,Twitter" href="https://twitter.com/NutellaUSA"> <i class="fa fa-twitter"></i> </a> </div>
    </div>
  </div>
</header>
