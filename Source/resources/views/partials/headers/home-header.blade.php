<!-- Header -->
<header @if ($page == 'pre-finalists') {{ 'class=pre-finalists' }} @endif>
  <div class="container">
    <div class="intro-text {{ $page }}">
      <h1 class="intro-lead-in">@if ($page == 'pre-finalists') Who do you want to lead @else You could be the face of @endif</h1>
      <h1 class="intro-logo">World nutella day</h1>
   @if ($page == 'pre-finalists')
   	<span class="qmark">?</span>
   	  <h2 class="intro-heading">The Chief Nutella Ambassador Contest.</h2>
      <p class="mobileOnly thanks">Thanks for your submissions! The entry period is now over. <br /><br /><b>Check back on November 16th when you’ll get to meet our 5 finalists, learn how to follow their social media campaigns and have the chance to vote for your favorite. In the meantime, feel free to check out the fan videos below.</b></p>
      <div class="embed-responsive-custom"><a href="#" onclick="show_popup('modal-howitworks')"><img src="{{ url() }}/img/placeholders/how_the_content_works-newest.jpg" class="img-responsive img-centered desktopOnly"><img src="{{ url() }}/img/placeholders/how_the_content_works-new-mobile.jpg" class="img-responsive img-centered mobileOnly"></a></div>
      <p class="desktopOnly thanks">Thanks for your submissions! The entry period is now over. <br /><br /><b>Check back on November 16th when you’ll get to meet our 5 finalists, learn how to follow their social media campaigns and have the chance to vote for your favorite. In the meantime, feel free to check out the fan videos below.</b></p>
   @else
      <h2 class="intro-heading">We want YOU to be our Chief Nutella Ambassador</h2>
      <p class="mobileOnly"><b>You could become a social media sensation</b>, bring together millions of fans around the world, and take the trip of a lifetime to NYC! Spread the word with <span class="hashtag">#NutellaAmbContest</span></p>
      <div class="embed-responsive-custom"><a href="#" onclick="show_popup('modal-howitworks')"><img src="{{ url() }}/img/placeholders/how_the_content_works-newest.jpg" class="img-responsive img-centered desktopOnly"><img src="{{ url() }}/img/placeholders/how_the_content_works-new-mobile.jpg" class="img-responsive img-centered mobileOnly"></a></div>
      <p class="desktopOnly"><b>You could become a social media sensation</b>, bring together millions of fans around the world, and take the trip of a lifetime to NYC! Spread the word with <span class="hashtag">#NutellaAmbContest</span></p>
      <a href="#howtoenter" class="page-scroll btn btn-xl" data-tracker="button,click,Get Started">Get Started</a>
   @endif
    </div>
  </div>
  @if ($page == 'entries')
  <div class="notice"><img src="{{ url() }}/img/banners/get_a_t-shirt.png" class="img-responsive" usemap="#Map" /><map name="Map" id="Map"><area shape="poly" coords="169,231,99,215,94,227,162,246" href="javascript:;" /></map></div>
  @endif
</header>