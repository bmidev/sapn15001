<!-- Header -->
<?php
$css = $page == 'pre-winner' ? ' prewinner' : '';
$prefix = $page == 'pre-winner' ? 'Follow' : 'Follow'; //Was see for pre-winner
?>
<header class="finalists{{ $css }}">
  <div class="container">
    <div class="intro-text">
   @if ($page == 'pre-winner')
      <h1 class="intro-lead-in">Who will lead <span class="intro-logo">World nutella day</span><span class="qmark desktopOnly inlineblock">?</span></h1>
   	  <h2 class="intro-heading">The Chief Nutella Ambassador Contest!</h2>
      <h3 class="intro-subheading upper">The voting period has ended, thanks for your votes</h3>
      <p class="desktopOnly">Stay tuned for our winner announcement on or about January 11, 2016.<br /><br />Until then, check out all the fan videos on this page and get inspired for World Nutella Day-it’s right around the corner!</p>
   @else
      <h1 class="intro-lead-in">Who do you want to lead <span class="intro-logo">World nutella day</span><span class="qmark">?</span></h1>
   	  <h2 class="intro-heading">Vote to help us choose the Chief Nutella Ambassador!</h2>
      <p class="desktopOnly">Get to know the five finalists below, follow their campaigns on social media, and let us know who you think should host World Nutella Day 2016.</p>
   @endif
    </div>
    <div class="finalistsarea">
        <div class="col-xs-6 col-md-4 col-lg-5ths videointo mobileOnly tabletOnly"> <img src="{{ url() }}/img/entry-spacer.gif" class="img-responsive img-center" alt=""> <div class="finalisth2">
        @if ($page == 'pre-winner')
        <h3><strong>We'll announce our winner on or about January 11, 2016!</strong> Until then, check out all the fan videos on this page and get inspired for World Nutella Day – it’s right around the corner!</h3>
        @else
        <h2>meet the finalists!</h2>
        <ul>
      		<li>&#8226; watch their videos</li>
            <li>&#8226; follow their campaigns</li>
            <li>&#8226; vote for your favorite!</li>
        </ul>
        @endif
        </div></div>
	 @foreach ($finalists as $entry)
 	   <div class="col-xs-6 col-md-4 col-lg-5ths video"> <a href="#" data-player="{{ $entry['video_url'] }}" data-id="{{ $entry['id'] }}" data-firstname="{{ $entry['first_name'] }}" data-tw-url="{{ $entry['twitter_url'] }}" data-fb-url="{{ $entry['facebook_url'] }}" class="videoplay voteplay"><img src="{{ $entry['video_thumbnail_url'] }}" class="img-responsive img-center" alt="{{ 'Entry By '.$entry['first_name'].' '.$entry['last_name'] }}"><h3 class="videoname mobileOnly">{{ $entry['first_name'] }}</h3><img src="{{ url() }}/img/play_small.png" class="img-responsive play" alt="play"></a>
       	@if ($page == 'finalists')
        <?php $hashtag = 'Vote'.preg_replace("/[^\da-z]/i","",ucfirst($entry['first_name'])).'NutellaAmbContest'; ?>
       	<a href="#" data-vote-for="{{ $entry['id'] }}" class="vote btn desktopOnly inlineblock">Vote for {{ $entry['first_name'] }}</a><p class="desktopOnly">also vote on Twitter with <br /><a href="https://twitter.com/intent/tweet?text={{ urlencode(url().'/e/'.$entry['id'].'/'.time().'/') }}&hashtags={{ urlencode($hashtag) }}" data-firstname="{{ $entry['first_name'] }}" data-id="{{ $entry['id'] }}" class="tweet">#Vote{{ preg_replace("/[^\da-z]/i","",ucfirst($entry['first_name'])) }}NutellaAmbContest</a></p>
        <h4 class="desktopOnly">{{ $prefix }} {{ $entry['first_name'].(substr($entry['first_name'],(strlen($entry['first_name'])-1),1)=='s'?"'":"'s") }} campaign</h4><a href="{{ $entry['facebook_url'] }}" target="_blank" class="fb_icon desktopOnly inlineblock"></a><a href="{{ $entry['twitter_url'] }}" target="_blank" class="tw_icon desktopOnly inlineblock"></a>
        @endif
        @if ($page == 'pre-winner')
        <h3 class="videoname desktopOnly">{{ $entry['first_name'] }}</h3>
        @endif
       </div>
     @endforeach
     <br class="clr" />
     <p class="desktopOnly cap">Social Media Campaign and Voting begins at 9:00:00 am EST on 11/16/15 and ends at 5:00:00 pm EST on 11/29/15 (“Campaign Period”). During the Campaign Period, Eligible Voters can visit nutelladay.com to Vote or cast their Vote by sending a Tweet with the hashtag displayed on nutelladay.com next to the Finalist for whom they wish to Vote. <strong class="bold">Limit five (5) Votes via nutelladay.com per person/per IP address/per day and one (1) Vote via Twitter per person/Twitter handle per day.</strong> For complete details, including Voter eligibility requirements, see Official Rules available <a href="javascript:;" class="ruleslnk">here</a>.</p>
  </div>
  </div>
</header>