<!-- Header -->

<header class="winner">
  <div class="container">
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2"></div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8"> 
        <!-- sm still 8 to keep size even if not grid of 12 -->
        <h2>THE 10TH ANNIVERSARY</h2>
        <img src="{{ url() }}/img/global-wnd.jpg" class="img-responsive img-centered desktopOnly winlogo">
        <img src="{{ url() }}/img/global-wnd-mobile.jpg" class="img-responsive img-centered mobileOnly winlogo">
        <h3>February 5th, 2016</h3>
        <p>Nutella lovers unite for one delicious day a year</p>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
        <div class="social-buttons"> <img src="{{ url() }}/img/winner-cele-with-us.png" class="img-responsive circle desktopOnly">
          <div class="sharerow"><h4 class="mobileOnly">celebrate with us on</h4><a target="_blank" data-tracker="footer,click,Facebook" href="https://www.facebook.com/nutellausa" class="fbr"> <i class="fa fa-facebook"></i> </a><a target="_blank" data-tracker="footer,click,Twitter" href="https://twitter.com/NutellaUSA"> <i class="fa fa-twitter"></i> </a><h4 class="hashtag mobileOnly">#WorldNutellaDay</h4> </div>
        </div>
      </div>
  </div>
</header>