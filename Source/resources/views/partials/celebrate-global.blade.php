<!-- Recipes Section -->
<section id="celebrate" class="global">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">10 easy ways to celebrate the big day</h2>
      </div>
    </div>
    <div class="row celebratebox">
		<div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb1.png" class="img-responsive img-centered desktopOnly" alt="Make a recipe with Nutella"><img src="{{ url() }}/img/celeb1-mobile.png" class="img-responsive img-centered mobileOnly" alt="Make a recipe with Nutella"><h3 class="subsection-heading">Make a recipe<br />with Nutella<sup>&reg;</sup></h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb2.png" class="img-responsive img-centered desktopOnly" alt="Write a song or poem about Nutella"><img src="{{ url() }}/img/celeb2-mobile.png" class="img-responsive img-centered mobileOnly" alt="Write a song or poem about Nutella"><h3 class="subsection-heading">Write a song or poem<br />about Nutella<sup>&reg;</sup></h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb3.png" class="img-responsive img-centered desktopOnly" alt="Enjoy Nutella somewhere interesting"><img src="{{ url() }}/img/celeb3-mobile.png" class="img-responsive img-centered mobileOnly" alt="Enjoy Nutella somewhere interesting"><h3 class="subsection-heading">Enjoy Nutella<sup>&reg;</sup><br />somewhere interesting</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb4.png" class="img-responsive img-centered desktopOnly" alt="Strike a pose with Nutella"><img src="{{ url() }}/img/celeb4-mobile.png" class="img-responsive img-centered mobileOnly" alt="Strike a pose with Nutella"><h3 class="subsection-heading">Strike a pose<br />with Nutella<sup>&reg;</sup></h3> </div>
        
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb5.png" class="img-responsive img-centered desktopOnly" alt="Create some Nutella inspired art"><img src="{{ url() }}/img/celeb5-mobile.png" class="img-responsive img-centered mobileOnly" alt="Create some Nutella inspired art"><h3 class="subsection-heading">Create some Nutella<sup>&reg;</sup><br />inspired art</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb6.png" class="img-responsive img-centered desktopOnly" alt="Do an original flavor pairing"><img src="{{ url() }}/img/celeb6-mobile.png" class="img-responsive img-centered mobileOnly" alt="Do an original flavor pairing"><h3 class="subsection-heading">Do an original<br />flavor pairing</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb7.png" class="img-responsive img-centered desktopOnly" alt="Relive your first Nutella experience"><img src="{{ url() }}/img/celeb7-mobile.png" class="img-responsive img-centered mobileOnly" alt="Relive your first Nutella experience"><h3 class="subsection-heading">Relive your first<br />Nutella<sup>&reg;</sup> experience</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb8.png" class="img-responsive img-centered desktopOnly" alt="Give Nutella to people you love"><img src="{{ url() }}/img/celeb8-mobile.png" class="img-responsive img-centered mobileOnly" alt="Give Nutella to people you love"><h3 class="subsection-heading">Give Nutella<sup>&reg;</sup> to<br />people you love</h3> </div>
        
        <div class="col-xs-6 col-sm-3 celeb div-centered"> <img src="{{ url() }}/img/celeb9.png" class="img-responsive img-centered desktopOnly" alt="Introduce someone to Nutella for the first time"><img src="{{ url() }}/img/celeb9-mobile.png" class="img-responsive img-centered mobileOnly" alt="Introduce someone to Nutella for the first time"><h3 class="subsection-heading">Introduce someone to<br />Nutella<sup>&reg;</sup> for the first time</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb div-centered"> <img src="{{ url() }}/img/celeb10.png" class="img-responsive img-centered desktopOnly" alt="Host a delicious party in honor of Nutella"><img src="{{ url() }}/img/celeb10-mobile.png" class="img-responsive img-centered mobileOnly" alt="Host a delicious party in honor of Nutella"><h3 class="subsection-heading downarrow">Host a delicious party<br />in honor of Nutella<sup>&reg;</sup><br /><a href="javascript:;" class="learnhow">learn how</a></h3> </div>
        <br class="clr" />
    </div>
    <div class="row">
      <div class="col-lg-12 text-center celebtweet">
        @if (isset($country_code) && $country_code !== 'US')
        <h3 class="section-subheading">Most importantly, SHARE your love with everyone in the world!<br>Here are some suggestions...</h3>
        @else
        <h3 class="section-subheading">Most importantly, SHARE your love with everyone in the world!<br>Here are some suggestions</h3>
        @endif
        <div class="celebpoints">
        	<table cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top" width="30"><p><span class="num">1.</span></p></td>
                    <td valign="top"><p> On Twitter: tweet your love for Nutella<sup>®</sup> adding in your message <b>@nutelladay</b> and <b>#worldnutelladay</b>. Become a follower of the <a href=" https://twitter.com/nutelladay" target="_blank">@nutelladay Twitter account</a>.</p></td>
                </tr>
                <tr>
                	<td valign="top"><p><span class="num">2</span></p></td>
                    <td valign="top"><p>On Facebook: update your Facebook status and <b>link it to the website nutelladay.com</b>. To follow the day’s celebration, like the <a href="https://www.facebook.com/WorldNutellaDay" target="_blank">World Nutella Day official Facebook page</a>.</p></td>
                </tr>
                <tr>
                	<td valign="top"><p><span class="num">3</span></p></td>
                    <td valign="top"><p>On Instagram and Pinterest: <b>tag your photos with #worldnutelladay</b></p></td>
                </tr>
                <tr>
                	<td valign="top"><p><span class="num">4</span></p></td>
                    <td valign="top"><p>No matter what kind of celebration you do, just share your pictures and videos on February 5th 2016. Include a <b>link to nutelladay.com as well as the World Nutella Day logo</b> that you can download here below!</p></td>
                </tr>
            </table>
        </div>
        <!-- <h3 class="subsection-subheading"><a href="https://www.facebook.com/WorldNutellaDay" target="_blank">facebook.com/WorldNutellaDay</a></h3> -->
        <h3 class="subsection-subheading">Use the World Nutella<sup>&reg;</sup> Day badge in your posts!</h3>
        <p class="celebreset">Download it below by right-clicking the badge and selecting “save image” from the menu.</p>
        <img class="img-responsive img-centered" src="{{ url() }}/img/global-wnd-share.jpg" id="download_badge">
        <p class="celebreset celebfootnote">The World Nutella Day logo and trademark are the exclusive property of Ferrero. Download and use are permitted for personal and non-commercial purposes only.<br /><br class="mobileOnly auto" />Such permission cannot be interpreted as granting, explicitly or implicitly, third parties the license to use Ferrero trademarks.</p>
      </div>
    </div>
  </div>
</section>