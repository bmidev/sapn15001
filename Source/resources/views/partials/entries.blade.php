<!-- Fan Video Submission Section -->
<?php
$css = $page === 'finalists' || $page === 'pre-winner' ? ' class=finalists' : '';
$css = $page === 'winner' ? ' class=winner' : '';
$gallery_version = $is_mobile ? ' mobile' : '';
$nogallery_version = $is_mobile || $is_tablet ? ' mobile' : '';
?>
<section id="entries"{{ $css }}>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
      	@if ($page === 'winner')
        <h2 class="section-heading">need some ideas for World Nutella Day?</h2>
            @if ($version == 1)
            <p class="section-subheading">Check out what our fans came up with for our Chief Nutella Ambassador contest.</p>
            @else
            <p class="section-subheading version2">Fans shared some great ideas for how they’d lead the day for our Chief Nutella Ambassador contest.</p>
            @endif
        @else
        <h2 class="section-heading">fan video submissions</h2>
        <h3 class="section-subheading">check out what your fellow fans have been doing!</h3>
        @endif
      </div>
    </div>
    <div class="row">
      <input type="text" name="data-video-type" value="Friend,Artist,Adventurer,Gourmet,Purist" id="data-video-type" style="display:none;" />
      @if ($entries->count()>0)
      <div class="col-lg-12 text-center filterlist desktopOnly">
        <div data-type="Friend" class="filterbox">Friend <span class="rmfilter"></span></div>
        <div data-type="Artist" class="filterbox">Artist <span class="rmfilter"></span></div>
        <div data-type="Adventurer" class="filterbox">Adventurer <span class="rmfilter"></span></div>
        <div data-type="Gourmet" class="filterbox">Gourmet <span class="rmfilter"></span></div>
        <div data-type="Purist" class="filterbox">Purist <span class="rmfilter"></span></div>
      </div>
          @if ($page !== 'winner' || ($page === 'winner' && $version == 1))
          <div class="col-lg-12 text-center filterlist mobileOnly">
              <select name="filter" id="selectfilter" required class="selectpicker">
                <option value="">filter</option>
                <option value="Friend">friend entries</option>
                <option value="Artist">artist entries</option>
                <option value="Adventurer">adventurer entries</option>
                <option value="Gourmet">gourmet entries</option>
                <option value="Purist">purist entries</option>
              </select>
          </div>
          @endif
      @endif
    </div>
    <div class="row gallery{{ $gallery_version }}">
		@if ($entries->count()>0)
			@foreach ($entries as $entry)
        		<?php $json = json_decode($entry,1); ?>
				<div class="col-xs-6 col-md-4 col-lg-5ths video"> <a href="#" data-player="{{ $json['video_url'] }}" class="videoplay"><img src="{{ $json['video_thumbnail_url'] }}" class="img-responsive" alt="{{ 'Entry By '.$json['first_name'] }}"><img src="{{ url() }}/img/play_small.png" class="img-responsive play" alt="play"></a> </div>
			@endforeach
		@else
            @for ($i = 0; $i < 10; $i++)
                <div class="col-xs-6 col-md-4 col-lg-5ths novideo"><img src="{{ url() }}/img/novideo.png" class="img-responsive" alt=""></div>
            @endfor
            <div class="novideobox{{ $nogallery_version }}"><div class="novideobanner"><p>No videos yet. Upload yours and you could be first!</p></div></div>
		@endif
        <br class="clr" />
    </div>
    <div class="row">
      <div class="col-lg-12 text-center">
      @if ($more === true)
        <button class="btn morebtn" type="button" id="morebtn" title="Submit">load more</button>
      @endif
      </div>
    </div>
  </div>
</section>