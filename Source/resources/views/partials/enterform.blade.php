<!-- Enter Form Section -->
<section id="enterform">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <img src="{{ url() }}/img/point1-mobile.png" class="img-responsive img-centered mobileOnly">
        <h2 class="section-heading point1">which  personality  shows how you’d lead  World Nutella Day?</h2>
        <!-- <p>The personality that you pick should help inspire your video.  Even though you need to pick one, it's not part of the judging and it won't affect your score.</p> -->
        <p id="erroranchor">Your video must be inspired by this personality, but it won’t be part of the judging and won’t affect your score.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <form name="dropzone-uploader" action="/" id="dropzone-uploader" method="post" novalidate>
          <p id="general_error_msg"><strong>We need a little more info</strong><br />Please fill in the red fields below.</p>
          <div class="row checkboxrow">
            <div class="nowrap checkboxes sm-50">
               <input type="radio" name="entry_type" value="friend" class="radio" /> <img src="{{ url() }}/img/icheck-friend.png" class="img-responsive" alt="">
            </div>
            <div class="nowrap checkboxes sm-50">
               <input type="radio" name="entry_type" value="artist" class="radio" /> <img src="{{ url() }}/img/icheck-artist.png" class="img-responsive" alt="">
            </div>
            <div class="nowrap checkboxes sm-50">
               <input type="radio" name="entry_type" value="adventurer" class="radio" /> <img src="{{ url() }}/img/icheck-adventurer.png" class="img-responsive" alt="">
            </div>
            <div class="nowrap checkboxes sm-50">
               <input type="radio" name="entry_type" value="gourmet" class="radio" /> <img src="{{ url() }}/img/icheck-gourmet.png" class="img-responsive" alt="">
            </div>
            <div class="nowrap checkboxes mobile-center sm-50">
               <input type="radio" name="entry_type" value="purist" class="radio" /> <img src="{{ url() }}/img/icheck-purist.png" class="img-responsive" alt="">
            </div>
            <p class="help-block text-danger" id="typeErr"></p>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="videoarea">
                <img src="{{ url() }}/img/point2-mobile.png" class="img-responsive img-centered point2img mobileOnly">
                <h2 class="section-heading point2">upload your video</h2>
                <p class="uploadintro">Videos must be 60 seconds or shorter and smaller than 20 MB. We can’t wait to see what you’ve made!</p>
                <p class="uploadtxt">Additional Video Submission Guidelines and Requirements apply.  See <a href="javascript:;" class="ruleslnk">Official Rules</a> for details.</p>
				<div class="dropzone">
                   <div id="dropzone">
                      <div class="dz-message">
						drag your video here
 					  </div>
                   </div>
                </div>
                <div class="file_preview"></div>
                <div class="dz-progress">
                    <div class="dz-upload"></div>
                </div>
                <div class="form-group">
                	<p class="help-block text-danger" id="fileErr"></p>
                </div>
                <p id="browsebtn" class="browse desktopOnly">Or <b class="orbrowse" style="cursor:pointer;"><u>Browse</u></b></p>
                <button type="button" class="uploadbtn btn mobileOnly">Upload</button>
                <input type="hidden" id="fileInput_hidden" name="fileInput_hidden" value="" />
			  </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <img src="{{ url() }}/img/point3-mobile.png" class="img-responsive img-centered mobileOnly">
              <h2 class="section-heading point3">provide your information</h2>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="first name" id="first_name" name="first_name" required data-validation-required-message="You must enter a first name.">
                <p class="help-block text-danger" id="firstnameErr">{{ $errors->first('first_name') }}</p>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="last name" id="last_name" name="last_name" required data-validation-required-message="You must enter a last name." />
                <p class="help-block text-danger" id="lastnameErr">{{ $errors->first('last_name') }}</p>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" placeholder="email" id="email" name="email" required data-validation-required-message="You must enter your email." />
                <p class="help-block text-danger" id="emailErr">{{ $errors->first('email') }}</p>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" placeholder="confirm email" id="confirm_email" name="confirm_email" required data-validation-required-message="You must confirm your email." />
                <p class="help-block text-danger" id="confirm-emailErr">{{ $errors->first('confirm_email') }}</p>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="street address 1 (no P.O. Boxes)" id="street1" name="street1" required data-validation-required-message="You must enter the first line of your address." />
                <p class="help-block text-danger" id="street1Err">{{ $errors->first('street1') }}</p>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="street address 2" id="street2" name="street2" />
                <p class="help-block text-danger" id="street2Err">{{ $errors->first('street2') }}</p>
              </div>

              <div class="form-group">
                <div class="col-lg-8">
                  <div class="form-group">
                   <input type="text" class="form-control mediumfld" placeholder="city" id="city" name="city" required data-validation-required-message="You must enter a city." />
                   <p class="help-block text-danger" id="cityErr">{{ $errors->first('city') }}</p>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                   <select name="state" id="state" class="selectpicker">
                     <option value="">state</option>
                     <?php
					 $states = Config::get('promotion.valid_states');
					 foreach($states as $row_no => $state){
                        echo '<option value="'.$state.'">'.$state.'</option>';
                     }
					 ?>
                   </select>
                   <p class="help-block text-danger" id="stateErr">{{ $errors->first('state') }}</p>
                 </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-8 mobile-center">
                   <input type="text" class="form-control mediumfld" placeholder="zip code" id="zip" name="zip" required data-validation-required-message="You must enter a zip." />
                   <p class="help-block text-danger" id="zipErr">{{ $errors->first('zip') }}</p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              	<p class="checkboxarea"><input type="checkbox" name="agreeTo" id="agreeTo" value="true" class="checkbox" /> I have read and agree to the <a href="#" class="ruleslnk">Official Rules</a></p>
                <p class="help-block text-danger" id="agreeToErr"></p>
                <p class="checkboxarea"><input type="checkbox" name="understand" id="understand" value="true" class="checkbox" /> I understand that my information will be used as described in the <a href="#" class="ruleslnk">Official Rules</a> and Sponsor's <a href="http://www.ferrerousa.com/ferrero-usa/privacy-policy/terms-of-use" target="_blank">Privacy Policy</a>.</p>
                <p class="help-block text-danger" id="understandErr">{{ $errors->first('agreeTo') }}</p>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12 text-center">
              <div id="success"></div>
              <button type="submit" class="btn btn-xl">submit</button>
            </div>
          </div>
        </form>
        <form name="ie9_alt_upload" action="{{ url() }}/api/file_upload" id="ie9_alt_upload" target="hidden_iframe" method="post" enctype="multipart/form-data">
        	<div class="alt_upload">
				<input type="file" id="fileInput" name="fileInput" accept="video/*" onpropertychange="if(window.event.propertyName.length>0){}" />
                <input type="hidden" name="_token" id="token" value="{{ $token }}" />
                <input type="hidden" name="ie9" value="true" />
                <input type="submit" name="altsubmitbtn" id="altsubmitbtn" value="" />
			</div>
        </form>
      </div>
    </div>
  </div>
</section>