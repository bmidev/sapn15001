<!-- Prizes Section -->
<section id="prizes">
  <div class="bg-city">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-3 col-lg-3 left_img desktopOnly">@if ($page === 'entries') <a href="javascript:;" class="ruleslnk"><img src="{{ url() }}/img/free-tshirt-left.png" class="img-responsive img-centered" alt="" /></a> @endif</div>
        <div class="col-sm-12 col-md-6 col-lg-6 text-center">
          <h4 class="section-heading">grand prize WINNER</h4>
          <h3 class="section-subheading">the honor of being</h3>
          <h2 class="section-thirdheading">Chief Nutella Ambassador</h2>
          <h3 class="section-fourthheading">the face of fans on social media for World Nutella Day!</h3>
          <p class="white trip-copy mobile-center">A 7-day/6-night trip to New York City (January 3-9, 2016) for you and a guest</p>
          <ul>
            <li>&#8226; Help us create fun content and star in videos for World Nutella Day</li>
            <li>&#8226; Enjoy once-in-a-lifetime NYC experiences</li>
          </ul>
          <p class="unbold">See <a href="javascript:;" class="ruleslnk prizelnk unbold">Official Rules</a> for additional information, including prize details</p>
          <p class="desktopOnly"><span class="white">A one-year supply of Nutella</span> <br />
            <span class="unbold">Awarded in the form of 5 cases of twelve (12) 13 oz. jars</span></p>
          <h4 class="runnerup">4 RUNNERS-UP</h4>
          <p class="mobilesize"><span class="white">Each runner-up will receive a one-year supply of Nutella</span> <br />
            <span class="unbold">Awarded in the form of 5 cases of twelve (12) 13 oz. jars</span></p>
            <img src="{{ url() }}/img/free-tshirt-left-mobile.png" class="img-responsive mobileOnly img-centered mobileOnly" alt="" usemap="#Map10" /><map name="Map10" id="Map10"><area shape="rect" coords="292,236,417,255" href="#" /></map>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-3 right_img desktopOnly"><img src="{{ url() }}/img/jar_right.jpg" alt="Nutella" class="img-responsive" alt="" /></div>
      </div>
    </div>
  </div>
</section>