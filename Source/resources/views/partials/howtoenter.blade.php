<!-- How to Enter Grid Section -->
<section id="howtoenter">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">in a 60-second or shorter video</h2>
        <h3 class="section-subheading">tell us how you'd lead World Nutella Day</h3>
        <h4 class="enterbefore mobileOnly">Enter before October 11</h4>
        <p class="section-intro">Every leader has their own distinct personality. Below, pick a personality that shows how you’d excite and unite our fans.<br /> Then make a video inspired by that personality. (The personality you pick won't play a role in judging or scoring).</p>
      </div>
    </div>
    <div class="row mobilenomargin">
      <div id="myCarousel" class="carousel slide mobileOnly" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
          <li data-target="#myCarousel" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active"> <img class="first-slide" src="{{ url() }}/img/carousel/jar_friend.jpg" alt="First slide">
            <div class="container">
              <div class="carousel-caption">
                <p><b>A BFF through and through, you believe happiness is always best when it’s shared.</b> You’d bring fans together with your kindness and generosity.</p>
              </div>
            </div>
          </div>
          <div class="item"> <img class="third-slide" src="{{ url() }}/img/carousel/jar_artist.jpg" alt="Second slide">
            <div class="container">
              <div class="carousel-caption">
                <p><b>You see beauty in everything and channel it into creative, jaw-dropping masterpieces.</b> You’d inspire fans everywhere to really express themselves.</p>
              </div>
            </div>
          </div>
          <div class="item"> <img class="fourth-slide" src="{{ url() }}/img/carousel/jar_adventurer.jpg" alt="Third slide">
            <div class="container">
              <div class="carousel-caption">
                <p><b>You live life off the beaten path. Nothing’s too daring or inventive for you.</b> Your leadership would get Nutella fans excited to explore uncharted territories.</p>
              </div>
            </div>
          </div>
          <div class="item"> <img class="fourth-slide" src="{{ url() }}/img/carousel/jar_gourmet.jpg" alt="Fourth slide">
            <div class="container">
              <div class="carousel-caption">
                <p><b>With exquisite taste, you appreciate the finer things in life.</b> You’d inspire Nutella fans to get fancy and trade the “ordinary” for the “culinary.” Magnifique!</p>
              </div>
            </div>
          </div>
          <div class="item"> <img class="fifth-slide" src="{{ url() }}/img/carousel/jar_purist.jpg" alt="Fifth slide">
            <div class="container">
              <div class="carousel-caption">
                <p><b>You like to keep things simple. Why complicate something that’s already perfect?</b> You’d help fans appreciate Nutella for what it is a comforting constant in an ever-changing world.</p>
              </div>
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
      <!-- /.carousel -->
      <div class="desktopOnly">
        <div class="col-lg-2 col-md-4 col-sm-6 howtoenter-item"> <img src="{{ url() }}/img/jar_friend.jpg" alt="Friend" class="img-responsive img-centered">
          <p><b>A BFF through and through, you believe happiness is always best when it’s shared.</b> You’d bring fans together with your kindness and generosity.</p>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 howtoenter-item"> <img src="{{ url() }}/img/jar_artist.jpg" alt="Artist" class="img-responsive img-centered">
          <p><b>You see beauty in everything and channel it into creative, jaw-dropping masterpieces.</b> You’d inspire fans everywhere to really express themselves. </p>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 howtoenter-item"> <img src="{{ url() }}/img/jar_adventurer.jpg" alt="Adventurer" class="img-responsive img-centered">
          <p><b>You live life off the beaten path. Nothing’s too daring or inventive for you.</b> Your leadership would get Nutella fans excited to explore uncharted territories.</p>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 howtoenter-item"> <img src="{{ url() }}/img/jar_gourmet.jpg" alt="Gourmet" class="img-responsive img-centered">
          <p><b>With exquisite taste, you appreciate the finer things in life.</b> You’d inspire Nutella fans to get fancy and trade the “ordinary” for the “culinary.” Magnifique!</p>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 howtoenter-item"> <img src="{{ url() }}/img/jar_purist.jpg" alt="Purist" class="img-responsive img-centered">
          <p><b>You like to keep things simple. Why complicate something that’s already perfect?</b> You’d help fans appreciate Nutella for what it is: a comforting constant in an ever-changing world.</p>
        </div>
      </div>
    </div>
    <div class="row mgt70">
      <div class="col-md-4 col-sm-6"><h3 class="subsection-heading important">important stuff for your video</h3>
        <ul>
          <li>must be in English</li>
          <li>must be 60 seconds or shorter</li>
          <li>can’t be bigger than 20 MB</li>
          <li>must be an original creation</li>
          <li>you must appear in the video for at least 3 seconds</li>
          <li>you must be the only person in your video</li>
        </ul>
        <p class="subsection"><em>Additional Video Submission Guidelines <br />and Requirements apply.</em></p>
        <a href="#" class="btn ruleslnk" data-tracker="button,click,Rules - Assignment">See <span>Official Rules</span> for details</a></div>
      <div class="col-md-4 col-sm-6">
        <h3 class="subsection-heading">what we’re looking for in a Chief Nutella Ambassador</h3>
        <div class="lookforblock">
          <div class="lookfor">Creativity</div>
          <div class="lookfor">Optimism</div>
          <div class="lookfor">Originality</div>
          <div class="lookfor">Passion</div>
          <div class="lookfor">Enthusiasm</div>
          <div class="lookfor">Communication Skills</div>
          <?php /*<img src="{{ url() }}/img/howto-arrow-mobile.png" class="img-responsive img-centered mobilearrow mobileOnly" alt="" /> */ ?>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 tablet-center">
         <?php /*<img src="{{ url() }}/img/free-tshirt.png" class="img-responsive img-centered" alt="" usemap="#Map2" /><map name="Map2" id="Map2"><area shape="rect" coords="114,227,187,243" href="javascript:;" /></map> */ ?>
         <img src="{{ url() }}/img/campaignbutton.jpg" class="img-responsive img-centered" alt="" />
         <img src="{{ url() }}/img/howto-arrow.png" class="img-responsive img-centered desktoparrow" alt="" />
      </div>
    </div>
  </div>
</section>