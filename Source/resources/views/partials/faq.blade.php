<!-- FAQ Section -->
<section id="faq">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">frequently asked questions</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <ul>
        	<li><a href="javascript:;" data-cell="1" class="mobilebar"><span class="mobileOnly inlineblock plus">+</span> general questions</a>
            <ul class="accord1">
               <li>
                 <h4>What will the Chief Nutella Ambassador actually do?</h4>
                 <p>He or she will come to New York city and spend the week of January 3-9, 2016 helping us to create social media content and star in videos that will be used on Nutella social channels, in the days leading up to and on World Nutella Day. In addition, we have planned a unique set of activities for the Chief Nutella Ambassador and his or his guest to enjoy New York City, that certainly won’t disappoint.</p>
                 <h4>Where can I view the Official Rules?</h4>
                 <p>Right <a href="#" class="ruleslnk" data-tracker="button,click,Rules - FAQ - Where can I view the rules">here</a>.</p>
                 
                 <!-- <h4>I don’t see an answer to my question here. Who can I contact?</h4>
                 <p><a href="">lorem@ipsum.com</a></p>-->
               </li>
            </ul>
            </li>
            <li class="noline"><a href="javascript:;" data-cell="2" class="mobilebar"><span class="mobileOnly inlineblock plus">+</span> entering the contest</a>
              <ul class="accord2">
               <li>
                 <h4>Who can enter the contest?</h4>
                 <p>Individuals who are legal residents of the 50 United States and the District of Columbia who are at least 18 years of age with a valid email account as of the date of entry may enter the Contest. For additional details on entering the Contest, please see the <a href="#"  class="ruleslnk" data-tracker="button,click,Rules - FAQ - Who can enter the contest">Official Rules</a>.</p>

                 <h4>I live outside the United States. Can I enter the contest?</h4>
                 <p>No. Entries for the Chief Nutella Ambassador Contest may only be submitted by individuals who are legal residents of the 50 United States and D.C. who are at least 18 years of age with a valid email account as of the date of entry. However, fans of Nutella around the world may be eligible to vote for their favorite finalists. We see the Chief Nutella Ambassador as a leader of Nutella fans all around the world. See <a href="#"  class="ruleslnk" data-tracker="button,click,Rules - FAQ - I live outside the United States">Official Rules</a> for details on eligibility for the voting phase of the contest.</p>
                 
                 <h4>When’s the last day to submit my video?</h4>
                 <p>The video is part of the  entire entry that must be submitted in order for someone to be eligible to win the role of Chief Nutella Ambassador.  Entries can be submitted during the Submission Period between 12:00:00 AM EDT on September 21, 2015 and 11:59:59 pm EDT on October 11, 2015.</p>
                 
                 <h4>Do videos have to look professional?</h4>
                 <p>Absolutely not.  Use your phone, tablet, computer – whatever’s easiest for you! We’re not looking for Hollywood quality film, just the best person to be our Chief Nutella Ambassador.</p>
                 
                 <h4>I already made a video about Nutella that is posted on a social media site. Can I use it for this Contest?</h4>
                 <p>No (but thanks for making it!). For this Contest, your video must be new and original and cannot have been previously used or publicly posted. Please see the Official Rules for the Video Submission Guidelines and Requirements.</p>
               </li>
              </ul>
            </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <ul class="accord2">
        	<!--<li><a href="javascript:;" data-cell="3" class="mobilebar"><span class="mobileOnly inlineblock plus">+</span> submitting your video</a>
               <ul class="accord3">-->
                  <li class="noline"><a href="javascript:;" class="desktopOnly">&nbsp;</a>
                    <h4>Do certain Nutella ‘personalities’ have a better chance of winning than others?</h4>
                    <p>Nope. The Nutella personality that we ask you to select at the start of the entry submission process is not something that will be part of the judging and will not affect scoring at any stage of the <a href="javascript:;" class="ruleslnk" data-tracker="button,click,Rules - FAQ - Personalities better chance of winning">judging process</a>. The personality that you select should be what you believe best describes how you would lead World Nutella Day and serves as an inspiration for the video part of your entry.</p>

                    <h4>What if none of the five personalities reflect how I’d lead World Nutella Day? Can I make up a new one or combine some?</h4>
                    <p>Entrants to the Contest are required to select one of the five Nutella personalities provided in order to proceed with his or her entry. If you feel that more than one or none of the personalities feels right, select the one that feels the closest or which you believe will inspire a good video. The personalities are a way to guide each contestant in the development of his or her video; however, they are not something that will be part of the judging and will not affect scoring at any stage of the <a href="javascript:;" class="ruleslnk" data-tracker="button,click,Rules - FAQ - Personalities do not match">judging process</a>.</p>

                    <h4>Do I have to appear in the video I am submitting with my entry?</h4>
                    <p>Yes, for at least 3 seconds.</p>

                    <h4>Do I need to show Nutella product in the video I am submitting with my entry?</h4>
                    <p>No, it is not required. However, if you decide to show Nutella in your video, you must show it being used in a respectful and non-indulgent manner and otherwise use it in a way that complies with all of the Video Submission Guidelines & Requirements in the <a href="javascript:;" class="ruleslnk" data-tracker="button,click,Rules - FAQ - Show Nutella Products">Official Rules</a>.</p>

                    <h4>Can I use a song in the video I am submitting with my entry?</h4>
                    <p>No. You are not permitted to use any music in your video even if you own all rights to the music and lyrics. Please see the Video Submission Guidelines & Requirements in the <a href="javascript:;" class="ruleslnk" data-tracker="button,click,Rules - FAQ - Use a song">Official Rules</a>.</p>

                    <h4>Can I have my friends or family members in the video I am submitting with my entry?</h4>
                    <p>No. We love hanging out with our favorite people too, but you must be the only person in your video. Please see the Video Submission Guidelines & Requirements in the <a href="javascript:;" class="ruleslnk" data-tracker="button,click,Rules - FAQ - Friend or family">Official Rules</a>.</p>
                    
                    <h4>Will I get a t-shirt if I submit a video?</h4>
                    <p>Entrants who are eligible to enter the Contest and who submit a video that adheres to the Video Submission Guidelines & Requirements in the Official Rules, will receive a t-shirt approximately 4-6 weeks after the entry verification process, provided supplies of the t-shirt still remain available. Please see <a href="#" class="ruleslnk" data-tracker="button,click,Rules - FAQ - Will I get a t-shirt if I submit a video">Official Rules</a> for additional details.</p>
                  </li>
               <!--</ul>
            </li>-->
        </ul>
      </div>
      <div class="col-sm-3">
         <ul>
           <li>
             <ul class="accord2">
                <li><a href="javascript:;" class="desktopOnly">&nbsp;</a>
                   <h4>Why can’t I find my video in the Fan Video Submissions gallery?</h4>
                   <p>There may be a few reasons why you are unable to find your video in the gallery. After entries are submitted, all video parts of the entry are moderated to ensure that the video is in adherence of the Video Submission Guidelines & Requirements.  If your entry was submitted during a time period when we received a large number of entries, the higher-than-expected volume can make the turnaround time for the moderation longer than anticipated. We also have the right to exclude videos from the gallery for any reason, as set out in the <a href="#" class="ruleslnk" data-tracker="button,click,Rules - FAQ - Why isn’t my video showing up in the Fan Video Submissions">Official Rules</a>; however, if you were eligible to enter the Contest and your video complies with the Video Submission Guidelines & Requirements, we will use our reasonable efforts to include your video in the gallery. Please see the Video Submission Guidelines & Requirements in the <a href="#" class="ruleslnk" data-tracker="button,click,Rules - FAQ - Why isn’t my video showing up in the Fan Video Submissions - Link 2">Official Rules</a>.</p>
    
                   <h4>Why can't I upload my video?</h4>
                   <p>First, check to see if your file size is too big. Videos need to be 20 MB or smaller. Also check to see what type of file you are trying to upload.  The video needs to be in one of the following file formats: .MOV, MPEG4, MP4, .AVI, .WMV, .MPEGPS, .FLV, 3GPP or WebM.</p>
    
                   <h4>Can I submit more than one video?</h4>
                   <p>Only one entry can be submitted per person/email address.</p>
    
                   <h4>What are the video requirements?</h4>
                   <p>Please see the Video Submission Guidelines and Requirements in the <a href="javascript:;" class="ruleslnk" data-tracker="button,click,Rules - FAQ - What are the video requirements?">Official Rules</a>.</p>
                </li>
            </ul>
           </li>
           <li class="noline"><a href="javascript:;" data-cell="3" class="mobilebar"><span class="mobileOnly inlineblock plus">+</span> selecting finalists, the <br class="mobileOnly" />campaign and the grand prize winner</a>
              <ul class="accord3 lastfaq">
                  <li>
                    <h4>How will I know if I’m a finalist?</h4>
                    <p>Finalists will be notified on or about October 27, 2015 by email at the email address provided at the time of entry that they have been selected as a potential Finalist. Potential Finalists will not be declared a Finalist until eligibility verification, Background Check and Documentation (all as defined and discussed in the <a href="javascript:;" class="ruleslnk" data-tracker="button,click,Rules - FAQ - How will I know if I’m a finalist?">Official Rules</a>) has been cleared and completed, respectively. Finalists will be announced to the public on the Nutelladay.com website on November 16, 2015 at or before 9:00:00 am EST. See <a href="javascript:;" class="ruleslnk" data-tracker="button,click,Rules - FAQ - How will I know if I’m a finalist? - Link 2">Official Rules</a> for additional details.</p>
                  </li>
               </ul>
           </li>
         </ul>
      </div>
      <div class="col-sm-3">
        <ul class="accord3 lastfaq">
        	<li><a href="javascript:;" class="desktopOnly">&nbsp;</a>
                <h4>If I am selected as a Finalist, what will I have to do?</h4>
                <p>During the period between November 16, 2015 and November 29, 2015, each Finalist will conduct a campaign through his or her own social media accounts on Facebook, Twitter and Instagram to show Nutella fans all over the world why he or she would be the best person to lead World Nutella Day on their behalf. Each Finalist will be given a toolkit of various items, such as Nutella products, Nutella t-shirts, buttons, etc., as well as digital graphics that can be used in social content, all for the purpose of getting Nutella fans to vote for him or her to be the choice for Chief Nutella Ambassador.  All campaigning and campaign content will be subject to the <a href="javascript:;" class="ruleslnk" data-tracker="button,click,Rules - FAQ - If I am selected as a Finalist">Official Rules</a> and the Documentation that each Finalist will be required to sign prior to being named an official Finalist.</p>
    
                <h4>I live outside of the United States and want to participate in the program.  How do I vote for the Chief Nutella Ambassador?</h4>
                <p>On November 16, 2015, the Finalists will be announced to the public on the Nutelladay.com website.  During the period between November 16, 2015 and November 29, 2015, each Finalist will conduct a campaign through his or her own social media accounts on Facebook, Twitter and Instagram to show Nutella fans all over the world why he or she would be the best person to lead World Nutella Day on their behalf. In addition, there will be content relating to the Contest on the Nutella Facebook, Twitter and Instagram pages about the Finalists and World Nutella Day. Nutella fans from all over the world will have the opportunity, if eligible, to participate in a public vote to help the Sponsor select the Chief Nutella Ambassador. To participate in the public vote, an Eligible Voter may visit Nutelladay.com between November 16, 2015 and November 29, 2015 to cast his/her vote by clicking the “VOTE” button associated with the Entrant for whom he/she wishes to vote.  Eligible Voters may also cast their votes by sending a Tweet with the hashtag displayed next to the Entrant on the Nutelladay.com website. There is a limit of up to five (5) votes per person/IP address per day on Nutelladay.com <b>and</b> one (1) vote per person/Twitter handle per day.</p>
    
                <h4>When will the winner be selected?</h4>
                <p>Upon the completion of the Campaign Period and the Public Vote, the Judges and Sponsor shall judge each of the Finalists, their Entries and their Social Media Campaigns based on the criteria established in the Official Rules.   The judging shall represent a percentage of the final decision and the public vote shall represent a percentage of the final decision. The winner will be notified by email on or about December 7, 2015. The Winner will be announced on the Nutelladay.com website, www.Nutella.com, facebook.com/WorldNutellaDay, twitter.com/worldnutelladay, facebook.com/nutellausa, facebook.com/nutella, twitter.com/nutellausa, on or about January 11, 2015.</p>
            </li>
        </ul>
      </div>
    </div>
  </div>
</section>