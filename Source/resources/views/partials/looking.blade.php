<!-- Services Section -->
@if ($page === 'entries')
  <?php $css = ''; ?>
@else
  <?php $css = 'thanks'; ?>
@endif
<section id="looking" class="{{ $css }}">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">We’re looking for someone to fire up our fans</h2>
        <div class="arrow_left">
          <div class="arrow_right">
            <p>World Nutella Day needs a Chief Nutella Ambassador—someone to star in videos, connect our passionate fans on social media, and lead the global celebration. 
            @if ($page === 'entries')
                Five finalists will campaign for the honor, but only one will win. Could it be you?</p>
                <h3 class="section-subheading">how do I enter?</h3>
            @else
            	Five finalists will campaign for fan votes, but only one will win the honor.
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  @if ($page === 'entries')
  <div class="arrow"><img src="{{ url() }}/img/more_arrow.png" class="img-responsive img-centered desktopOnly"><img src="{{ url() }}/img/more_arrow-mobile.png" class="img-responsive img-centered mobileOnly"></div>
  @endif
</section>