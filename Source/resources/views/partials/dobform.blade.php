<!-- Enter Section -->
@if ($page === 'entries')
  <?php $css = $underage === true?'underage':''; ?>
@else
  <?php $css = 'thanks prevote'; ?>
@endif
<section id="enter" class="{{ $css }}">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
      	@if ($page === 'entries')
        <h2 class="section-heading">enter now</h2>
        <p id="dob_into_text">Please begin by entering your date of birth.</p>
        @else
        <h2 class="section-heading">The entry period is now over.</h2>
        <p>Check back on November 16th when you’ll get to meet our 5 finalists, learn how to follow their social media campaigns and have the chance to vote for your favorite.</p>
        @endif
      </div>
    </div>
    @if ($page === 'entries')
    <form id="dob_form">
      <div class="row">
        <div class="enter-arrows">
          @if (!isset($underage) || $underage === false)
          <select name="dob_mm" id="dob_mm" class="selectpicker">
            <option value="">MM</option>
            @for ($i = 1; $i < 13; $i++)
                <option value="{{ sprintf('%02d',$i) }}">{{ sprintf('%02d',$i) }}</option>
            @endfor
          </select>
          <select name="dob_dd" id="dob_dd" class="selectpicker">
            <option value="">DD</option>
            @for ($i = 1; $i < 32; $i++)
                <option value="{{ sprintf('%02d',$i) }}">{{ sprintf('%02d',$i) }}</option>
            @endfor
          </select>
          <select name="dob_yyyy" id="dob_yyyy" class="selectpicker">
            <option value="">YYYY</option>
            @for ($i = date('Y'); $i > date('Y')-100; $i--)
                <option value="{{ $i }}">{{ $i }}</option>
            @endfor
          </select>
          @endif
        </div>
        <p class="help-block text-danger">
        @if (isset($underage) && $underage === true)
        <strong>Sorry, you’re unable to participate in the contest :(</strong><br />Check out <a href="javascript:;" onClick="goto('#entries')">Fan Video Submissions</a> to see what your fellow Nutella fans have been making!
        @endif
        </p>
      </div>
      <div class="row">
        <div class="col-lg-12 text-center">
        @if (!isset($underage) || $underage === false)
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button class="btn submitbtn" type="button" title="Submit">Submit</button>
        @else
          <img src="{{ url() }}/img/spacer.gif" width="200" height="100" />
        @endif
        </div>
      </div>
    </form>
    @endif
  </div>
</section>