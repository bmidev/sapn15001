<!-- Abbr Rules -->
<section id="abbrrules" class="mobileOnly">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
      	@if ($page == 'finalists')
      		<p>Social Media Campaign and Voting begins at 9:00:00 am EST on 11/16/15 and ends at 5:00:00 pm EST on 11/29/15 (“Campaign Period”).  During the Campaign Period, Eligible Voters can visit nutelladay.com to Vote or cast their Vote by sending a Tweet with the hashtag displayed on nutelladay.com next to the Finalist for whom they wish to Vote. <strong class="bold">Limit five (5) Votes via nutelladay.com per person/per IP address/per day and one (1) Vote via Twitter per person/Twitter handle per day.</strong> For complete details, including Voter eligibility requirements, see Official Rules available <a href="javascript:;" class="ruleslnk">here</a>.</p>
        @else
        	<p>NO PURCHASE NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT INCREASE YOUR CHANCES OF WINNING.  Open only to legal residents of the 50 U.S. and D.C. who are at least 18 years of age with a valid email account as of the date of entry.  Void where prohibited.  Submission Period begins at 12:00:00 AM EDT on 9/21/2015 and ends at 11:59:59 PM EDT on 10/11/2015.  For complete details, including entry and prizing details, see <a class="ruleslnk" href="#" data-tracker="button,click,Official Rules - ABBR">Official Rules</a> available at www.NutellaDay.com. Sponsor: Ferrero U.S.A., Inc., 600 Cottontail Lane, Somerset, NJ 08873.</p>
        @endif
      </div>
    </div>
  </div>
</section>