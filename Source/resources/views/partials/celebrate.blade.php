<!-- Recipes Section -->
<section id="celebrate">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">10 easy ways to celebrate the big day</h2>
      </div>
    </div>
    <div class="row celebratebox">
		<div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb1.png" class="img-responsive img-centered desktopOnly" alt="Make a recipe with Nutella"><img src="{{ url() }}/img/celeb1-mobile.png" class="img-responsive img-centered mobileOnly" alt="Make a recipe with Nutella"><h3 class="subsection-heading">Make a recipe<br />with Nutella</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb2.png" class="img-responsive img-centered desktopOnly" alt="Write a song or poem about Nutella"><img src="{{ url() }}/img/celeb2-mobile.png" class="img-responsive img-centered mobileOnly" alt="Write a song or poem about Nutella"><h3 class="subsection-heading">Write a song or poem<br />about Nutella</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb3.png" class="img-responsive img-centered desktopOnly" alt="Enjoy Nutella somewhere interesting"><img src="{{ url() }}/img/celeb3-mobile.png" class="img-responsive img-centered mobileOnly" alt="Enjoy Nutella somewhere interesting"><h3 class="subsection-heading">Enjoy Nutella<br />somewhere interesting</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb4.png" class="img-responsive img-centered desktopOnly" alt="Strike a pose with Nutella"><img src="{{ url() }}/img/celeb4-mobile.png" class="img-responsive img-centered mobileOnly" alt="Strike a pose with Nutella"><h3 class="subsection-heading">Strike a pose<br />with Nutella</h3> </div>
        
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb5.png" class="img-responsive img-centered desktopOnly" alt="Create some Nutella inspired art"><img src="{{ url() }}/img/celeb5-mobile.png" class="img-responsive img-centered mobileOnly" alt="Create some Nutella inspired art"><h3 class="subsection-heading">Create some Nutella<br />inspired art</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb6.png" class="img-responsive img-centered desktopOnly" alt="Do an original flavor pairing"><img src="{{ url() }}/img/celeb6-mobile.png" class="img-responsive img-centered mobileOnly" alt="Do an original flavor pairing"><h3 class="subsection-heading">Do an original<br />flavor pairing</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb7.png" class="img-responsive img-centered desktopOnly" alt="Relive your first Nutella experience"><img src="{{ url() }}/img/celeb7-mobile.png" class="img-responsive img-centered mobileOnly" alt="Relive your first Nutella experience"><h3 class="subsection-heading">Relive your first<br />Nutella experience</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb"> <img src="{{ url() }}/img/celeb8.png" class="img-responsive img-centered desktopOnly" alt="Give Nutella to people you love"><img src="{{ url() }}/img/celeb8-mobile.png" class="img-responsive img-centered mobileOnly" alt="Give Nutella to people you love"><h3 class="subsection-heading">Give Nutella to<br />people you love</h3> </div>
        
        <div class="col-xs-6 col-sm-3 celeb div-centered"> <img src="{{ url() }}/img/celeb9.png" class="img-responsive img-centered desktopOnly" alt="Introduce someone to Nutella for the first time"><img src="{{ url() }}/img/celeb9-mobile.png" class="img-responsive img-centered mobileOnly" alt="Introduce someone to Nutella for the first time"><h3 class="subsection-heading">Introduce someone to<br />Nutella for the first time</h3> </div>
        <div class="col-xs-6 col-sm-3 celeb div-centered"> <img src="{{ url() }}/img/celeb10.png" class="img-responsive img-centered desktopOnly" alt="Host a delicious party in honor of Nutella"><img src="{{ url() }}/img/celeb10-mobile.png" class="img-responsive img-centered mobileOnly" alt="Host a delicious party in honor of Nutella"><h3 class="subsection-heading">Host a delicious party<br />in honor of Nutella</h3> </div>
        <br class="clr" />
    </div>
    <div class="row">
      <div class="col-lg-12 text-center">
        <h3 class="section-subheading">Most importantly, SHARE your love with everyone in the world!</h3>
        <h3 class="subsection-subheading"><a href="https://www.facebook.com/WorldNutellaDay" target="_blank">facebook.com/WorldNutellaDay</a></h3>
      </div>
    </div>
  </div>
</section>