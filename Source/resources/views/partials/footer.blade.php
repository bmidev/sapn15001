<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6 quicknav">
        @if ($page != 'winner')
        <ul class="list-inline quicklinks">
          <li><a href="http://nutellausa.com/" data-tracker="footer,click,NutellaUSA.com" target="_blank">NutellaUSA.com</a> </li>
          <li><a href="http://www.ferrerousa.com/ferrero-usa/privacy-policy/terms-of-use" data-tracker="footer,click,Privacy Policy" target="_blank">Privacy Policy</a> </li>
        </ul>
        @else
        <ul class="list-inline quicklinks">
          @if (isset($country_code) && $country_code !== 'US')
          <li><a href="http://nutella.com/" data-tracker="footer,click,Nutella.com" target="_blank">Nutella.com</a> </li>
          <li><a href="javascript:;" class="global_privacy" data-tracker="footer,click,Legal Terms">Legal Terms</a> </li>
          @else
          <li><a href="http://nutellausa.com/" data-tracker="footer,click,NutellaUSA.com" target="_blank">NutellaUSA.com</a> </li>
          <li><a href="http://www.ferrerousa.com/ferrero-usa/privacy-policy/terms-of-use" data-tracker="footer,click,Privacy Policy" target="_blank">Privacy Policy</a> </li>
          @endif
        </ul>
        @endif
      </div>
      <div class="col-md-6 social">
        <ul class="list-inline social-buttons">
          <li><a href="https://www.facebook.com/worldnutelladay" data-tracker="footer,click,Facebook" target="_blank"><i class="fa fa-facebook"></i></a> </li>
          <li><a href="https://twitter.com/Nutelladay" data-tracker="footer,click,Twitter" target="_blank"><i class="fa fa-twitter"></i></a> </li>
          <!--<li><a href="https://instagram.com/nutella/" data-tracker="footer,click,Instagram" target="_blank"><i class="fa fa-instagram"></i></a> </li>
           <li><a href="https://www.pinterest.com/nutellausa/" data-tracker="footer,click,Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a> </li> -->
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
      @if (isset($country_code) && $country_code === 'US')
      <span class="copyright"><strong>NO PURCHASE NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT INCREASE YOUR CHANCES OF WINNING.</strong> Open only to legal residents of the 50 U.S. and D.C. who are at least 18 years of age with a valid email account as of the date of entry.  Void where prohibited.  Submission Period begins at 12:00:00 AM EDT on 9/21/2015 and ends at 11:59:59 PM EDT on 10/11/2015.  For complete details, including entry and prizing details, see <a class="ruleslnk" href="#" data-tracker="footer,click,Official Rules">Official Rules</a> available at www.nutelladay.com. Sponsor: Ferrero U.S.A., Inc., 600 Cottontail Lane, Somerset, NJ 08873.</span>
      @else
      <span class="copyright winleft">© 2016 Ferrero Group. All rights reserved.</span>
      @endif
      </div>
    </div>
  </div>
</footer>
@include('partials.modals')
<!-- jQuery -->
<script src="{{ url() }}/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ url() }}/js/bootstrap.min.js"></script>
<!-- Bootstrap select Library -->
<script src="{{ url() }}/js/bootstrap-select.js"></script>
<!-- Custom Checkbox JavaScript -->
<script src="{{ url() }}/js/icheck.min.js"></script>
@if ($page == 'entries')
<!-- File Upload JavaScript -->
<script src="{{ url() }}/js/dropzone.js"></script>
@endif
<!-- Plugin JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="{{ url() }}/js/classie.js"></script>
<!-- <script src="{{ public_path() }}js/cbpAnimatedHeader.js"></script> -->
<!-- Contact Form JavaScript -->
<script src="{{ url() }}/js/jqBootstrapValidation.js"></script>
<!-- Adds touch events to site -->
<script src="{{ url() }}/js/jquery.mobile.custom.min.js"></script>
<!-- Custom HTML5 Player -->
<script src="{{ url() }}/video-js/video.js"></script>
<!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
<script>
videojs.options.flash.swf = "video-js.swf";
</script>
<!--[if !IE]>

<![endif]-->
<!--[if lg IE 9]>

<![endif]-->
<!--[if IE 9]>
<script src="{{ url() }}/js/jquery.placeholder.min.js"></script>
<script>
document.getElementById('dropzone').setAttribute("id", "dropzone_ie9");
document.getElementsByClassName("uploadbtn btn mobileOnly")[0].setAttribute("class", "uploadbtn btn");
$(function() {
	$('input, textarea').placeholder();
});

function ie9_upload_complete(success,fileuploaded){
	alert('Upload complete, returned: '+success+' & '+fileuploaded);
    $(function () {
    	if(success == true){
    		$(".dz-progress").css('display','none');
			trackEvent('button', 'click', 'File Upload');
			$("#fileErr").html('');
			$("#fileInput_hidden").val(fileuploaded);
			$('.file_preview').html(fileuploaded+' uploaded').css('display','block');
        }else{
        	$('.file_preview').html('').css('display','none');
			$(".dz-progress").css('display','none');
			$("#fileErr").html('There was a problem uploading your file because '+fileuploaded);
            goto("#fileErr");
        }
        $('#hidden_iframe').remove();
    });
}
</script>
<script src="{{ url() }}/ie9-uploader/main.js"></script>
<![endif]-->

<!-- Custom Theme JavaScript -->
<script src="{{ url() }}/js/agency.js?v=201601281705"></script>
@if ($is_tablet === true)
<script>
	adjust_orientation_viewport(window.orientation);
</script>
@endif
@if (isset($show_entry))
<script>
$(function() {
	<?php $add_vote = $show_entry['finalist'] === 'yes' ? '' : ''; ?>
	show_entry('{{ $show_entry['video_url'] }}', '{{ $show_entry['video_thumbnail_url'] }}'{{ $add_vote }});
});
</script>
@endif
@if (isset($show_rules) && $show_rules === true)
<script>
	show_popup('modal-rules');
</script>
@endif
@if (isset($country_code) && $country_code !== 'US' && (!isset($show_cookie) || $show_cookie === true))
<div id="global_cookie">
	<h4>Cookies</h4>
  <p>This Site uses technical cookies.<br />
  If you wish to learn more on cookies, or change your browser's settings on cookies (up to excluding the installation of whatever cookies), click <a href="javascript:;" class="cookieclose">here</a>.<br />If you continue browsing by accessing any other section or selecting any item on the website (e.g. by clicking a picture or a link), you signify your consent to the use of cookies;</p>
  <p><a href="javascript:;" class="cookieclose btn">Close X</a></p>
</div>
<script>
@endif
</body>
</html>