<!-- Recipes Section -->
<section id="recipes" @if ($page === 'winner' && isset($country_code) && $country_code !== 'US') class="global" @endif>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">recipe inspiration for the big day</h2>
        <p>The combinations with Nutella<sup>&reg;</sup> are endless. Try these fun dishes with your family, friends and fellow fans.</p>
      </div>
    </div>
    <div class="row recipesbox">
		@if ($recipes->count()>0)
			@foreach ($recipes as $recipe)
        		<?php $json = json_decode($recipe,1); ?>
				<div class="col-xs-6 col-sm-3 recipe"> <img src="{{ url() }}/img/recipes/{{ $json['image'] }}" class="img-responsive img-centered" alt="{{ $json['name'] }}"></div>
			@endforeach
		@endif
        <br class="clr" />
    </div>
    <div class="row">
      <div class="col-lg-12 text-center">
      <?php
	  /*
      <input type="hidden" name="recipes_used" id="recipes_used" value="{{ implode('~',$recipes_used) }}" />
      @if ($recipes_more === true)
        <button class="btn morerecipebtn" type="button" id="morerecipebtn" title="Submit">load more</button>
      @endif
	  */
	  ?>
      @if ($recipes_more === true)
      	<a href="https://www.pinterest.com/nutellausa" class="btn morerecipebtn" id="morerecipebtn" title="see more" target="_blank">see more</a>
      @endif
      </div>
    </div>
  </div>
</section>