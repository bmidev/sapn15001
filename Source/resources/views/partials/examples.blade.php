<!-- Example Section -->
<section id="examples">
  <div class="container">
    <div class="row">
      <h2>Need inspiration? <span class="mobred">watch example entries!</span></h2>
      <p>To help get your creative juices flowing, some well-known Nutella fans from the Internet created some example videos (these are not actual contest entries). Remember, your video doesn’t need to look like a Hollywood production—seriously, use your phone!</p>
      <div class="col-md-4 col-sm-6 portfolio-item"> <a href="#" class="example-link" data-toggle="modal-example1"> <img src="{{ url() }}/img/placeholders/Hailey_CNA_Video.jpg" class="img-responsive desktopOnly inlineblock" alt=""><img src="{{ url() }}/img/placeholders/Hailey_CNA_Video-mobile.jpg" class="img-responsive mobileOnly inlineblock" alt=""> </a>
        <!-- <div class="portfolio-caption">
           <h4>Diane</h4>
        </div> -->
      </div>
      <div class="col-md-4 col-sm-6 portfolio-item"> <a href="#" class="example-link" data-toggle="modal-example2"> <img src="{{ url() }}/img/placeholders/Will-You-Be-There_1.jpg" class="img-responsive desktopOnly inlineblock" alt=""><img src="{{ url() }}/img/placeholders/Will-You-Be-There_1-mobile.jpg" class="img-responsive mobileOnly inlineblock" alt=""> </a>
        <!-- <div class="portfolio-caption">
          <h4>Mike</h4>
        </div> -->
      </div>
      <div class="col-md-4 col-sm-6 portfolio-item"> <a href="#" class="example-link" data-toggle="modal-example3"> <img src="{{ url() }}/img/placeholders/Will_CNA_Video.jpg" class="img-responsive desktopOnly inlineblock" alt=""><img src="{{ url() }}/img/placeholders/Will_CNA_Video-mobile.jpg" class="img-responsive mobileOnly inlineblock" alt=""> </a>
        <!-- <div class="portfolio-caption">
          <h4>Liz</h4>
        </div> -->
      </div>
    </div>
  </div>
</section>