<!-- About Section -->
<?php /* Add below to 3 if statements if they seperate the UK and global pages
&& isset($country_code) && $country_code !== 'US'*/
?>
<section id="about">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-6 text-center @if ($page === 'winner')  globalHide @endif">
   	     <img src="{{ url() }}/img/world-new.jpg" alt="World Nutella Day" class="img-responsive img-centered world desktopOnly excTablet">
         <img src="{{ url() }}/img/world-new-mobile.jpg" alt="World Nutella Day" class="img-responsive img-centered world mobileOnly incTablet">
      </div>
      <div class="col-xs-12 col-md-6 text-center @if ($page === 'winner')  globalCenter @endif">
      	 <h2 class="section-heading desktopOnly excTablet">About</h2>
         <img src="{{ url() }}/img/logo3.png" alt="World Nutella Day" class="img-responsive img-centered desktopOnly">
         @if ($page === 'winner')
         	<img src="{{ url() }}/img/logo3-global-mobile.png" alt="World Nutella Day" class="img-responsive img-centered mobileOnly globalImg">
         @endif
         <h3 class="section-subheading">Started by fans like you!</h3>
         <p>Have you ever loved something so much you thought it deserved a holiday? An American blogger named Sara Rosso did. That’s why she founded World Nutella Day on February 5, 2007.</p>
         <p>And Nutella fans loved it! They came together to celebrate their passion for Nutella on social media through photos, recipes, poems, and messages. From Italy to Australia to the Americas, World Nutella Day has since become a global phenomenon with people sharing and spreading the wonder of Nutella at home, work, and school; in their communities off- and online; with family, friends, and fellow fans everywhere.</p>
         <p>In 2015, Sara transferred World Nutella Day to Ferrero (the makers of Nutella) to help it live on and grow for years to come. So for 2016, we searched for a new Chief Nutella Ambassador—someone with Sara’s delicious devotion—to help us lead the fans for this upcoming World Nutella Day.</p>
      </div>
    </div>
  </div>
</section>