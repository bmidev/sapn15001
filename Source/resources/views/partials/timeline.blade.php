<!-- Timeline Section -->
<section id="timeline">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">contest timeline</h2>
        <p class="section-intro">@if ($page === 'entries') Here’s what will happen once all of the submissions are received. @endif</p>
      </div>
    </div>
    <div class="row">
      <div id="myTimelineCarousel" class="carousel slide mobileOnly" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myTimelineCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myTimelineCarousel" data-slide-to="1"></li>
          <li data-target="#myTimelineCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item slide1 active">
            <h3 class="month">October</h3>
            <img class="first-slide" src="{{ url() }}/img/carousel/timeline-top5.jpg" alt="First slide">
            <div class="container">
              <div class="carousel-caption">
                <h3>We’ll pick five finalists</h3>
        		<p>After reviewing the entries, we’ll announce our top five finalists on this site.</p>
              </div>
            </div>
          </div>
          <div class="item slide2">
            <h3 class="month">November</h3>
            <img class="first-slide" src="{{ url() }}/img/carousel/timeline-speaker.jpg" alt="Second slide">
            <div class="container">
              <div class="carousel-caption">
                <h3>Finalists will campaign on social media</h3>
        		<p>Our top five finalists will promote themselves and World Nutella Day on social media. Fans all over the world will vote to help us choose a winner.</p>
              </div>
            </div>
          </div>
          <div class="item slide3">
            <h3 class="month">January</h3>
            <img class="first-slide" src="{{ url() }}/img/carousel/timeline-badge.jpg" alt="Third slide">
            <div class="container">
              <div class="carousel-caption">
                <h3>The Chief Nutella Ambassador will be revealed!</h3>
       			<p>He or she will lead the World Nutella Day celebration on February 5</p>
              </div>
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#myTimelineCarousel" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myTimelineCarousel" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
      <!-- /.carousel -->
    </div>
    <div class="row timeline-arrow desktopOnly">
      <div class="col-lg-5ths text-right">
        <h4 class="section-heading text-right">October</h4>
      </div>
      <div class="col-lg-5ths timeline-arrow-left text-center"><h4 class="section-heading">&nbsp;</h4></div>
      <div class="col-lg-5ths timeline-arrow-center text-center">
        <h4 class="section-heading">November</h4>
      </div>
      <div class="col-lg-5ths timeline-arrow-right text-center"><h4 class="section-heading">&nbsp;</h4></div>
      <div class="col-lg-5ths text-left">
        <h4 class="section-heading text-left">January</h4>
      </div>
    </div>
    <div class="row process desktopOnly">
      <div class="col-sm-4 text-center">
        <img src="{{ url() }}/img/timeline-top5.jpg" class="img-responsive img-centered" alt="Top 5">
        <h3>We’ll pick five finalists</h3>
        <p>After reviewing the entries, we’ll announce our top five finalists on this site.</p>
      </div>
      <div class="col-sm-4 text-center">
        <?php /*<img src="{{ url() }}/img/timeline-charts.jpg" class="img-responsive img-centered" alt="Chart Image">*/ ?>
        <img src="{{ url() }}/img/timeline-speaker.jpg" class="img-responsive img-centered" alt="Speaker Image">
        <h3>Finalists will campaign on social media</h3>
        <p>Our top five finalists will promote themselves and World Nutella Day on social media. Fans all over the world will vote to help us choose a winner.</p>
      </div>
      <div class="col-sm-4 text-center">
        <img src="{{ url() }}/img/timeline-badge.jpg" class="img-responsive img-centered" alt="Badge">
        <h3>The Chief Nutella Ambassador will be revealed!</h3>
        <p>He or she will lead the World Nutella Day celebration on February 5.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="spark-left"><h2 class="section-heading spark-right">fans will celebrate world nutella day on february 5!</h2></div>
      </div>
    </div>
  </div>
</section>