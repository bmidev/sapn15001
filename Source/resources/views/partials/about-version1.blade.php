<!-- About Section -->
<section id="about">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-lg-6 text-center">
   	     <img src="{{ url() }}/img/world-new.jpg" alt="World Nutella Day" class="img-responsive img-centered world desktopOnly">
         <img src="{{ url() }}/img/world-new-mobile.jpg" alt="World Nutella Day" class="img-responsive img-centered world mobileOnly">
      </div>
      <div class="col-sm-12 col-lg-6 text-center">
      	 <h2 class="section-heading desktopOnly">About</h2>
         <img src="{{ url() }}/img/logo3.png" alt="World Nutella Day" class="img-responsive img-centered desktopOnly">
         <h3 class="section-subheading">Started by a fan like you!</h3>
         <p>Have you ever loved something so much you thought it deserved a holiday? Sara Rosso, an American blogger living in Italy, did. That’s why she founded World Nutella Day on February 5th, 2007.</p>
         <p>And Nutella fans loved it! They came together to celebrate their passion for Nutella on social media through photos, recipes, poems, and messages. From Italy to India to Israel, World Nutella Day has since become a global phenomenon with people sharing and spreading the wonder of Nutella at home, work, and school; in their communities off- and online; with family, friends, and fellow fans everywhere.</p>
         <p>In 2015, Sara transferred World Nutella Day to Ferrero (the makers of Nutella) to help it live on and grow for years to come. This year, we’re looking for a Chief Nutella Ambassador – someone with Sara’s delicious devotion – to help us lead the fans for World Nutella Day 2016. Will it be you?</p>
      </div>
    </div>
  </div>
</section>