<!-- winner Section -->
<section id="winner">
  <div class="container">
    <div class="row">
      <h2>meet your Chief Nutella<sup>&reg;</sup> Ambassador & host for World Nutella<sup>&reg;</sup> Day 2016</h2>
      <div class="star"><h3>{{ $winner['first_name'].' '.$winner['last_name'] }}</h3></div><br />
      <div class="col-md-6 center">
          @if (isset($country_code) && $country_code !== 'US')
          <p>{!! str_replace("Congratulations, Chrissy, Chief Nutella Ambassador!","Congratulations, Chrissy, Chief Nutella Ambassador 2016!",$winner['winner_biog']) !!}</p>
          @else
          <p>{!! $winner['winner_biog'] !!}</p>
          @endif
          <a href="#" class="videoplay" data-player="{{ $winner['winner_video'] }}"> <img src="{{ $winner['winner_image2'] }}" class="img-responsive img-centered" alt=""> </a>
      </div>
    </div>
  </div>
</section>