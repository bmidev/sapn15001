<!-- Recipes Section -->
<section id="winners-statement" @if ($page === 'winner' && isset($country_code) && $country_code !== 'US') class="global" @endif>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
      	@if (isset($country_code) && $country_code !== 'US')
      	<h2 class="section-heading">Chrissy’s mission: inspire 7 billion people</h2>
        <p>For World Nutella<sup>®</sup> Day 2016, Chrissy is on a mission to get the 7 billion people of the world to join in the celebration. See how she’s going to do it.</p>
        @else
      	<h2 class="section-heading">Chrissy’s mission: inspire 7 billion fans</h2>
        <p>For World Nutella Day 2016, Chrissy is on a mission to get the 7 billion people of the world to join in the celebration. See how she’s going to do it.</p>
        @endif
      </div>
    </div>
    <div class="row statementbox">
		<div class="col-xs-12 col-sm-4 statement"> <h3 class="subsection-heading mobileOnly">Discover</h3><a href="javascript:;" class="example-link" data-toggle="modal-mission1"><img src="{{ url() }}/img/placeholders/mission1.jpg" class="img-responsive img-centered" alt="Discover"></a><h3 class="subsection-heading desktopOnly">Discover</h3> </div>
        <div class="col-xs-12 col-sm-4 statement"> <h3 class="subsection-heading mobileOnly">Share</h3><a href="javascript:;" class="example-link" data-toggle="modal-mission2"><img src="{{ url() }}/img/placeholders/mission2.jpg" class="img-responsive img-centered" alt="Share"></a><h3 class="subsection-heading desktopOnly">Share</h3> </div>
        <div class="col-xs-12 col-sm-4 statement"> <h3 class="subsection-heading mobileOnly">Come Together</h3><a href="javascript:;" class="example-link" data-toggle="modal-mission3"><img src="{{ url() }}/img/placeholders/mission3.jpg" class="img-responsive img-centered" alt="Come Together"></a><h3 class="subsection-heading desktopOnly">Come Together</h3> 
        </div>
        <br class="clr" />
    </div>
  </div>
</section>