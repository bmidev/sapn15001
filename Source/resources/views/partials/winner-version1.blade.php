<!-- winner Section -->
<section id="winner">
  <div class="container">
    <div class="row">
      <h2>meet your Chief Nutella Ambassador & host for World Nutella Day</h2>
      <div class="star"><h3>{{ $winner['first_name'].' '.$winner['last_name'] }}</h3></div><br />
      <div class="col-md-5 center">
          <p>{!! $winner['winner_biog'] !!}</p>
          <a href="#" class="winner-link" data-toggle="winner-main"> <img src="{{ $winner['winner_image1'] }}" class="img-responsive img-centered mainimg" alt=""> </a>
          <div class="col-md-6 winvideotxt">
            <h4>Check out his video submission to get a glimpse of his tasty talents.</h4>
          </div>
          <div class="col-md-6 nopad">
             <a href="#" class="videoplay winentry" data-player="{{ $winner['video_url'] }}"> <img src="{{ $winner['video_thumbnail_url'] }}" class="img-responsive secondimg desktopOnly inlineblock" alt=""><img src="{{ $winner['video_thumbnail_url'] }}" class="img-responsive img-centered mobileOnly" alt=""><img src="{{ url() }}/img/play_small.png" class="img-responsive play desktopOnly" alt="play"><img src="{{ url() }}/img/play.png" class="img-responsive play mobileOnly" alt="play"> </a>
          </div>
      </div>
    </div>
  </div>
</section>