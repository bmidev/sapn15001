<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWinnerStageFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('entries', function($table)
		{
			$table->text('winner_biog')->after('understand');
			$table->string('winner_video')->after('winner_biog');
			$table->string('winner_image1')->after('winner_video');
			$table->string('winner_image2')->after('winner_image1');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('entries', function($table)
		{
			$table->dropColumn(['winner_biog', 'winner_video', 'winner_image1', 'winner_image2']);
		});
	}

}
