<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueFromEntryEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		/*Schema::table('entries', function($table)
		{
			$table->string('email')->change();
		});*/
		DB::statement('ALTER TABLE entries DROP INDEX entries_email_unique;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('entries', function($table)
		{
			$table->string('email')->unique()->change();
			//ALTER TABLE `entries` ADD UNIQUE(`email`);
		});
	}

}
