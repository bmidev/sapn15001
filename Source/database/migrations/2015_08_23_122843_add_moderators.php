<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModerators extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\DB::table('users')->insert([
		    'name' 		 => 'BMI Admin',
		    'email'   	 => 'admin@brandmovers.com',
		    'password'   => Hash::make('bO*gnxCi$WV12M/'),
		    'created_at' => new DateTime(),
		    'updated_at' => new DateTime()
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement("DELETE FROM users WHERE email='admin@brandmovers.com' LIMIT 1;");
	}

}
