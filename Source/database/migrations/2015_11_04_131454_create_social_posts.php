<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialPosts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('social_posts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('network');
			$table->bigInteger('network_id');
			$table->string('network_id_str');
			$table->text('url');
			$table->text('image')->nullable();
            $table->text('image_thumb')->nullable();
            $table->text('video_url')->nullable();
			$table->string('title')->nullable();
			$table->text('comments');
			$table->string('author_name');
			$table->text('author_image');
			$table->string('author_username');
			$table->string('author_profile');
			$table->dateTime('post_date');
			$table->boolean('processed')->default(false);
			$table->boolean('valid_entry')->nullable();
			$table->text('errors')->nullable();
			$table->unique(['network', 'network_id', 'network_id_str']);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('social_posts');
	}

}
