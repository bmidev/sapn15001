<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->enum('status', array('approved','rejected','pending'))
				->default('pending');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email')->unique();
			$table->enum('type', array('friend','artist','adventurer','gourmet','purist'));
			$table->string('street1');
			$table->string('street2')->nullable();
			$table->string('city',100);
			$table->string('state',2);
			$table->string('zip',10);
			$table->date('date_of_birth')->default('0000-00-00');
			$table->string('video_filename');
			$table->string('video_status',50)->default('PENDING APPROVAL');
			$table->string('video_source',50)->nullable();
			$table->string('video_source_url')->nullable();
			$table->string('video_url')->nullable();
			$table->string('video_thumbnail_url')->nullable();
			$table->string('facebook_url')->nullable();
			$table->string('twitter_url')->nullable();
			$table->string('hashtag')->nullable();
			$table->enum('accept_rules', array('yes','no'))->default('no');
			$table->enum('understand', array('yes','no'))->default('no');
			$table->enum('finalist', array('yes','no'))->default('no');
			$table->enum('winner', array('yes','no'))->default('no');

			$table->index('created_at');
			$table->index('email');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('entries');
	}

}
