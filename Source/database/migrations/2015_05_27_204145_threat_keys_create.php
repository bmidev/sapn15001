<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThreatKeysCreate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('threat_keys', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->index('created_at');
            $table->enum('type', array('domain', 'ip address'))->index();
            $table->smallInteger('threat_score')->unsigned()->default(0)->index();
            $table->string('target')->index();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        // Drop the table.
        Schema::dropIfExists('threat_keys');
	}

}
