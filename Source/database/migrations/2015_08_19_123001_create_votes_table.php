<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('votes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('entry_id')->unsigned()->index();
			$table->string('ip',15)->index();
			$table->string('twitter_name')->index();

			$table->foreign('entry_id')
				 ->references('id')
				 ->on('entries')
				 ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('votes');
	}

}
