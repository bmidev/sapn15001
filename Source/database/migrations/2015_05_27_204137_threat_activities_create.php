<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThreatActivitiesCreate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('threat_activities', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->timestamps();
            $table->index('created_at');
            $table->smallInteger('threat_score')->unsigned()->default(0)->index();
            $table->string('email_sanitized')->nullable()->index();
            $table->string('email_domain')->nullable()->index();
            $table->string('ip_address')->index();
            $table->enum('activity', array(
                'login', 'login failure',
                'registration', 'registration failure',
                'sweepstakes entry', 'sweepstakes entry failure',
                'change password', 'change password failure',
                'forgot password', 'forgot password failure',
            ))->index();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        // Drop the table.
        Schema::dropIfExists('threat_activities');
	}

}
