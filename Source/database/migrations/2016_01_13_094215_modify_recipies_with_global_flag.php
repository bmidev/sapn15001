<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyRecipiesWithGlobalFlag extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('recipes', function($table)
		{
			$table->boolean('only_global')->after('url')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('recipes', function($table)
		{
			$table->dropColumn(['only_global']);
		});
	}

}
