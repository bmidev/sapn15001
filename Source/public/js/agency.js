/*!
 * Start Bootstrap - Agency Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// The following variables are used to track the funnel
/*var enter_clicked = false;
var submit_birthday = false;
var browse_file = false;
var type_clicked = false;

var type_clicked = false;*/
var loading_more = false;
var submitting_entry = false;

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
		var tracking_attr = $(this).attr('data-tracker');
		if (typeof tracking_attr !== typeof undefined && tracking_attr !== false) {
			var tmp = $(this).attr('data-tracker').split(",");
			trackEvent(tmp[0], tmp[1], tmp[2]);
			if(tmp[2]=='Enter Contest'){
				trigger_floodlight_tracking('enter');
			}else if(tmp[2]=='Get Started'){
				trigger_floodlight_tracking('get_started_btn');	
			}
		}
		if(checkCookie('underage')){
			setCookie('underage',servertime,30);	
		}
		goto($(this).attr('href'));
        event.preventDefault();
    });

	$('a.ruleslnk, area').bind('click', function(event) {
		var tracking_attr = $(this).attr('data-tracker');
		if (typeof tracking_attr !== typeof undefined && tracking_attr !== false) {
			var tmp = tracking_attr.split(",");
			trackEvent(tmp[0], tmp[1], tmp[2]);
		}
		trigger_floodlight_tracking('rules');
		if(checkCookie('underage')){
			setCookie('underage',servertime,30);	
		}	
		show_popup('modal-rules');
        event.preventDefault();
    });

	$('a.learnhow').bind('click', function(event) {
		show_popup('modal-party');
        event.preventDefault();
    });

	$('a.global_privacy').bind('click',function(event) {
		show_popup('modal-policy');
        event.preventDefault();
    });

	$('a.cookieclose').bind('click', function(event) {
		$('#global_cookie').fadeOut();
		// Maybe send something to server to hide on refresh
		$.get( url+'/api/cookie-policy', function( data ) {});
		if(!$(this).hasClass('btn')){
			show_popup('modal-cookie-policy');
		}
        event.preventDefault();
    });
	$('a.cookiepolicyhere').bind('click', function(event) {
		$('#modal-policy').fadeOut( 500, function() {
			show_popup('modal-cookie-policy');
		});
	});

	$('a.videoplay').bind('click', function(event) {
		var tracking_attr = $(this).attr('data-tracker');
		if (typeof tracking_attr !== typeof undefined && tracking_attr !== false) {
			var tmp = tracking_attr.split(",");
			trackEvent(tmp[0], tmp[1], tmp[2]);
		}
		show_entry($(this).attr('data-player'), $(this).find('.img-responsive').first().attr('src'), $(this));
		if(checkCookie('underage')){
			setCookie('underage',servertime,30);	
		}
        event.preventDefault();
    });

	$('a.vote').bind('click', function(event) {
		send_vote($(this));
		send_tracking('https://analytics.twitter.com/i/adsct?txn_id=nts46&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0');
        event.preventDefault();
    });

	/*$('#howitworks_video .vjs-poster').bind('click', function(event) {
		var myPlayer = $('#howitworks_video_html5_api');
		var watched = 0;

		if (myPlayer.paused()) {
			console.log('Video was Paused');
			watched = (myPlayer.currentTime() / myPlayer.duration()) * 100;
			console.log(watched+'% of the video was watched based on '+myPlayer.currentTime()+' / '+myPlayer.duration()+' * 100');
		} else {
			console.log('Video is Playing');
		}
	});*/
	
	$("#howitworks_video_html5_api, #example1_video_html5_api, #example2_video_html5_api, #example3_video_html5_api").bind('ended', function(event){ 
		//console.log('Ended '+$(this).attr('id'));
		trackEvent('video', 'ended', $(this).attr('id'));
		trackEvent('video', $(this).attr('id'), '100%');
		//event.currentTarget.poster.show();
	});

	$("#howitworks_video_html5_api, #example1_video_html5_api, #example2_video_html5_api, #example3_video_html5_api").bind('play', function(event){ 
		//console.log('Playing');
		trackEvent('video', 'play', $(this).attr('id'));
	});
	
	$("#howitworks_video_html5_api, #example1_video_html5_api, #example2_video_html5_api, #example3_video_html5_api").bind('pause', function(event){ 
		//console.log('Paused');
		var watched = 0;
		trackEvent('video', 'paused', $(this).attr('id'));
		
		watched = (event.currentTarget.currentTime / event.currentTarget.duration) * 100;
		trackEvent('video', $(this).attr('id'), parseInt(watched)+'%');
		//console.log('Watched: '+parseInt(watched)+'%');
	});

	/*$('a.tweet').bind('click', function(event) {
		// Rewrite tweet model to include current name etc
		$('.tweetname').html($(this).attr('data-firstname').trim());
		var stripped = $(this).attr('data-firstname').trim().split(' ').join('');
		var hashtag = 'Vote'+stripped+'NutellaAmbContest';
		//$('.tweethashtag').html(stripped);
		//$('.tweetbtn').attr('href','https://twitter.com/intent/tweet?text='+encodeURIComponent('I\'m voting for '+$(this).attr('data-firstname')+' to be Chief Nutella Ambassador:')+'&url='+encodeURIComponent(url+'/entry/'+$(this).attr('data-id'))+'&hashtags='+encodeURIComponent(hashtag));
		$('.tweetbtn').attr('href','https://twitter.com/intent/tweet?hashtags='+encodeURIComponent(hashtag));

		//show_popup('modal-vote-twitter');
		//$('.tweetbtn').trigger('click');
		//$('.tweetbtn').click();
        //event.preventDefault();
    });*/

	$('button.navbar-toggle').bind('click', function(event) {
		if($('#bs-example-navbar-collapse-1').css('display') == 'block'){
			$('button.navbar-toggle').removeClass('openbtn');
		}else{
			$('button.navbar-toggle').addClass('openbtn');
		}
	});

	if($('#bs-example-navbar-collapse-1').css('display') == 'block'){
		$('button.navbar-toggle').addClass('openbtn');
	}

	$('a.example-link').bind('click', function(event) {
		var tracking_attr = $(this).attr('data-tracker');
		if (typeof tracking_attr !== typeof undefined && tracking_attr !== false) {
			var tmp = tracking_attr.split(",");
			trackEvent(tmp[0], tmp[1], tmp[2]);
		}
		if(checkCookie('underage')){
			setCookie('underage',servertime,30);	
		}
		show_popup($(this).attr('data-toggle'));
        event.preventDefault();
    });

	$('a.winner-link').bind('click', function(event) {
		var tracking_attr = $(this).attr('data-tracker');
		if (typeof tracking_attr !== typeof undefined && tracking_attr !== false) {
			var tmp = tracking_attr.split(",");
			trackEvent(tmp[0], tmp[1], tmp[2]);
		}
		if(checkCookie('underage')){
			setCookie('underage',servertime,30);	
		}
		show_popup($(this).attr('data-toggle'));
        event.preventDefault();
    });

	$('.dropdown-toggle').bind('click', function(event) {
		var tracking_attr = $(this).attr('data-tracker');
		if (typeof tracking_attr !== typeof undefined && tracking_attr !== false) {
			var tmp = tracking_attr.split(",");
			trackEvent(tmp[0], tmp[1], tmp[2]);
		}
	});

	/* Resets for if the browser has been refreshed and its the enter stage */
	if($('#fileInput_hidden').length>0){
		$('#dob_mm option').first().prop('selected');
		$('button[data-id="dob_dd"] .filter-option').html('DD');
		$('button[data-id="dob_mm"] .filter-option').html('MM');
		$('button[data-id="dob_yyyy"] .filter-option').html('YYYY');
		$('button[data-id="state"] .filter-option').html('State');
		$('#dob_form .dropdown-menu li').removeClass('selected');
		$('#enterform .dropdown-menu li').removeClass('selected');
		if($('#fileInput_hidden').val().length>0){
			$('#enterform .file_preview').html($('#fileInput_hidden').val()+' Uploaded').css('display','block');
		}
	}

	/* If they change the desktop mobile settings */
	$('.filterbox').bind('click', function(event) {
		var video_type = '';
		var opt = '';
		var current_video_type = $('#data-video-type').val().split(",");

		if($(this).hasClass('off')){
			$(this).removeClass('off');
			opt = 'add';
			video_type += $(this).attr('data-type');
		}else{
			if(current_video_type.length>1){
				$(this).addClass('off');
				opt = 'rm';
			}
		}

		if(opt.length>0){
			for(var i = 0; i < current_video_type.length; i++){
				if(current_video_type[i] != $(this).attr('data-type')){
					if(video_type.length>0){ video_type += ','; }
					video_type += current_video_type[i];
				}
			}

			$('#data-video-type').val(video_type);
			if(checkCookie('underage')){
				setCookie('underage',servertime,30);	
			}
			// Get the data, as the filter has been changed we empty the list and start again
			loading_more = true;
			load_data("api/videos",{ videolist: '', video_type: $('#data-video-type').val(), platform: 'desktop', _token: token }, reload_videos);
		}
	});

	/* If they change the mobile filter settings */
	$('#selectfilter').bind('change', function(event) {
		var video_type = '';
		var opt = '';
		var current_video_type = $('#data-video-type').val().split(",");
		var video_type = $('#selectfilter option:selected').val().length > 0 ? $('#selectfilter option:selected').val() : 'Friend,Artist,Adventurer,Gourmet,Purist';
		$('#data-video-type').val(video_type);
		if(checkCookie('underage')){
			setCookie('underage',servertime,30);	
		}

		// Get the data, as the filter has been changed we empty the list and start again
		loading_more = true;
		load_data("api/videos",{ videolist: '', video_type: $('#data-video-type').val(), platform: 'mobile', _token: token }, reload_videos);
	});

	/* Set up custom radioboxes */
	$('input.radio').iCheck({
		radioClass: 'iradio_square'
	});

	/* Set up custom checkboxes */
	$('input.checkbox').iCheck({
		radioClass: 'icheckbox_square'
	});

	/* Set up Accordian for footer FAQ */
	$('.mobilebar').bind('click', function(event) {
		if($(window).width()<=768){
			var accord_no = $(this).attr('data-cell');
			var link_clicked = $(this);
			$( 'ul.accord'+accord_no ).each(function( index ) {
				if($(this).css('display')=='block'){
					link_clicked.children('span').html('+').addClass('plus').removeClass('minus');
					$(this).slideUp();
				}else{
					link_clicked.children('span').html('-').addClass('minus').removeClass('plus');
					$(this).slideDown();
				}
			});
		}
    });

	/* Process file uploader */
	if($("div#dropzone").length>0){
		var uploaded_count = 0;
		var myDropzone = new Dropzone($("div.dropzone").get(0),{
			url: "/api/file_upload",
			previewsContainer: '',
			maxFiles: 1,
			clickable: ['#dropzone', '.orbrowse', '.uploadbtn'],
			maxfilesexceeded: function(file) {
				//console.log('remove using config: '+file);
				this.removeAllFiles(true);
				$.post( "api/file_replaced", { _token: $("#_token").val() }, "json")
				.done(function( json ) {
					myDropzone.addFile(file);
				});
			},
			success: function(original_file, response){
				//console.log('Config success: '+response.file);
				trackEvent('button', 'click', 'File Upload');
				$("#fileErr").html('');
				$("#fileInput_hidden").val(response.file);
				$('.file_preview').html(response.file+' uploaded').css('display','block');
			},
			error: function(original_file, response){
				var item_filename = typeof response.file !== "undefined" ? response.file : original_file;
				var response_msg = typeof response.msg !== "undefined" ? response.msg : 'a server error was encountered.';
				$('.file_preview').html('').css('display','none');
				$(".dz-progress").css('display','none');
				$("#fileErr").html('There was a problem uploading '+item_filename+' because '+response_msg);
			},
			init: function() {},
			addedfile: function(file) {},
			sending: function(file, xhr, formData) {
				// Show the total progress bar when upload starts
				formData.append("_token",  $("#_token").val());
				xhr.setRequestHeader('X-CSRF-Token', $("#_token").val());
				$(".dz-progress").css('opacity', "1").css('display','block');
	
				// And disable the items so people can't do another upload 
				//file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
			},
			uploadprogress: function(file, progress) {
				//console.log("File progress", progress);
				$(".dz-progress .dz-upload").css('width',progress + "%");
			},
			totaluploadprogress: function(file, progress) {
				/*$(".dz-progress .dz-upload").css('width',progress + "%");*/
			},
			queuecomplete: function(file) {
				$(".dz-progress").css('opacity',"0");
				$(".dz-progress").css('display','none');
			},
			maxFilesize: 20,
			addRemoveLinks: true,
  			acceptedFiles:".avi,.mov,.wmv,.mp4",
  			dictInvalidFileType:"Invalid file type. Only avi, mov, wmv and mp4 are supported."
		});
	}

	// Validates entry form submission.
    $("#dropzone-uploader input,#dropzone-uploader select,#dropzone-uploader textarea").not("[type=submit]").jqBootstrapValidation({
        bindEvents: ['blur','change'],
		preventSubmit: true,
        submitError: function($form, event, errors) {
            trigger_floodlight_tracking('final_submit');
			// additional error messages or events
			/*
			below goes to the first error it finds on the page
			for (var prop in errors) {
				break;
			}
			if($('#'+prop).length>0){
				goto('#'+prop);
			}*/
			$("#general_error_msg").fadeIn();
			goto('#erroranchor');
        },
        submitSuccess: function($form, event) {
            trigger_floodlight_tracking('final_submit');
			event.preventDefault(); // prevent default submit behaviour

			// Custom validation for form cheack
			var success = true;

			// Check the date field is set
			if(typeof $("#state option:selected") === "undefined" || $("#state option:selected").val().length==0){fld_error('state','You must enter a state');success='state';event.preventDefault();}else{fld_reset('state');}
		
			// Check the type field is set
			if (!$("input[name='entry_type']:checked").val()) {fld_error('type','You must enter a entry type');success='type';event.preventDefault();}else{ fld_reset('type'); }
		
			// Check Zip field is either 5 or 9 digits
			if($("#zip").val().length!=5 && $("#zip").val().length!=9){fld_error('zip','You must enter a valid zip');success='zip';event.preventDefault();}else{dob_reset('zip');}
		
			// File upload stuff
			if($("#fileInput").val().length==0 && $("#fileInput_hidden").val().length==0){fld_error('file','You must upload a file');success='file';event.preventDefault();}else{fld_reset('file');}
			
			//Checkboxes validation
			if (!$("input[id='agreeTo']:checked").val()) {fld_error('agreeTo','You must agree to the official rules');success='agreeTo';event.preventDefault();}else{ fld_reset('agreeTo');}
			if (!$("input[name='understand']:checked").val()) {fld_error('understand','You must acknowledge that you understand how your information will be used.');success='understand';event.preventDefault();}else{ fld_reset('understand');}

			/* Ended custom validation, if still valid send to server to validate */
			if( success === true){
				$.ajax({
					url: "/api/enter",
					type: "POST",
					dataType: "json",
					data: {
						entry_type: $("input[name='entry_type']:checked").val(),
						fileInput_hidden: $("#fileInput_hidden").val(),
						first_name: $("#first_name").val(),
						last_name: $("#last_name").val(),
						email: $("#email").val(),
						confirm_email: $("#confirm_email").val(),
						street1: $("#street1").val(),
						street2: $("#street2").val(),
						city: $("#city").val(),
						state: $("#state").val(),
						zip: $("#zip").val(),
						agreeTo: $("input[id='agreeTo']:checked").val(),
						understand: $("input[name='understand']:checked").val(),
						dob_mm:  $("#dob_mm").attr('data-option'), 
						dob_dd: $("#dob_dd").attr('data-option'),
						dob_yyyy: $("#dob_yyyy").attr('data-option'),
						_token: token
					},
					cache: false,
					success: function(response) {
						if(response.status === true){
							$('#enterform').fadeOut();
							$('#dob_form').fadeOut();
							$('#enter').attr('class','thanks').find('.col-lg-12.text-center').first().html('<h2 class="section-heading">Thanks for submitting your entry!</h2><p class="goodluck">We\'ll reveal our top five finalists on November 16, 2015. In the meantime, check out <a href="javascript:;" onClick="goto(\'#entries\')" class="white">Fan Video Submissions</a> below to see videos made by your fellow fans!</p>');
							goto('#enter');
						}else{
							var msg = 'Due to a server issue we were unable to complete your entry';
							$('#typeErr').html(msg);
							goto('#typeErr');
						}
					},
					error: function(response) {
						//statusCode, responseText, responseJSON, status, statusText
						if(response.responseJSON.status === false){
							var msg_list = response.responseJSON.msg;
							if (msg_list !== null && typeof msg_list === 'object') {
								for (var prop in msg_list) {
									
									var msg = eval('response.responseJSON.msg.'+prop);
									break;
								}
							}else{
								var msg = response.responseJSON.msg;
							}
						}else{
							var msg = 'Due to a server issue we were unable to complete your entry';
						}

						$('#typeErr').html(msg);
						goto('#typeErr');
						
					},
				})
			}else{
				$("#general_error_msg").fadeIn();
				goto('#erroranchor');
				//goto('#'+success+'Err');
			}
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });

	/*When clicking on Full hide fail/success boxes */
	$('#name').focus(function() {
		$('#success').html('');
	});

	/* No idea what this does as they all seem to override each other - review and remove later*/
	if($("#howitworks_video").length>0){
		videojs("howitworks_video").ready(function(){
			var myPlayer = this;
		});
		videojs("example1_video").ready(function(){
			var myPlayer = this;
		});
		videojs("example2_video").ready(function(){
			var myPlayer = this;
		});
		videojs("example3_video").ready(function(){
			var myPlayer = this;
		});
	}

	if($("#mission1_video").length>0){
		videojs("mission1_video").ready(function(){
			var myPlayer = this;
		});
		videojs("mission2_video").ready(function(){
			var myPlayer = this;
		});
		videojs("mission3_video").ready(function(){
			var myPlayer = this;
		});
	}

	if($("#winnermain_video").length>0){
		videojs("winnermain_video").ready(function(){
			var myPlayer = this;
		});
		videojs("winnersecondary_video").ready(function(){
			var myPlayer = this;
		});
	}

	/* Add swip listeners to mobile carousels */
	$("#myCarousel").swiperight(function() {
		$(this).carousel('prev');
	});
	$("#myCarousel").swipeleft(function() {
		$(this).carousel('next');
	});
	$("#myTimelineCarousel").swiperight(function() {
		$(this).carousel('prev');
	});
	$("#myTimelineCarousel").swipeleft(function() {
		$(this).carousel('next');
	});

	/* Track badge right clicks */
	$('#download_badge').mousedown(function(event) {
		switch (event.which) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				ga('send', 'event', 'download', 'image', 'Badge');
				break;
			default:
		}
	});

	/* Track right clicks content menu loading */
	/*$(document).on("contextmenu", "#download_badge", function(e){
	   ga('send', 'event', 'click', 'download', 'Badge');
	});*/
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

// Styles select fields
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
	$('.selectpicker').selectpicker('mobile');
}else{
	$('.selectpicker').selectpicker();	
}

// Sets the dob fields on change. (may not have been needed as it reload may have been causing error)
$('#dob_mm, #dob_dd, #dob_yyyy').change(function () {
	var select_fld = $(this);
	var selectedText = $(this).find("option:selected").text();
	select_fld.attr('data-option',selectedText);
});

// Vaildiate dob form
$(".submitbtn").click(function() {
	$('#enter .text-danger').html('');
	if($('#enterform').css('display') == 'block'){return;}
	if(typeof $("#dob_mm").attr('data-option') === "undefined" || $("#dob_mm").attr('data-option').length==0){dob_error('dob_mm','You must enter a dob month');return;}else{dob_reset('dob_mm');}
	if(typeof $("#dob_dd").attr('data-option') === "undefined" || $("#dob_dd").attr('data-option').length==0){dob_error('dob_dd','You must enter a dob day');return;}else{dob_reset('dob_dd');}
	if(typeof $("#dob_yyyy").attr('data-option') === "undefined" || $("#dob_yyyy").attr('data-option').length==0){dob_error('dob_yyyy','You must enter a dob year');return;}else{dob_reset('dob_yyyy');}

	trackEvent('button', 'click', 'Date of Birth Submit');
	trigger_floodlight_tracking('dob_submit');
	loading(true);
	$.post( "/api/dob_check", { dob_mm:  $("#dob_mm").attr('data-option'), dob_dd: $("#dob_dd").attr('data-option'), dob_yyyy: $("#dob_yyyy").attr('data-option'), _token: $("#_token").val() }, "json")
	  .done(function( json ) {
		if(json.status === true){
			$(".submitbtn").css({opacity: 1.0, visibility: "visible", cursor: "inherit"}).animate({opacity: 0}, 500);
			loading(false);
			$('#enterform').slideDown();
			//$('#enter').css('background-image','url(img/enter-arrows-open.png)').css('background-position','center center');
			$('#enter').addClass('down');
			goto('#enterform');
		}else{
			$.each(json.errors, function( index, value ) {
				var test = value.toString();
				if(test.substr(0,18)=='<strong>Sorry, you'){
					//console.log('Under 18 error');
					$(".submitbtn").css({opacity: 1.0, visibility: "visible", cursor: "inherit"}).animate({opacity: 0}, 500);
					$(".enter-arrows").html('');
					$("#enter").addClass('underage');

					//Add cookie
					setCookie('underage',servertime,30);
					document.cookie = "underage="+servertime;
				}
				dob_error(index,value);
			});
		}
	  })
	  .fail(function( jqxhr, textStatus, error ) {
		loading(false);
		var err = textStatus + ", " + error;
		alert( "Request Failed: " + err );
	});
});

//Validate Entry Form and if error cancel event.
$("#dropzone-uploader").submit(function( event ) {
	trackEvent('button', 'click', 'Entry Form Submit');
	$('#enterform .text-danger').html('');
});

// Vaildiate dob form
$(".morebtn").click(function() {
	if(loading_more === false){
		loading_more = true;
		load_data("/api/videos",{ videolist: used, video_type: $('#data-video-type').val(), _token: token }, load_more_videos);
	}else{
		alert('Loading in progress, please wait.');	
	}
});

/*$(".morerecipebtn").click(function() {			   
	load_data("/api/recipes",{ recipelist: recipes_used, _token: token }, load_more_recipes);
});*/

// Gets content from the server{
function load_data(send_to,data_to_send,callback){
	loading(true);
	$.ajax({
		url: send_to,
		type: "POST",
		dataType: "json",
		data: data_to_send,
		cache: false,
		success: function(response) {
			loading(false);
			if(response.status === true){
				callback (response);
			}else{
				var msg = 'Due to a server issue we were unable to retreive your data';
				loading_more = false;
			}
		},
		error: function(response) {
			loading(false);
			if(response.status === false){
				var msg_list = response.responseJSON.msg;
				for (var prop in msg_list) {
					var msg = eval('response.responseJSON.msg.'+prop);
					break;
				}
			}else{
				var msg = 'Due to a server issue we were unable to complete your entry';
			}
			loading_more = false;
		},
	});
}

// Callback for loading more videos
function load_more_videos(json){
	loading_more = false;
	used = json.videolist;
	for(var i=0; i<json.entries.length; i++){
		$( '<div class="col-xs-6 col-md-4 col-lg-5ths video"> <a href="#" data-player="'+json.entries[i]['video_url']+'" class="videoplay"><img src="'+json.entries[i]['video_thumbnail_url']+'" class="img-responsive" alt="Entry By '+json.entries[i]['first_name']+'"><img src="'+url+'/img/play_small.png" class="img-responsive play" alt="play"></a> </div>' ).appendTo( ".gallery" );
	}
	if(json.more === false){
		$(".morebtn").fadeOut();
	}else{
		$(".morebtn").fadeIn();
	}

	$('a.videoplay').unbind(); // Remove previous bind and add it on all videos
	$('a.videoplay').bind('click', function(event) {
		var tracking_attr = $(this).attr('data-tracker');
		var make_b
		if (typeof tracking_attr !== typeof undefined && tracking_attr !== false) {
			var tmp = tracking_attr.split(",");
			trackEvent(tmp[0], tmp[1], tmp[2]);
		}
		show_entry($(this).attr('data-player'), $(this).find('.img-responsive').first().attr('src'), $(this));
        event.preventDefault();
    });
}

// Callback for reloading video
function reload_videos(json){
	loading_more = false;
	used = json.videolist;
	var html = '';
	if(json.entries.length>0){
		for(var i=0; i<json.entries.length; i++){
			html += '<div class="col-xs-6 col-md-4 col-lg-5ths video"> <a href="#" data-player="'+json.entries[i]['video_url']+'" class="videoplay"><img src="'+json.entries[i]['video_thumbnail_url']+'" class="img-responsive" alt="Entry By '+json.entries[i]['first_name']+'"><img src="'+url+'/img/play_small.png" class="img-responsive play" alt="play"></a> </div>';
		}
	}else{
		for(var i=0; i<10; i++){
			html += '<div class="col-xs-6 col-md-4 col-lg-5ths novideo"><img class="img-responsive" alt="" src="'+url+'/img/novideo.png"></div>';
		}
		html += '<div class="novideobox"><div class="novideobanner"><p>No videos yet. Upload yours and you could be first!</p></div></div>';
	}
	
	$(".gallery").html(html+'<br class="clr">');
	if(json.more === false){
		$(".morebtn").fadeOut();
	}else{
		$(".morebtn").fadeIn();
	}

	$('a.videoplay').unbind(); // Remove previous bind and add it on all videos
	$('a.videoplay').bind('click', function(event) {
		var tracking_attr = $(this).attr('data-tracker');
		var make_b
		if (typeof tracking_attr !== typeof undefined && tracking_attr !== false) {
			var tmp = tracking_attr.split(",");
			trackEvent(tmp[0], tmp[1], tmp[2]);
		}
		show_entry($(this).attr('data-player'), $(this).find('.img-responsive').first().attr('src'), $(this));
        event.preventDefault();
    });
}

// Callback for loading more videos
/*function load_more_recipes(json){
	used = json.recipelist;
	for(var i=0; i<json.recipes.length; i++){
		$( '<div class="col-xs-6 col-md-3 recipe"> <a href="'+json.recipes[i]['url']+'" target="_blank"><img src="'+url+'/img/recipes/'+json.recipes[i]['image']+'" class="img-responsive" alt="'+json.recipes[i]['name']+'"></a> </div>' ).appendTo( ".recipesbox" );
	}
	if(json.more === false){
		$(".morerecipebtn").fadeOut();
	}else{
		$(".morerecipebtn").fadeIn();
	}
}*/

// Resizes the menu to be the height of the current window view
function getVisible() {
	if($(window).width()<=990){
		var footer_height = $('footer').css('position') == 'fixed' ? $('footer').height() : 0; //Works if footer is fixed or not.
		$('.navbar-collapse').css('max-height',$(window).height()-$('.navbar').height()-footer_height);
	}
}

// Resets the accordian if greater than 
function resetAccordian(){
	if($(window).width()>768){
		$( "a.mobilebar" ).each(function( index ) {
			$(this).children('span').html('+');
			$(this).parent().children('ul').removeAttr('style');
		});
	}
}

/* scrolls the page to an element on the page */
function goto(id){
	var offset = $('.navbar').height();
	$('html, body').stop().animate({
		scrollTop: $(id).offset().top-offset
	}, 1500, 'easeInOutExpo');
}

/* Triggers a loading modal */
function loading(show){
	if(show == true){
		// Show loading screen
	}else{
		// Remove loading screen
	}
}

/* Handles field errors */
function fld_error(field_errored,error_msg){
	if($('#'+field_errored).prop('tagName') == 'INPUT' && $('#'+field_errored).attr('type')=='checkbox'){
		// If its a checkbox, highlight parent text.
		$('#'+field_errored).parent().addClass('err');
	}else if($('#'+field_errored).prop('tagName') == 'SELECT' && $('#'+field_errored).hasClass('selectpicker')){
		// If its a select, highlight parent childrens .filter-option text.
		$('#'+field_errored).closest('.form-group').find('.filter-option:first').addClass('err');
	}else{
		// Its a normal field so just make its font red.
		$('#'+field_errored).addClass('err');
	}
	$('#'+field_errored+'Err').append(error_msg+'<br />').fadeIn();
}
function fld_reset(field_errored){
	if($('#'+field_errored).prop('tagName') == 'INPUT' && $('#'+field_errored).attr('type')=='checkbox'){
		// If its a checkbox, highlight parent text.
		$('#'+field_errored).parent().removeClass('err');
	}else if($('#'+field_errored).prop('tagName') == 'SELECT' && $('#'+field_errored).hasClass('selectpicker')){
		// If its a select, highlight parent childrens .filter-option text.
		$('#'+field_errored).closest('.form-group').find('.filter-option:first').removeClass('err');
	}else{
		// Its a normal field so just make its font red.
		$('#'+field_errored).removeClass('err');
	}
	$('#'+field_errored+'Err').html('');
}

/* Handles DOB of Errors */
function dob_error(field_errored,error_msg){
	$('button[data-id="'+field_errored+'"] .filter-option').css('color','red').addClass('err');
	$('#enter .text-danger').append(error_msg+'<br />').fadeIn();
}
function dob_reset(field_errored){
	$('button[data-id="'+field_errored+'"] .filter-option').css('color','inherit').removeClass('err');
}

/* Triggers a google analytic command */
function trackEvent(category, action, opt_label) {
	//alert('Triggering tracking event for '+opt_label);
	ga('send', 'event', category, action, opt_label);
}

/** opens a modal popup **/
function show_popup(div,closable){
	jQuery(function($) {
		if(div == undefined || div.length==0){ div='terms'; }
		if(typeof closable  === 'undefined' || closable.length==0 || closable != false){ closable = true; }

		var bg_div_id = closable === false ? 'fade2' : 'fade';
		if(closable === false && $('#'+div).find('a.close').length > 0){
			$('#'+div).find('a.close').remove();
		}else if(closable === true && $('#'+div).find('a.close').length == 0){
			$('#'+div).prepend('<a href="javascript:;" alt="Close" title="Close" class="close">X</a>');
		}
		
		$('#'+div).fadeIn();
	
		//Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
		var popMargLeft = (($( window ).width() - $('#'+div).width()) / 2);
	
		//Apply Margin to Popup
		$('#'+div).css({
			'left' : (popMargLeft>=0?popMargLeft:0)
		});
	
		//Fade in Background
		if ($("#"+bg_div_id).length == 0){
			//$('body').append('<div id="fade" style="z-index:200;"></div>');
			$('body').append('<div id="'+bg_div_id+'" style="z-index:9000;"></div>'); 
			$('#'+bg_div_id).css({'filter' : 'alpha(opacity=80)'}).fadeIn();
		}
	
		//Close Popups and Fade Layer event handle
		//$('a.close, #fade').on('mouseup', function() { 
		if(closable === true){
			$('a.close, #fade').mouseup(function() {
				close_modal();
				return false;
			});

		}

		$( window ).resize(function() {
			var popMargLeft = (($( window ).width() - $('#'+div).width()) / 2);
			//console.log(popMargLeft+' = '+(($( window ).width() - $('#'+div).width()) / 2)+' ('+$( window ).width()+' - '+$('#'+div).width()+') divide 2 - 27');
		
			//Apply Margin to Popup
			$('#'+div).css({
				'left' : popMargLeft
			});
		});
	});
	return false;
}

/** closes a modal popup **/
function close_modal(){
	jQuery(function($) {
		$('#fade, .modal_cont').fadeOut(function() {
			$('#fade').remove();
			// If a video is playing this will pause it.
			$('video').each(function() {
				$(this).get(0).pause();
			});
			$('a.vote.dynamic').unbind();
		});
	});
}

/*** Used to load an entry video and the modal associated ***/
function show_entry(video_to_load, thumbnail, jqueryLink){
	if($('#entry_video').length>0){ videojs("entry_video").dispose(); }
	$("#video_to_show").html('');
	$('#modal-entry').removeClass('withVote');
	$(document).off( "click", '#entry_video, .vjs-play-control' );
	$("#video_to_show").html('<video id="entry_video" class="video-js vjs-default-skin" controls preload="none" width="auto" height="auto" poster="'+thumbnail+'" data-setup="{  }"><source src="'+video_to_load+'" type="video/mp4" /><p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p></video>');
	if(typeof jqueryLink !== typeof undefined && jqueryLink.hasClass('voteplay')){
		$('#modal-entry').addClass('withVote');	
		var prefix = $('#modal-entry').hasClass('prewinner') ? 'see' : 'Follow';
		var name = jqueryLink.attr('data-firstname');
		var id = jqueryLink.attr('data-id');
		var fb_url = jqueryLink.attr('data-fb-url');
		var tw_url = jqueryLink.attr('data-tw-url');

		var stripped = name.trim().split(' ').join('');
		var hashtag = 'Vote'+stripped+'NutellaAmbContest';
		var milliseconds = new Date().getTime();
		var hashlink = 'https://twitter.com/intent/tweet?text='+encodeURIComponent(url+'/e/'+id+'/'+milliseconds+'/')+'&hashtags='+encodeURIComponent(hashtag);
		$( '<a href="#" data-vote-for="'+id+'" class="vote btn mobileOnly inlineblock dynamic">Vote for '+name+'</a><p class="mobileOnly">also vote on twitter with <a href="'+hashlink+'" class="tweet">#'+hashtag+'</a></p><h4 class="mobileOnly">'+prefix+' '+name+'\'s campaign</h4><a href="'+fb_url+'" target="_blank" class="fb_icon mobileOnly inlineblock"></a><a href="'+tw_url+'" target="_blank" class="tw_icon mobileOnly inlineblock"></a>' ).appendTo( "#video_to_show" );
		$('a.vote.dynamic').bind('click', function(event) {
			send_vote($(this));
			event.preventDefault();
		});
	}

	videojs("entry_video").ready(function(){
  		var myPlayer = this;
		var watched = 0;
		show_popup('modal-entry');

		jQuery(document).on('click', '#entry_video, .vjs-play-control', function(){
			if (myPlayer.paused()) {
				//console.log('Video was Paused');
				trackEvent('video', 'paused', video_to_load);
				watched = (myPlayer.currentTime() / myPlayer.duration()) * 100;
				trackEvent('video', video_to_load, watched+'%');
				//console.log(watched+'% of the video was watched based on '+myPlayer.currentTime()+' / '+myPlayer.duration()+' * 100');
			} else {
				//console.log('Video is Playing');
				trackEvent('video', 'play', video_to_load);
			}
		});
	});
}

function send_vote(item_clicked){
	$.ajax({
		url: "/api/vote"+stage,
		type: "POST",
		dataType: "json",
		data: {
			entry_id: item_clicked.attr('data-vote-for'),
			_token: token
		},
		cache: false,
		success: function(response) {
			if(response.status === true){
				show_popup('modal-vote-success');
			}else{
				if(response.msg.length>0){
					var msg = response.msg;
				}else{
					var msg = 'Due to a server issue we were unable to complete your vote';
				}
				//$('#typeErr').html(msg);
				//goto('#typeErr');
				$('#modal-vote-error p').html(msg);
				show_popup('modal-vote-error');
			}
		},
		error: function(response) {
			//statusCode, responseText, responseJSON, status, statusText
			try{
				if(response.responseJSON.status === false){
					var msg = response.responseJSON.msg;
					var errno = response.responseJSON.errno;
				}else{
					var msg = 'Due to a server issue we were unable to complete your vote';
				}
			}catch(err) {
				var msg = 'Due to a server issue we were unable to complete your vote';
			}

			//$('#typeErr').html(msg);
			//goto('#typeErr');
			if(errno == 001){
				show_popup('modal-vote-failed');
			}else{
				$('#modal-vote-error p').html(msg);
				show_popup('modal-vote-error');
			}
		},
	});
}

function send_tracking(url){
	$.get( url, function( data ) {});
	 fbq('track', 'CompleteRegistration'); 
}

function trigger_floodlight_tracking(event_id){
	var event_list = {dob_submit:"nutel002", enter:"nutel00", final_submit:"nutel003", get_started_btn:"nutel000", rules:"nutel001"};
	//$("#floodlight_event").html('var axel = Math.random() + "";var a = axel * 10000000000000;document.write(\'<iframe src="https://4319270.fls.doubleclick.net/activityi;src=4319270;type=world0;cat='+event_list[event_id]+';ord=\' + a + \'?" width="1" height="1" frameborder="0" style="display:none"></iframe>\');');
	var axel = Math.random() + "";
	var a = axel * 10000000000000;
	$('<iframe src="https://4319270.fls.doubleclick.net/activityi;src=4319270;type=world0;cat='+event_list[event_id]+';ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>').prependTo( "body" );
	trigger_facebook_tracking(event_id);
}

function trigger_facebook_tracking(event_id) {
	var event_list = {dob_submit:["6039726893880",'0.00'], enter:["6039726893880","0.00"], final_submit:["6039539833280","0.00"], get_started_btn:["6039726893880","0.00"], rules:["6039726893880","0.00"]};

	if(event_list[event_id][0] && event_list[event_id][0].length>0){
		var cd = {};
		cd.value = event_list[event_id][1];
		cd.currency = "USD";
		window._fbq.push(['track', event_list[event_id][0], cd]);
	}
}

function adjust_orientation_viewport(orientation){
	if(orientation==0 || orientation==180){
		//portrait
		$('#myViewport').attr('content','width=750');
	}else{
		//landscape
		$('#myViewport').attr('content','980');
	}
}

/* Function for setting the underage cookie */
function setCookie(cname, cvalue, exmins) {
    var d = new Date();
    d.setTime(d.getTime() + (exmins*60*1000));
    var expires = "expires="+d.toUTCString();
    //document.cookie = cname + "=" + cvalue + "; " + expires + ";";
	//alert(expires);
	document.cookie = "underage="+servertime;
	//alert('Setting cookie');
	//alert('Cookie contains: '+getCookie('underage'));
}

/* Function for getting the underage cookie */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
} 
//alert('Cookie contains: '+getCookie('underage'));

/* Function for checking if the underage cookie is set */
function checkCookie(cname) {
    var checkit=getCookie(cname);
    if (checkit!="") {
        return true;
    }
	return false;
} 

getVisible();
$( window ).resize(function() {
	getVisible();
	resetAccordian();
});