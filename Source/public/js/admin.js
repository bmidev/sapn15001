$(function() {

	// Entry Moderation - Approved button.
	$('#entries-list .status').on('click', '.approved', function() {
		var self = $(this);
		moderate('entries', $(this).attr('data-id'), 'approved').then(function(data) {
			if(data.success == true){
				self.closest('tr').remove();
			}else{
				alert(data.message);
			}
		}, function(data) {
			alert(data.responseJSON.message);
		});
	});

	// Phenom Moderation - Reject button.
	$('#entries-list .status').on('click', '.rejected', function() {
		var self = $(this);
		moderate('entries', $(this).attr('data-id'), 'rejected').then(function(data) {
			if(data.success == true){
				self.closest('tr').remove();
			}else{
				alert(data.message);
			}
		}, function(data) {
			alert(data.responseJSON.message);
		});
	});

	// Nominate Moderation - Approve button.
	$('#entries-list .finalist').on('click', '.approve', function() {
		var self = $(this);
		moderate('finalist', $(this).attr('data-id'), 'yes').then(function(data) {
			if(data.success == true){
				self.closest('.commands').removeClass('no').addClass('yes');
			}else{
				alert(data.message);
			}
		}, function(data) {
			alert(data.responseJSON.message);
		});
	});

	// Nominate Moderation - Reject button.
	$('#entries-list .finalist').on('click', '.reject', function() {
		var self = $(this);
		moderate('finalist', $(this).attr('data-id'), 'no').then(function(data) {
			if(data.success == true){
				self.closest('.commands').removeClass('yes').addClass('no');
			}else{
				alert(data.message);
			}
		}, function(data) {
			alert(data.responseJSON.message);
		});
	});

	// Nominate Moderation - Approve for Featured button.
	$('#entries-list .winner').on('click', '.approve', function() {
		var self = $(this);
		moderate('winner', $(this).attr('data-id'), 'yes').then(function(data) {
			if(data.success == true){
				self.closest('.commands').removeClass('no').addClass('yes');
			}else{
				alert(data.message);
			}
		}, function(data) {
			alert(data.responseJSON.message);
		});
	});

	// Nominate Moderation - Reject for Featured button.
	$('#entries-list .winner').on('click', '.reject', function() {
		var self = $(this);
		moderate('winner', $(this).attr('data-id'), 'no').then(function(data) {
			if(data.success == true){
				self.closest('.commands').removeClass('yes').addClass('no');
			}else{
				alert(data.message);
			}
		}, function(data) {
			alert(data.responseJSON.message);
		});
	});

	$('a.videoplay').on('click', function(event) {
		if($('#entry_video').length>0){ videojs("entry_video").dispose(); }
		$("#video_to_show").html('');

		$("#video_to_show").html('<video id="entry_video" class="video-js vjs-default-skin" controls preload="none" width="auto" height="auto" poster="'+$(this).find('.submission').first().attr('src')+'" data-setup="{  }"><source src="'+$(this).attr('data-player')+'" type="video/mp4" /><p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p></video>');

		$('#entryId').val($(this).attr('data-id'));
		$('#entryUrl').val($(this).attr('data-player'));

		videojs("entry_video").ready(function(){
			show_popup('modal-entry');
		});

		event.preventDefault();
	});
	
	$('#download_badge').mousedown(function(event) {
		switch (event.which) {
			case 1:
				console.log('left clicked badge');
				break;
			case 2:
				break;
			case 3:
				ga('send', 'event', 'click', 'download', 'Badge');
				console.log('right clicked badge');
				break;
			default:
				
		}
	});
	
	
})

/** opens a modal popup **/
function show_popup(div,closable){
	jQuery(function($) {
		if(div == undefined || div.length==0){ div='terms'; }
		if(typeof closable  === 'undefined' || closable.length==0 || closable != false){ closable = true; }

		var bg_div_id = closable === false ? 'fade2' : 'fade';
		if(closable === false && $('#'+div).find('a.close').length > 0){
			$('#'+div).find('a.close').remove();
		}else if(closable === true && $('#'+div).find('a.close').length == 0){
			$('#'+div).prepend('<a href="javascript:;" alt="Close" title="Close" class="close">X</a>');
		}
		
		$('#'+div).fadeIn();
	
		//Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
		var popMargLeft = (($( window ).width() - $('#'+div).width()) / 2) - 27;
	
		//Apply Margin to Popup
		$('#'+div).css({
			'left' : popMargLeft
		});
	
		//Fade in Background
		if ($("#"+bg_div_id).length == 0){
			//$('body').append('<div id="fade" style="z-index:200;"></div>');
			$('body').append('<div id="'+bg_div_id+'" style="z-index:99999;"></div>'); 
			$('#'+bg_div_id).css({'filter' : 'alpha(opacity=80)'}).fadeIn();
		}
	
		//Close Popups and Fade Layer event handle
		//$('a.close, #fade').on('mouseup', function() { 
		if(closable === true){
			$('a.close, #fade').mouseup(function() {
				close_modal();
				return false;
			});

		}

		$( window ).resize(function() {
			var popMargLeft = (($( window ).width() - $('#'+div).width()) / 2) - 27;
			//console.log(popMargLeft+' = '+(($( window ).width() - $('#'+div).width()) / 2)+' ('+$( window ).width()+' - '+$('#'+div).width()+') divide 2 - 27');
		
			//Apply Margin to Popup
			$('#'+div).css({
				'left' : popMargLeft
			});
		});
	});
	return false;
}

/** closes a modal popup **/
function close_modal(){
	jQuery(function($) {
		$('#fade, .modal_cont, .entry_data_modal').fadeOut(function() {
			$('#fade').remove();
			// If a video is playing this will pause it.
			$('video').each(function() {
				$(this).get(0).pause();
			});
		});
	});
}

function moderate(action_name, id, status) {
	if(action_name != 'entries' && action_name != 'finalist' && action_name != 'winner')
		return '';

	return $.get(BASE_URL+'/admin/entries/moderate_entries', {
		action: action_name,
		id: id,
		status: status
	});
}