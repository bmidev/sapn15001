$(function() {
    
     // Phenom Moderation - Approve button.
    $('#phenom-list').on('click', '.phenom-approve', function() {
        var self = $(this);
        moderate('phenom', $(this).attr('data-id'), 'approved').then(function() {
	        self.closest('.commands').removeClass('rejected').addClass('approved');
        });
    });
    
    // Phenom Moderation - Reject button.
    $('#phenom-list').on('click', '.phenom-reject', function() {
        var self = $(this);
        moderate('phenom', $(this).attr('data-id'), 'rejected').then(function() {
	        self.closest('.commands').removeClass('approved').addClass('rejected');
        });
    });
	
   // Nominate Moderation - Approve button.
    $('#nomination-list').on('click', '.moderate-approve', function() {
        var self = $(this);
        moderate('moderate', $(this).attr('data-id'), 'approved').then(function() {
	        self.closest('.commands').removeClass('rejected').addClass('approved');
        });
    });
    
    // Nominate Moderation - Reject button.
    $('#nomination-list').on('click', '.moderate-reject', function() {
        var self = $(this);
        moderate('moderate', $(this).attr('data-id'), 'rejected').then(function() {
	        self.closest('.commands').removeClass('approved').addClass('rejected');
        });
    });
	
	// Nominate Moderation - Approve for Featured button.
    $('#nomination-list').on('click', '.featured-approve', function() {
        var self = $(this);
        moderate('featured', $(this).attr('data-id'), 'approved').then(function() {
	        self.closest('.commands').removeClass('rejected').addClass('approved');
        });
    });
    
    // Nominate Moderation - Reject for Featured button.
    $('#nomination-list').on('click', '.featured-reject', function() {
        var self = $(this);
        moderate('featured', $(this).attr('data-id'), 'rejected').then(function() {
	        self.closest('.commands').removeClass('approved').addClass('rejected');
        });
    });
    
    // View image in a modal.
  	$("#phenom-list .submission, #nomination-list .submission").on("click", function() {
	  	var image_id = $(this).attr('data-id');
	  	$('#admin_image_preview').attr('src', $('#image_'+image_id).attr('src'));
  		$('#admin_image_modal').modal('show');
	});      
})

function moderate(action_name, id, status) {
	if(action_name != 'phenom' && action_name != 'moderate' && action_name != 'featured')
		return '';
	if(action_name == 'phenom')
	{
	    return $.get('nominators/moderate_phenom', {
	        nominator_id: id,
	        status: status
	    });		
	}
	else
	{
	    return $.get('entries/moderate_nomination', {
		    action: action_name,
	        nomination_id: id,
	        status: status
	    });
	}
}
//# sourceMappingURL=admin.js.map