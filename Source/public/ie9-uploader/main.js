/*
 * jQuery File Upload Code For IE8
 */

/* global $, window */
var check_count = 0;
$(function () {
	$(".uploadbtn").bind('click', function(event) {
		$("#fileInput").trigger("click");	
	});

	$("#fileInput").bind('change', function(event) {
		$('<iframe id="hidden_iframe" class="hidden" name="hidden_iframe" src="#"></iframe>').appendTo( $( "body" ) );
		$("#fileErr").html('');
		$(".dz-progress").css('display','block').html('Uploading...Please wait');
		$("#altsubmitbtn").trigger("click");
	});

});