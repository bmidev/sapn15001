<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Votes extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'votes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['entry_id', 'ip', 'twitter_name'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	// Used to check if an IP has already voted.
	public function scopeHaveVotedToday($query, $value, $field='ip'){
		$mytime = Carbon\Carbon::now();
		$mytime->format('Y-m-d');
		
		return $query->where($field,'=',$value)->where('created_at','>=',$mytime->format('Y-m-d').' 00:00:00')->where('created_at','<=',$mytime->format('Y-m-d').' 23:59:59');
	}

	public function scopeTwitterVote($query){
		return $query->where('ip','=','');
	}

	public function scopeWebsiteVote($query){	
		return $query->where('twitter_name','=','');
	}

	// Relationships
	/**
	 * A vote is made on a Entry
	 */
	public function entry()
	{
		return $this->belongsTo('App\Entries');
	}
}
