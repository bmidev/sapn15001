<?php namespace App\Http\Controllers;

use Response; // Means we can access the Response Class for returning content with a HTTP statuscode
use DB; // Means we can access the DB Class for making database calls
use Input; // Means we can reference the HTTP Inputs
use Config;
use Validator;
use App\Entries;
use App\Recipes;
use App\ThreatActivity;
use Brandmovers\Extension\ThreatManager;
use Request;
use Image;
use Session;

use Aws\Common\Enum\Region;
use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Brandmovers\Extension\SourceUploader;
use Jenssegers\Agent\Agent;

class APIController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| API Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles passing authenticated data for the application
	| back to the Frontend. For functions regarding Authentication you should
	| view the user controller which handles API calls such as register, login
	| & forgotten passwords.
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Stuff I want to do on all routes
		$this->token = csrf_token();
		
		$dates_to_check = Config::get('promotion.switchover_dates');
		
		// Works out which stage of the site we are in.
		$cur_time = time();
		if($cur_time >= strtotime($dates_to_check['winner_announced'])){
			$this->page = 'winner';
		}elseif($cur_time >= strtotime($dates_to_check['voting_ends'])){
			$this->page = 'pre-winner';
		}elseif($cur_time >= strtotime($dates_to_check['voting_starts'])){
			$this->page = 'finalists';
		}elseif($cur_time >= strtotime($dates_to_check['entry_ends'])){
			$this->page = 'pre-finalists';
		}elseif($cur_time >= strtotime($dates_to_check['launch'])){
			$this->page = 'entries';
		}else{
			$this->page = 'pre-launch';
		}

		//This should only be used for dev and testing purposes
		//if(env('APP_ENV', 'production') != 'production'){
			$valid_stages = array('winner','pre-winner','finalists','pre-finalists','entries','pre-launch');
			if(isset($_REQUEST['stage']) && in_array($_REQUEST['stage'],$valid_stages)){
				$this->page = $_REQUEST['stage'];
				//$_SESSION['stage_to_test'] = $_REQUEST['stage'];
				session(array('stage_to_test' => $_REQUEST['stage']));
			}elseif(isset($_REQUEST['stage']) && $_REQUEST['stage']=='clear'){
				session()->forget('stage');
			}elseif(strlen(session('stage_to_test'))>0 && in_array(session('stage_to_test'),$valid_stages)){
				$this->page = session('stage_to_test');
			}
			//$this->page = 'pre-finalists';
			//$this->page = 'finalists';
			//$this->page = 'pre-winner';
			//$this->page = 'winner';
		//}

		//Check if its a mobile
		$this->agent = new Agent();
	}

	/**
	 * Retrieve data for the Auction List (Landing) page.
	 *
	 * @return Response
	 */
	public function index()
	{

		return Response::json(array('status'=>false, 'msg'=>'Invalid '), 200);
	}

	/**
	 * Processes a date of birth check.
	 *
	 * @return Response
	 */
	public function check_dob()
	{
		// Get the data from the input
		if($this->page == 'entries'){
			$input = Input::all();
	
			// Rules for Dob Process
			$rules = array(
				'dob_mm' => 'required|numeric|min:1|max:12',
				'dob_dd' => 'required|numeric|min:1|max:31',
				'dob_yyyy' => 'required|integer|min:1900|max:'.date('Y').'|dob_is_valid'
			);
	
			//Validate it using the advanced validator
			$validator = Validator::make($input, $rules);
			if ($validator->fails())
			{
				return Response::json(array('status'=>false, 'errors' => $validator->messages()), 200);
			}
			
			// Get the current auction (could use elequant relationships but as first project playing it safe.
			//$auction = Entries::check_dob($user, $auction_id);
			//Request::session()->put('dob', $input['dob_yyyy'].'-'.$input['dob_mm'].'-'.$input['dob_dd']);
			return Response::json(array('status'=>true), 200);
		}
		return Response::json(array('status'=>true, 'msg'=>'Entry phase has now ended.'), 400);
	}

	/**
	 * Make a bid on an auction.
	 *
	 * @return Response
	 */
	public function file_upload()
	{
		if($this->page == 'entries'){
			// Get the data from the input
			$input = Input::all();

			if (!Input::hasFile('file') && !Input::hasFile('fileInput')) {
				if(isset($input['ie9'])){
					return view('partials.ie9_upload', array('status'=>false, 'file'=>'No video file was uploaded.'));
				}else{
					return Response::json(array('errors' => array('video_file' => 'No video file was uploaded.')), 401);
				}
			}else{
				$fileFieldToUse = Input::hasFile('file') ? 'file' : 'fileInput';	
			}

			if (Request::file($fileFieldToUse)->isValid()){
				$file = Input::file($fileFieldToUse);

				// If a previous file has been uploaded remove it, as we are replacing with this one
				if (Request::session()->has($fileFieldToUse)) {
					$request = Request::create('/api/file_replaced', 'POST', array());
				}

				if($file) {
					$filename = $file->getClientOriginalName();
					$prepend = time().'-'.rand(1,10000).'-';

					//Upload the video to AWS if the aws variables are set.
					//Format - project_id/environment/ugc-upload/
					if(Config::get('aws.enabled') === true){
						$uploader = new SourceUploader( Config::get('aws.access_key'), Config::get('aws.secret_key') );
						$break_cnt = 0;

						$sourceMediaBucket = Config::get('aws.source-media-bucket');
						$outputMediaBucket = Config::get('aws.output-media-bucket');
						if($sourceMediaBucket == $outputMediaBucket){
							$sourcePrefixAdd = 'source/';
							$outputPrefixAdd = 'output/';
						}else{
							$sourcePrefixAdd = '';
							$outputPrefixAdd = '';
						}
						$sourcePrefix = strtolower(Config::get('aws.project_id').'/'.env('APP_ENV', 'staging').'/ugc-upload/'.$sourcePrefixAdd);

						while($uploader->checkForExistingFile($sourceMediaBucket, $sourcePrefix.$prepend.$filename) && $break_cnt < 5){
							$prepend = time().'-'.rand(1,10000).'-';
							$break_cnt++;
						}

						if($break_cnt >= 5){
							if(isset($input['ie9'])){
								return view('partials.ie9_upload', array('status'=>false, 'file'=>'Due to a system issue there was a problem uploading your file, please try again.'));
							}else{
								return Response::json(array('status'=>false, 'msg'=>'Due to a system issue there was a problem uploading your file, please try again.'), 401);
							}
						}

						$videoSourceKey = $prepend.$filename;
	
						// Upload the file to S3.
						//$uploader = new SourceUploader( Config::get('aws.access_key'), Config::get('aws.secret_key') );
						$uploader->upload($file->getRealPath(), $sourceMediaBucket, $sourcePrefix.$prepend.$filename);

						$upload_check = $uploader->checkForExistingFile($sourceMediaBucket, $sourcePrefix.$prepend.$filename);
						if ($upload_check) {
							// It uploaded fine
							Request::session()->put('file', $prepend.$filename);

							if(isset($input['ie9'])){
								return view('partials.ie9_upload', array('status'=>true, 'file'=>$prepend.$filename));
							}else{
								return Response::json(array('status'=>true, 'file'=>$prepend.$filename), 200);
							}
						}else{
							if(isset($input['ie9'])){
								return view('partials.ie9_upload', array('status'=>false, 'file'=>'Failed to upload the file to the server'));
							}else{
								return Response::json(array('status'=>false, 'msg' => 'Failed to upload the file to the server'), 400);
							}
						}
					}else{
						$destinationPath = public_path() . '/uploads/';
						while(file_exists($destinationPath.$prepend.$filename)){
							$append = time().'-'.rand(1,10000).'-';
						}

						$upload_success = Input::file('file')->move($destinationPath, $prepend.$filename);

						if ($upload_success) {
							// Add it to the session
							Request::session()->put('file', $prepend.$filename);
			
							//Send back a response.
							if(isset($input['ie9'])){
								return view('partials.ie9_upload', array('status'=>true, 'file'=>$prepend.$filename));
							}else{
								return Response::json(array('status'=>true, 'file'=>$prepend.$filename), 200);
							}
						} else {
							return Response::json(array('status'=>false), 400);
						}
					}
				}
			}else{
				if(isset($input['ie9'])){
					return view('partials.ie9_upload', array('status'=>true, 'file'=>'Invalid file format supplied'));
				}else{
					return Response::json(array('status'=>false, 'msg'=>'Invalid file format supplied'), 401);
				}
			}

			if(isset($input['ie9'])){
				return view('partials.ie9_upload', array('status'=>false, 'file'=>'No file was uploaded'));
			}else{
				return Response::json(array('status'=>false, 'msg'=>'No file was uploaded'), 401);
			}
		}
		if(isset($input['ie9'])){
			return view('partials.ie9_upload', array('status'=>false, 'file'=>'Entry phase has now ended'));
		}else{
			return Response::json(array('status'=>true, 'msg'=>'Entry phase has now ended.'), 400);
		}
	}
	
	public function file_replaced()
	{
		if($this->page == 'entries'){
			if (Request::session()->has('file')) {
				$file = Request::session()->get('file');
				if(Config::get('aws.enabled') === true){
					$uploader = new SourceUploader( Config::get('aws.access_key'), Config::get('aws.secret_key') );
					$sourceMediaBucket = Config::get('aws.source-media-bucket');
					$outputMediaBucket = Config::get('aws.output-media-bucket');
					if($sourceMediaBucket == $outputMediaBucket){
						$sourcePrefixAdd = 'source/';
						$outputPrefixAdd = 'output/';
					}else{
						$sourcePrefixAdd = '';
						$outputPrefixAdd = '';
					}
					$sourcePrefix = strtolower(Config::get('aws.project_id').'/'.env('APP_ENV', 'staging').'/ugc-upload/'.$sourcePrefixAdd);
					if($uploader->checkForExistingFile($sourceMediaBucket, $sourcePrefix.$file)){
						$result = $uploader->delete($sourceMediaBucket,$sourcePrefix.$file); 
					}
				}else{
					$destinationPath = public_path() . '/uploads/';
					if(file_exists($destinationPath.$file)){
						unlink($destinationPath.$file);
					}
				}
				Request::session()->forget('file');
			}
			return Response::json(array('status'=>true), 200);
		}
		return Response::json(array('status'=>true, 'msg'=>'Entry phase has now ended.'), 400);
	}

	public function enter()
	{
		if ($this->page == 'entries' && Request::isMethod('post')) {
			$input = Input::all();

			// Rules for Dob Process
			$rules = array(
				'entry_type' => 'required|in:friend,artist,adventurer,gourmet,purist',
				'fileInput_hidden' => 'required',
				'first_name' => 'required|between:1,255',
				'last_name' => 'required|between:1,255',
				'email' => 'required|email|between:6,255',
				'confirm_email' => 'required|email|same:email',
				'street1' => 'required|between:1,255',
				'street2' => 'max:255',
				'city' => 'required|between:1,100',
				'state' => 'required|size:2|state_is_real',
				'zip' => 'required|zip_is_valid',
				'agreeTo' => 'required|in:true',
				'understand' => 'required|in:true',
				'dob_mm' => 'sometimes|required|numeric|min:1|max:12',
				'dob_dd' => 'sometimes|required|numeric|min:1|max:31',
				'dob_yyyy' => 'sometimes|required|integer|min:1900|max:'.date('Y').'|dob_is_valid'
			);

			//Validate it using the advanced validator
			$validator = Validator::make($input, $rules);
			if ($validator->fails())
			{
				//Error 
				return Response::json(array('status'=>false, 'msg'=>$validator->messages()), 400);
			}
			
			// Process the video file
			$file = $input['fileInput_hidden'];
			if(Config::get('aws.enabled') === true){
				$uploader = new SourceUploader( Config::get('aws.access_key'), Config::get('aws.secret_key') );
				$sourceMediaBucket = Config::get('aws.source-media-bucket');
				$outputMediaBucket = Config::get('aws.output-media-bucket');
				if($sourceMediaBucket == $outputMediaBucket){
					$sourcePrefixAdd = 'source/';
					$outputPrefixAdd = 'output/';
				}else{
					$sourcePrefixAdd = '';
					$outputPrefixAdd = '';
				}
				$sourcePrefix = strtolower(Config::get('aws.project_id').'/'.env('APP_ENV', 'staging').'/ugc-upload/'.$sourcePrefixAdd);
				if(!$uploader->checkForExistingFile($sourceMediaBucket, $sourcePrefix.$file)){
					return Response::json(array('status'=>false, 'msg'=>'There was a problem processing your entry, your video file could not be found, please try again.'), 400);
				}
			}else{
				$file_to_encode = public_path() . '/uploads/'.$file;
				if (!file_exists($file_to_encode)) {
					return Response::json(array('status'=>false, 'msg'=>'There was a problem processing your entry, your video file could not be found, please try again.'), 400);
				}
			}
			$video_url = '';

			$activity = new ThreatActivity(array(
            	'activity' => 'entry',
				'ip_address' => (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']),
			));
			$activity->setEmail($input['email']);
			
			$enter_check = \App\Entries::HaveEntered($input['email'])->count();
			// Look for any other entries for this user today.
			if($enter_check>0){
				$activity->setAsFailed();
				$activity->save();
				// Already entered error
				return Response::json(array('status'=>false, 'msg'=>'You have already entered this competition'), 400);
			}

			// Detect possible threat.
			$threatManager = new ThreatManager(Config::get('promotion.threatThreshold'));
			if (!$threatManager->analyze($activity)) {
				$activity->setAsFailed();
				$activity->save();
				// Suspecious Error
				return Response::json(array('status'=>false, 'msg'=>'Your entry has been flagged as suspicous'), 400);
			}

			// Create and save sweepstakes entry.
			$entry = new \App\Entries(array_merge($input,
				array(
					'type' => $input['entry_type'],
					'video_filename' => $input['fileInput_hidden'],
					'video_url' => $video_url, 
					'date_of_birth' => $input['dob_yyyy'].'-'.$input['dob_mm'].'-'.$input['dob_dd'],
					'accept_rules' => ($input['agreeTo']==true?'yes':'no'),
					'understand' => ($input['understand']==true?'yes':'no'),
				)
			));
			$entry->save();
			
			//Upload the video to AWS if the aws variables are set.
			//project_id/environment/ugc-upload/
			if(Config::get('aws.enabled') === true){
				$outputPrefix = strtolower(Config::get('aws.project_id').'/'.env('APP_ENV', 'staging').'/ugc-upload/'.$sourcePrefixAdd.$entry->id.'/');

				$tmp = explode(".",$input['fileInput_hidden']);
				$ext = array_pop($tmp);

				$videoOutputKey = 'video.mp4';

				// Create an Elastic Transcoder client
				$etClient = ElasticTranscoderClient::factory(array(
					'credentials' => [
						'key' => Config::get('aws.access_key'),
						'secret' => Config::get('aws.secret_key')
					],
					'version'=> '2012-09-25',
					'region' => 'us-east-1'
				));

				// Create a job.
				$etResponse = $etClient->createJob(array(
					'PipelineId' => Config::get('aws.et-pipeline-id'),
					'Input' => array(
						'Key' => $sourcePrefix.$file,
						'FrameRate' => 'auto',
						'Resolution' => 'auto',
						'AspectRatio' => 'auto',
						'Interlaced' => 'auto',
						'Container' => 'auto'
					),
					'UserMetadata' => array(    
						'project' => (string)Config::get('aws.project_id'),
						'environment' => (string)env('APP_ENV', 'staging'),
						'entry_id' => (string)$entry->id
					),
					'OutputKeyPrefix' => $outputPrefix,
					'Outputs' => array(array(
						'Key' => $videoOutputKey,
						'ThumbnailPattern' => 'thumbs/{count}',
						'Rotate' => 'auto',
						'PresetId' => Config::get('aws.et-pipeline-preset')
					))
				));

				// Set and save upload specific data.
				$entry->video_source_url = "https://".Config::get('aws.source-media-bucket').'.s3.amazonaws.com/'.$sourcePrefix.$file;
				$entry->video_url = "https://".Config::get('aws.output-media-bucket').'.s3.amazonaws.com/'.$outputPrefix.$videoOutputKey;
				$entry->video_thumbnail_url = "https://".Config::get('aws.output-media-bucket').'.s3.amazonaws.com/'.$outputPrefix.'thumbs/00001.png';
				$entry->video_source = 'UPLOAD';
				$entry->video_status = 'PENDING ENCODING';
				$entry->save();

				return Response::json(array('status'=>true, 'entry'=>(array)$entry), 200);
			}else{
				return Response::json(array('status'=>false, 'msg'=>'Due to a system error we were unable to process your video.'), 400);
			}

			// Save the activity - Do we need this?
			$activity->save();
		}

		return Response::json(array('status'=>false, 'msg'=>'Entry phase has now ended.'), 400);
	}

	// Get videos for the entries area
	public function getVideos(){
		$input = Input::all();
		$used[] = '';
		$return = array();

		//$start = isset($input['start']) ? $input['start'] : 0; //completely random so we just pull out next 10 excluding previous ones
		//$limit = isset($input['limit']) ? $input['limit'] : 10;

		//return print_r($input,1);
		$entries_count = \App\Entries::category($input['video_type'])->exclude($input['videolist'])->approved()->count();
		$random_str = Config::get('database.default') == 'mysql' || Config::get('database.default') == 'sqlsrv' ? "RAND()" : "RANDOM()";
		$rows_to_get = $this->agent->isMobile() && !$this->agent->isTablet() ? 4 : 10;
		$entries = \App\Entries::orderByRaw($random_str)->approved()->exclude($input['videolist'])->category($input['video_type'])->take($rows_to_get)->get();

		//cycle through the above and append them to video list via the used array
		foreach($entries as $row => $obj){
			$used[] = $obj['id'];
			$return[] = $obj;
		}

		return Response::json(array('status'=>true, 'entries'=>$return, 'more' => $entries_count>$rows_to_get?true:false, 'videolist' => $input['videolist'].implode(",",$used)), 200);
	}

	// Get videos for the entries area
	public function getRecipes(){
		$input = Input::all();
		$used[] = '';
		$return = array();

		//$start = isset($input['start']) ? $input['start'] : 0; //completely random so we just pull out next 10 excluding previous ones
		//$limit = isset($input['limit']) ? $input['limit'] : 10;

		//return print_r($input,1);
		$recipes_count = \App\Recipes::exclude($input['recipelist'])->count();
		$random_str = Config::get('database.default') == 'mysql' || Config::get('database.default') == 'sqlsrv' ? "RAND()" : "RANDOM()";
		$recipes = \App\Recipes::orderByRaw($random_str)->exclude($input['recipelist'])->take(4)->get();

		//cycle through the above and append them to video list via the used array
		foreach($recipes as $row => $obj){
			$used[] = $obj['id'];
			$return[] = $obj;
		}

		return Response::json(array('status'=>true, 'recipes'=>$return, 'more' => $recipes_count>4?true:false, 'recipelist' => $input['recipelist'].implode("~",$used)), 200);
	}

	// Make a vote for an entry
	public function vote(){
		if ($this->page == 'finalists' && Request::isMethod('post')) {
			$input = Input::all();
			
			// Get IP Address
			$request = Request::instance();
			//$request->setTrustedProxies(array('127.0.0.1')); // only trust proxy headers coming from the IP addresses on the array (change this to suit your needs)
			//$input['ip'] = $request->getClientIp();
			$input['ip'] = (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $request->getClientIp());
	
			// Check if the user has voted today 
			$vote_check = \App\Votes::HaveVotedToday($input['ip'])->count();
			if($vote_check >= 5){
				return Response::json(array('status'=>false, 'msg'=>'You have already voted 5 times today.', 'errno'=>'001'), 400);
			}
	
			$entry = new \App\Votes($input);
			$entry->save();
			
			return Response::json(array('status'=>true, 'msg'=>'Your vote was successul.'), 200);
		}
		
		return Response::json(array('status'=>false, 'msg'=>'Voting phase has now ended.', 'errno'=>'002'), 400);
	}
	
	//
	public function setCookiePolicyCookie(){
		session(array('cookie-policy-hidden' => true));
	}

	/*
    |--------------------------------------------------------------------------
    | Recieve Encoder Notificaton
    |--------------------------------------------------------------------------
    |
    | Recieve and process notification from the encoder (Elastic Transcoder).
    |
    |   Route::get('/sweepstakes/encode/notification', 'SweepstakesController@encodeNotification');
    |
    */

    // Transcoder Notifications
    public function encodeNotification() {
        // Accept the message.
        $message = Aws\Sns\MessageValidator\Message::fromRawPostData();
 
        // Handle subscription confirmation notifications.
		if ($message->get('Type') === 'SubscriptionConfirmation') {
			$client = new \Guzzle\Http\Client();
			$client->get($message->get('SubscribeURL'))->send();
			return;
		}

        // Handle normal notifications
        if ($message->get('Type') == 'Notification' && $message->get('Message') != NULL) {
            $notification = json_decode($message->get('Message'));
            $state = $notification->state;
            $key = $notification->input->key;
        } else {
            // Nothing valid found, so lets quit.
            return;
        }

        // Attempt to disassemble the key and validate the id.
        // list($env, $id) = explode("/", $key);
        //if ($id == NULL) return;
		$id = $notification->userMetadata->entry_id;
		if(env('APP_ENV', 'staging') != $notification->userMetadata->environment) return;
		if(Config::get('aws.project_id') != $notification->userMetadata->project) return;

        // Validate the state
        if ($state != 'COMPLETED' && $state != 'ERROR') return;

        // Get the registration so we can update it.
        $entry = \App\Entries::find($id);
        if ($entry == NULL) return;

        // Update the video status and save it.
        if ($state == 'COMPLETED')
            $entry->video_status = 'ENCODING SUCCESSFUL';
        else
            $entry->video_status = 'FAILED ENCODING';
        $reg->save();
    }
}
