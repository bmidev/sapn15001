<?php namespace App\Http\Controllers;

use Auth;
use Config;
use Input;
use Request;
use Redirect;
use Session;
use App\Entries;
use App\Votes;
use Jenssegers\Agent\Agent;
use Brandmovers\GeoIP\GeoIP;

class PageController extends Controller {

	/*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | This is the landing page.
    |
    |   Route::get('/', 'PageController@index');
    |   Route::post('/', 'PageController@index');
    |
    */

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		/*if(((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ===false){
			if(env('APP_ENV', 'production') == 'production'){
				header('Location: https://'.$_SERVER['HTTP_HOST'].'/');
			}
		}*/
		$dates_to_check = Config::get('promotion.switchover_dates');

		// Works out which stage of the site we are in.
		$cur_time = time();
		if($cur_time >= strtotime($dates_to_check['winner_announced'])){
			$this->page = 'winner';
		}elseif($cur_time >= strtotime($dates_to_check['voting_ends'])){
			$this->page = 'pre-winner';
		}elseif($cur_time >= strtotime($dates_to_check['voting_starts'])){
			$this->page = 'finalists';
		}elseif($cur_time >= strtotime($dates_to_check['entry_ends'])){
			$this->page = 'pre-finalists';
		}elseif($cur_time >= strtotime($dates_to_check['launch'])){
			$this->page = 'entries';
		}else{
			$this->page = 'pre-launch';
		}
		
		//This should only be used for dev and testing purposes
		//if(env('APP_ENV', 'production') != 'production'){
			$valid_stages = array('winner','pre-winner','finalists','pre-finalists','entries','pre-launch');
			if(isset($_REQUEST['stage']) && in_array($_REQUEST['stage'],$valid_stages)){
				$this->page = $_REQUEST['stage'];
				//$_SESSION['stage_to_test'] = $_REQUEST['stage'];
				session(array('stage_to_test' => $_REQUEST['stage']));
			}elseif(isset($_REQUEST['stage']) && $_REQUEST['stage']=='clear'){
				session()->forget('stage');
			}elseif(strlen(session('stage_to_test'))>0 && in_array(session('stage_to_test'),$valid_stages)){
				$this->page = session('stage_to_test');
			}
			//$this->page = 'pre-finalists';
			//$this->page = 'finalists';
			//$this->page = 'pre-winner';
			//$this->page = 'winner';
		//}

		//Check if its a mobile
		$this->agent = new Agent();

		//Check if its US or Global
		$this->geoip['country_code'] = 'US';
		if($this->page == 'winner' && !is_array(session('geoip') || count(session('geoip'))==0)){
			$geoip = new GeoIP();
			$this_geoip = $geoip->Data();
			if($this_geoip!==false){
				$this->geoip = $this_geoip;
			}
			session(array('geoip' => $this->geoip));
		}elseif($this->page == 'winner'){
			$this->geoip = session('geoip');
		}

		if(isset($_REQUEST['geo'])){
			$this->geoip['country_code'] = $_REQUEST['geo'];
		}

		$this->version = ((isset($_REQUEST['version']) && $_REQUEST['version'] == 1) || (!isset($_REQUEST['version']) && Config::get('promotion.version') == 1)) ? 1 : 2;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index($entry_id='')
	{
		// Get the CSRF token.
		$token = csrf_token();

		// Populates the Entries section.
		$entries_count = \App\Entries::approved()->count();
		$random_str = Config::get('database.default') == 'mysql' || Config::get('database.default') == 'sqlsrv' ? "RAND()" : "RANDOM()";

		$rows_to_get = $this->agent->isMobile() && !$this->agent->isTablet() ? 4 : 10;
		$entries = \App\Entries::approved()->orderByRaw($random_str)->take($rows_to_get)->get();
		$more = $entries_count > 10 ? true : false;
		$underage = false;
		$used = array();
		foreach($entries as $key => $val){
			$json = json_decode($val,true);
			$used[] = $json['id'];
		}

		// Need to pull cookie stuff out and check its not more than a day old
		//print_r($_COOKIE);
		if(isset($_COOKIE['underage'])){
			if($_COOKIE['underage'] === true){
				//Removes cookie if legacy code
				setcookie("underage", "", time() - 3600);
				$underage = true;
			}else{
				//echo date('r',$_COOKIE['underage']).' = '.date('r',$_COOKIE['underage']+7200).' < '.date('r',time()).'<br />';
				if($_COOKIE['underage']+7200<time()){
					setcookie("underage", "", time() - 3600);
				}else{
					$underage = true;	
				}
			}
		}

		// List of variables to pass into the views
		$variables = ['token' => $token, 'page' => $this->page, 'entries' => $entries, 'more' => $more, 'client_id' => '', 'used' => implode(",",$used), 'underage' => $underage, 'is_mobile' => $this->agent->isMobile(), 'is_tablet' => $this->agent->isTablet(), 'country_code' => $this->geoip['country_code'], 'version' => $this->version, 'show_cookie' => (session('cookie-policy-hidden')===true?false:true)];

		$uri = Request::path();

		// If the user has come straight to their entry
		if(strlen($entry_id)>0){
			$show_entry = \App\Entries::find($entry_id);

			if(strlen($show_entry)>0){
				$variables['show_entry'] = $show_entry;
			}
		}elseif($uri == 'rules'){
			$variables['show_rules'] = true;
		}

		if ($this->page === 'finalists' || $this->page === 'pre-winner'){
			// Get the finalists rows
			$finalists = array();
			$finalists_list = \App\Entries::finalists()->orderByRaw($random_str)->skip(0)->take(5)->get();
			foreach($finalists_list as $key => $obj){ $finalists[] = $obj; }
			$variables['finalists'] = $finalists;
		}elseif ($this->page === 'winner'){
			// Get the winning entry.
			$winner = array();
			$winning_item = \App\Entries::Winner()->get();
			foreach($winning_item as $key => $obj){ $winner = $obj; }
			//If no winner could be found create an empty row so it does not error.
			if(count($winner)==0){ $winner = array('first_name' => '', 'last_name' => '', 'winner_biog'=> '', 'winner_image1'=> '', 'winner_image2'=> '', 'winner_video'=> ''); }
			$variables['winner'] = $winner;
			
			// Get the recipes
			if($this->geoip['country_code'] == 'US' || $this->version==1){
				$num_to_get = 4;
				$recipes = \App\Recipes::orderByRaw($random_str)->take($num_to_get)->WithOutGlobal()->get();
				$recipes_count = \App\Recipes::WithOutGlobal()->count();
			}else{
				$num_to_get = 8;
				$recipes = \App\Recipes::orderByRaw($random_str)->take($num_to_get)->get();
				$recipes_count = \App\Recipes::count();
			}

			/*$recipes_more = $recipes_count > $num_to_get ? true : false;*/
			$recipes_more = $this->page == 'winner' && isset($this->geoip['country_code']) && $this->geoip['country_code'] !== 'US' ? false : true;
			$recipes_used = array();
			foreach($recipes as $key => $obj){
				$recipes_used[] = $obj['id'];
			}

			$variables['recipes'] = $recipes;
			$variables['recipes_more'] = $recipes_more;
			$variables['recipes_used'] = $recipes_used;
		}
		return view('index', $variables);
	}

	public function redirect()
	{
		header('Location: '.url());
		exit;
	}

	public function approve_email()
	{
		return view('emails.template', array('message'=>'test'));
	}
}
