<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

use App\Entries;
use App\Votes;

class DashboardController extends Controller {

	/*
	|---------------------------------------------------------------------------
	| Admin Dashboard Controller
	|---------------------------------------------------------------------------
	|
	| Primary admin controller displaying dashboard stats
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Admin dashboard
	 *
	 * @param Request $request
	 * @return Response
	 */

	public function index(Request $request)
	{
		$totalEntries    	 = Entries::count();
		
		$pendingEntries		 = Entries::pending()->count();
		$approvedEntries	 = Entries::approved()->count();
		$rejectedEntries	 = Entries::rejected()->count();
		
		$entries = Entries::finalists()->get();

		return view('admin.dashboard.index', [
			'totalEntries'			=> $totalEntries,
			'pendingEntries' 		=> $pendingEntries,
			'approvedEntries'		=> $approvedEntries,
			'rejectedEntries'		=> $rejectedEntries,
			'finalists'				=> $entries,
		]);
	}
	
}
