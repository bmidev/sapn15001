<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

use App\Phenom;
use App\Nomination;

class NominatorsController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Nominators Photo Moderation
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		// Get Nominators
		//$nominators = Phenom::whereNotNull('photo_url')->orderBy('created_at', 'DESC')->paginate(25);
		$nominators = Phenom::isRegistered()->hasPhoto()->orderBy('created_at', 'DESC')->paginate(25);
		return view('admin.nominators.index', [
			'nominators' => $nominators,
		]);
	}
	
	public function moderate_phenom(Request $request)
	{
		$nominator_id = $request->input('nominator_id');
		$status = $request->input('status');
		
		$phenom = Phenom::findOrFail($nominator_id);
		if($phenom) {
			
			if(!in_array($status, array('approved', 'rejected')))
			{
				return response()->json([
					'success' 	=> false,
					'message' 	=> "Invalid selection"
				]);
			}
			
			$phenom->admin_moderation = $status;
			$phenom->admin_moderated_at = date('Y-m-d H:i:s');
			$phenom->save();			
			
			return response()->json([
				'success' 	=> true
			]);
		}
		
		return response()->json([
			'success' 	=> false,
			'message' 	=> "Couldn't find media"
		]);
	}

}
		