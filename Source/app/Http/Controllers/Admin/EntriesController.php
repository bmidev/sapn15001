<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Mail;
use Config;
use Response;
use File;

use App\Entries;
use Brandmovers\Extension\SourceUploader;

class EntriesController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Nominations Moderation
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		// Get Nominations
		if($request->input('show')=='approved'){
			$entries = Entries::approved()->orderBy('created_at', 'DESC')->paginate(25);
			$show = 'approved';
		}elseif($request->input('show')=='rejected'){
			$entries = Entries::rejected()->orderBy('created_at', 'DESC')->paginate(25);
			$show = 'rejected';
		}elseif($request->input('show')=='finalists'){
			$entries = Entries::finalists()->paginate(25);
			$show = 'finalists';
		}elseif($request->input('show')=='winners'){
			$entries = Entries::winner()->paginate(25);
			$show = 'winners';
		}else{
			$entries = Entries::pending()->orderBy('created_at', 'DESC')->paginate(25);
			$show = 'pending';
		}

		return view('admin.entries.index', [
			'entries' => $entries,
			'show' => $show,
		]);
	}

	public function download_entry(Request $request)
	{
		$url = $request->input('entry_url');
		$tmp = explode("/",$url);
		$filename = array_pop($tmp);

		/*$uploader = new SourceUploader( Config::get('aws.access_key'), Config::get('aws.secret_key') );
		$sourceMediaBucket = Config::get('aws.source-media-bucket');
		$outputMediaBucket = Config::get('aws.output-media-bucket');
		if($sourceMediaBucket == $outputMediaBucket){
			$sourcePrefixAdd = 'source/';
			$outputPrefixAdd = 'output/';
		}else{
			$sourcePrefixAdd = '';
			$outputPrefixAdd = '';
		}

		$sourcePrefix = strtolower(Config::get('aws.project_id').'/'.env('APP_ENV', 'staging').'/ugc-upload/'.$sourcePrefixAdd);

		$response = $uploader->download($filename, $sourcePrefix);
		print_r($response);*/
		//$path = app_path('public_path');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		//curl_setopt($ch, CURLOPT_WRITEFUNCTION, $callback);
		$response = curl_exec($ch);
		
		$path = public_path();

		$file = $path.'/uploads/'.$request->input('entry_id').'mp4';
		File::put($file, $response);

		$headers = array(
			'Content-Type: application/octet-stream'
		);
        return Response::download($file, $request->input('entry_id').'mp4', $headers);
	}

	public function moderate_entries(Request $request)
	{
		$action = $request->input('action');
		$record_id = $request->input('id');
		$status = $request->input('status');

		$entry = Entries::findOrFail($record_id);
		if($entry)
		{
			if((!in_array($status, array('approved', 'rejected')) && $action == 'entries') || (!in_array($status, array('yes', 'no')) && ($action == 'finalist' || $action == 'winner')))
			{
				return response()->json([
					'success' 	=> false,
					'message' 	=> "Invalid action selected"
				],400);
			}

			if($action == 'winner')
			{
				if($entry->finalist == 'no'){
					return response()->json([
						'success' 	=> false,
						'message' 	=> "The entry must be a finalist to be marked as the winning entry."
					],400);
				}

				$winner_test = Entries::winner()->count();
				if($status == 'yes' && $winner_test>0){
					return response()->json([
						'success' 	=> false,
						'message' 	=> "You can only have one winner, to mark this entry as a winner mark the current winning entry as a non-winner."
					],400);
				}
				$entry->winner = $status;
				$entry->save();
			}
			else if($action == 'finalist')
			{
				if($entry->winner == 'yes'){
					return response()->json([
						'success' 	=> false,
						'message' 	=> "The entry can not be the winner unless its a finalist, to make this a non-finalist you must also make it the non-winner."
					],400);
				}

				$finalist_test = Entries::Finalists()->count();
				if($status == 'yes' && $finalist_test>=5){
					return response()->json([
						'success' 	=> false,
						'message' 	=> "You can only mark 5 entries as finalists."
					],400);
				}
				$entry->finalist = $status;
				$entry->save();
			}
			else
			{
				$entry->status = $status;
				$entry->save();

				if($status == 'rejected'){
					$email_view = 'emails.entryRejected';
					$variables = array('link'=>str_replace('/admin/entries/moderate_entries','',$request->url()).'/rules');
					$subject = 'Bummer. we couldn\'t approve your video :(';
				}else{
					$email_view = 'emails.entryApproved';
					$variables = array('link'=>str_replace('/admin/entries/moderate_entries','',$request->url()).'/entry/'.$entry->id);
					$subject = 'Yay! your video has been approved.';
					
					//To send the rejected email move the below code outside the else statement.
					Mail::send($email_view, $variables, function($message) use ($entry, $subject)
					{
						$message->from(Config::get('promotion.mailFromAddress'), Config::get('promotion.mailFromName'));
						$message->to($entry->email, $entry->first_name.' '.$entry->last_name);
						$message->subject($subject);
					});
				}
			}

			return response()->json([
				'success' 	=> true
			]);
		}

		return response()->json([
			'success' 	=> false,
			'message' 	=> "The record you are trying to ".$status." could not be found."
		],400);
	}
}
		