<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Brandmovers\Social\Post;


class ProcessController extends Controller {

	/*
	|---------------------------------------------------------------------------
	| Admin Process Controller
	|---------------------------------------------------------------------------
	|
	| Handles the processing of social posts into entries 
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Show the processing handler screen
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		// Testing...
		//\DB::connection()->enableQueryLog();
		//\OpenTable\Social\Seed::generateFake(1);
		//die();
		
		//\OpenTable\Social\Post::processEntries();

		//$queries = \DB::getQueryLog();
		//echo "<pre>";
		//var_dump($queries);
		//die();
		
		$unprocessedCount = Post::unprocessed()->count();
		
		return view('admin.process.index', [
			'unprocessedCount'	=> $unprocessedCount
		]);
	}
	
	
	public function run() {
		
		$batchSize = 1000;
		$valid = 0;
		$invalid = 0;

		// Run a batch
		$total = Post::processEntries($batchSize, $valid, $invalid);
		$remaining = Post::unprocessed()->count();

		return response()->json([
			'total'		=> $total,
			'valid'		=> $valid,
			'invalid'	=> $invalid,
			'remaining' => $remaining
		]);

	}



}