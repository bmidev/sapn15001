<?php

// This just builds an asset from env variables
function ass($path)
{
	$path = $path[0] == '/' ? $path : '/'.$path;

	return getenv('ASSETS_DIR') . $path . '?v=' . getenv('CACHE_VERSION');
}

// This is shorthand for using the ass function just for images
function img($path)
{
	$path = $path[0] == '/' ? $path : '/'.$path;

	return getenv('ASSETS_DIR') . '/img'. $path . '?v=' . getenv('CACHE_VERSION');
}

// global CDN link helper function
function cdn( $asset ){

    // Verify if KeyCDN URLs are present in the config file
    if( !Config::get('promotion.cdn') )
        return asset( $asset );

    // Get CDN URL
    $cdn = Config::get('promotion.cdn');

	// If a cdn is not set for this environment, return asset
    if(!isset($cdn[env('APP_ENV', '')]) || strlen($cdn[env('APP_ENV', '')])==0)
    	return asset( $asset );

	// Return CDN URL instead
    return (substr($cdn[env('APP_ENV', '')],0,2)=='//' || substr($cdn[env('APP_ENV', '')],0,4)=='http'?"":"//") . $cdn[env('APP_ENV', '')].$asset;

}