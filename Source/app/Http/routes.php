<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$middleware = array();

// If we were going to have add, edit, delete etc we can use 'resource' to merge all routes for that route into 1 line

// Load the landing page.
Route::match(array('GET', 'POST'), '/', array('as' => 'landing', 'uses' => 'PageController@index'));
Route::match(array('GET', 'POST'), '/rules', array('uses' => 'PageController@index'));
Route::match(array('GET', 'POST'), '/entry/{entry_id}', array('uses' => 'PageController@index'));
Route::match(array('GET', 'POST'), '/e/{entry_id}/{timestamp}', array('uses' => 'PageController@redirect'));
if(env('APP_ENV', 'production') != 'production'){
	Route::match(array('GET', 'POST'), '/approve_email', array('uses' => 'PageController@approve_email'));
}

// API Routes.
Route::post('/api/dob_check', array('middleware' => $middleware, 'uses' => 'APIController@check_dob'));
Route::post('/api/file_upload', array('middleware' => $middleware, 'uses' => 'APIController@file_upload'));
Route::post('/api/file_replaced', array('middleware' => $middleware, 'uses' => 'APIController@file_replaced'));
Route::post('/api/enter', array('middleware' => $middleware, 'uses' => 'APIController@enter'));
Route::post('/api/videos', array('middleware' => $middleware, 'uses' => 'APIController@getVideos'));
Route::post('/api/vote', array('middleware' => $middleware, 'uses' => 'APIController@vote'));
Route::post('/api/recipes', array('middleware' => $middleware, 'uses' => 'APIController@getRecipes'));
Route::get('/api/cookie-policy', array('middleware' => $middleware, 'uses' => 'APIController@setCookiePolicyCookie'));

// AWS Routes
Route::post('/api/encodeNotification', array('middleware' => $middleware, 'uses' => 'APIController@encodeNotification'));

// Dashboard Routes
Route::group(['middleware' => 'auth'], function()
{
	Route::get('admin', 'DashboardController@index');
});

// CMS Panel
Route::group(['namespace' => 'Admin'], function() {

	Route::get('admin/dashboard', [
		'as'			=> 'admin.dashboard',
		'uses' 			=> 'DashboardController@index',
		'middleware' 	=> 'auth'
	]);

	Route::get('admin/entries', [
		'as'			=> 'admin.entries',
		'uses' 			=> 'EntriesController@index',
		'middleware' 	=> 'auth'
	]);

	Route::get('admin/entries/moderate_entries', [
		'as'			=> 'admin.moderate',
		'uses' 			=> 'EntriesController@moderate_entries',
		'middleware' 	=> 'auth'
	]);

	Route::post('admin/entries/download_entry', [
		'as'			=> 'admin.download',
		'uses' 			=> 'EntriesController@download_entry',
		'middleware' 	=> 'auth'
	]);

	Route::get('admin/process-social-posts', [
		'as' => 'admin.process-social-posts',
		'uses' => 'ProcessController@run',
		'middleware' => 'auth'
	]);

	Route::controller('admin', 'AdminAuthController');
});