<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class ThreatActivity extends Model {

	protected $threatValues = array();

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'threat_activities';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes that can be mass assigned.
	 *
	 * @var array
	 */
	protected $fillable = array(
		'activity', 'email_domain', 'email_sanitized', 'ip_address', 'threat_score', 'user_id',
	);

	public function __construct($attributes = array()) {
		$this->threatValues = array(
            'entry' => 25,
            'entry failure' => 50,
            'vote' => 5,
            'vote failure' => 25,
        );

        parent::__construct($attributes);
	}

	/**
	 * Activity mutator.
	 */
	public function setActivityAttribute($value)
	{
		$this->attributes['activity'] = trim(strtolower($value));
        $this->attributes['threat_score'] = $this->threatValues[$this->activity];
	}

	/**
	 * Email Domain mutator.
	 */
	public function setEmailDomainAttribute($value)
	{
		$this->attributes['email_domain'] = trim(strtolower($value));
	}

	/**
	 * Email Sanitized mutator.
	 */
	public function setEmailSanitizedAttribute($value)
	{
		$this->attributes['email_sanitized'] = trim(strtolower($value));
	}

	/**
	 * IP Address mutator.
	 */
	public function setIpAddressAttribute($value)
	{
		$this->attributes['ip_address'] = trim(strtolower($value));
	}

	/**
	 * Email address mutator.
	 */
	public function setEmail($value)
	{
        // Split the user and host.
        list($user, $full_host) = explode("@", $value);

        // Remove anything following + in the user.
        if (substr_count($user, "+") > 0) {
            $user_parts = explode("+", $user);
            $user = $user_parts[0];
        }

        // Remove any . from the user.
        $user = str_replace(".", "", $user);

        // Check for subdomain host. Remove if not .co.uk.
        if (substr_count($full_host, '.') > 1 && substr($full_host, -6) != '.co.uk') {
            $host_parts = explode('.', $full_host);
            $shortened_host = $host_parts[(sizeof($host_parts) - 2)].'.'.$host_parts[(sizeof($host_parts) - 1)];
        } else {
            $shortened_host = $full_host;
        }

        // Set the sanitized email and email host.
        $this->attributes['email_sanitized'] = $user.'@'.$full_host;
        $this->attributes['email_domain'] = $shortened_host;
	}

	/**
	 * Set the activity as failed.
	 */
	public function setAsFailed()
	{
		if (substr($this->activity, -7) != 'failure')
			$this->activity .= ' failure';
	}
}
