<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Entries extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'entries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
		'first_name', 'last_name', 'email', 'type', 'video_filename', 'video_url', 
		'street1', 'street2', 'city', 'state', 'zip', 'date_of_birth', 'accept_rules',
		'accept_rules', 'understand'
	];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
	
	/* Scopes */
	
	// Used to check if a email has already entered.
	public function scopeHaveEntered($query, $email){
		//DB::table('auctions')->where('startAt','<=',date('Y-m-d H:i:s'))->where('endAt','>',date('Y-m-d H:i:s'))->orderBy('endAt', 'asc');
		return $query->where('email','=',$email)->where('status','<>','rejected');
	}

	// Used to get approved entries.
	public function scopeApproved($query){
		return $query->where('status','=','approved');
	}

	// Used to get pending entries.
	public function scopeRejected($query){
		return $query->where('status','=','rejected');
	}

	// Used to check if a email has already entered.
	public function scopePending($query){
		return $query->where('status','=','pending');
	}

	// Used to get users flagged as finalists
	public function scopeFinalists($query){
		return $query->where('finalist','=','yes');
	}

	// Used to get users flagged as the winner
	public function scopeWinner($query){
		return $query->where('winner','=','yes');
	}

	// Used to exclude entries from the entries request
	public function scopeExclude($query,$excludeStr){
		return strlen($excludeStr)>0 ? $query->whereNotIn('id', explode(",",$excludeStr)) : $query; //may have to be a string
	}

	// Used to exclude entries from the entries request
	public function scopeCategory($query,$categoriesStr){
		return $query->whereIn('type', explode(",",$categoriesStr)); //may have to be a string
	}

	// Relationships
	/**
	 * An Entry can have many votes
	 */
	public function votes()
	{
		return $this->hasMany('App\Votes', 'entry_id', 'id');
	}
}
