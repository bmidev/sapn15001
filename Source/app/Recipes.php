<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Recipes extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'recipes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/* Scopes */

	// Used to exclude entries from the entries request
	public function scopeExclude($query,$excludeStr){
		return strlen($excludeStr)>0 ? $query->whereNotIn('id', explode("~",$excludeStr)) : $query; //may have to be a string
	}

	// Used to include only recipies flaged as not as global
	public function scopeWithOutGlobal($query){
		return $query->where('only_global','=','0');
	}

	// Used to include only recipies flaged as global
	public function scopeWithGlobal($query){
		return $query->where('only_global','=','1');
	}
}
