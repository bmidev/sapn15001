<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class ThreatKey extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'threat_keys';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes that can be mass assigned.
	 *
	 * @var array
	 */
	protected $fillable = array('target', 'threat_score', 'type');

	/**
	 * Target mutator.
	 */
	public function setTargetAttribute($value)
	{
		$this->attributes['target'] = trim(strtolower($value));
	}

	/**
	 * Threat Level mutator.
	 */
	public function setThreatLevelAttribute($value)
	{
		$this->attributes['threat_level'] = trim(strtolower($value));
	}

	/**
	 * Type mutator.
	 */
	public function setTypeAttribute($value)
	{
		$this->attributes['type'] = trim(strtolower($value));
	}

}
