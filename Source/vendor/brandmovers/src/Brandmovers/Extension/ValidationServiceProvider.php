<?php namespace Brandmovers\Extension;

use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Boot up the provider.
	 *
	 * @return AdvancedValidator
	 */
	public function boot()
	{
		$this->app->validator->resolver(function($translator, $data, $rules, $messages)
	    {
			return new AdvancedValidator($translator, $data, $rules, $messages);
	    });
	}

}
