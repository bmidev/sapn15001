<?php namespace Brandmovers\Extension;

use App\ThreatActivity;
use App\ThreatKey;
use Carbon\Carbon;

class ThreatManager {
    protected $threatScore;
	protected $threshold;

	public function __construct($threshold)
	{
		$this->threshold = $threshold;
        $this->threatScore = 0;
	}

    /**
     * Analyzes the activity. Returns false if the threat is too high and true
     * if the threat is acceptable.
     *
     * @return boolean
     */
	public function analyze($activity)
	{
        // Check for duplicate email address today.
        // We do this early to save all the other check as it's not necessary if this is a duplicate.
        $midnight = new Carbon('midnight');
        if ($activity->activity == 'entry') {
            $duplicate = ThreatActivity::where('email_sanitized', '=', $activity->email_sanitized)
                ->where('created_at', '>=', $midnight)
                ->where('activity', '=', 'entry')
                ->first();
            if ($duplicate) {
                // We need to fail the threat check if we hit a duplicate.
                $this->threatScore = $this->threshold;
                $activity->user_id = $duplicate->user_id;
                return false;
            }
        }

        // Define the threat window.
        $threatWindow = new Carbon('-1 hour');

		// Check the threat keys for the ip address.
        $ipResult = ThreatKey::where('target', '=', $activity->ip_address)->first();
        if (!empty($ipResult)) {
            $ipThreat = $ipResult->threat_score;
        } else {
            $ipThreat = $activity->threat_score;
        }
        $this->threatScore += $ipThreat;

        // Check activity by ip within the threat window.
        if ($ipThreat) {
            $this->threatScore += ThreatActivity::where('created_at', '>=', $threatWindow)
                ->where('ip_address', '=', $activity['ip_address'])
                ->sum('threat_score');
        }

        // Check the threat keys for the domain.
        $domainResult = ThreatKey::where('target', '=', $activity->email_domain)->first();
        if (!empty($domainResult)) {
            $domainThreat = $domainResult->threat_score;
        } else {
            $domainThreat = $activity->threat_score;
        }
        $this->threatScore += $domainThreat;

        // Check activity by domain within the threat window.
        if ($domainThreat) {
            $this->threatScore += ThreatActivity::where('created_at', '>=', $threatWindow)
                ->where('email_domain', '=', $activity->email_domain)
                ->sum('threat_score');
        }

        return $this->isAcceptable();
	}

    /**
     * Returns false if the threat is too high and true if the threat is acceptable.
     *
     * @return boolean
     */
    public function isAcceptable()
    {
        return ($this->threatScore < $this->threshold) ? true : false;
    }

}