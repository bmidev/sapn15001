<?php namespace Brandmovers\Extension;

use Illuminate\Validation\Validator;
use Config;

class AdvancedValidator extends Validator {
 
	public function validateStateIsReal($attribute, $value, $parameters, $validator)
	{
		$states = Config::get('promotion.valid_states');

		return (in_array($value, $states)) ? true : $validator->errors()->add('state', 'You must enter a valid state');
	}

	public function validateZipIsValid($attribute, $value, $parameters, $validator)
	{
		// Remove all characters except digits.
		$pattern = '/[\D]/';
		$value = preg_replace($pattern, '', $value);

		// Check that 10 digits remain
		if (strlen($value) != 5 && strlen($value) != 9) {
			$validator->errors()->add('zip', 'Zip must be either 5 or 9 numbers in length');
		}

		return true;
	}

	public function validateDobIsValid($attribute, $value, $parameters, $validator)
	{
		$input = $this->data;
		$dob = strtotime($input['dob_yyyy'].'-'.$input['dob_mm'].'-'.$input['dob_dd']);
		if(!checkdate($input['dob_mm'],$input['dob_dd'],$input['dob_yyyy'])){
			$validator->errors()->add('dob_yyyy', 'Date of birth must be a real date');
		}elseif($dob > strtotime('-18 years')){
			$validator->errors()->add('dob_yyyy', '<strong>Sorry, you’re unable to participate in the contest :(</strong><br />Check out <a href="javascript:;" onClick="goto(\'#entries\')">Fan Video Submissions</a> to see what your fellow Nutella fans have been making!');
		}

		return true;
	}
}