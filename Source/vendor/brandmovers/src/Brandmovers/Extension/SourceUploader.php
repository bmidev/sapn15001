<?php namespace Brandmovers\Extension;

use Aws\S3\S3Client;
use Aws\Common\Enum\Region;
use Config;

class SourceUploader {
    
    private $s3Client;

    /*
    |--------------------------------------------------------------------------
    | Constructor
    |--------------------------------------------------------------------------
    |
    | Construct a source uploader object. Basically does initial configurations.
    |
    */

    public function __construct($aws_access_key, $aws_secret_key) {
        $this->s3Client = S3Client::factory(array(
            'credentials' => [
				'key'    => $aws_access_key,
            	'secret' => $aws_secret_key
			],
			'version'=> '2006-03-01',
			'region' => 'us-east-1'
        ));
    }

    public function upload($source, $bucket, $destination) {
		$result = $this->s3Client->putObject(array(
			'Bucket'     => $bucket,
			'Key'        => $destination,
			'SourceFile' => $source,
			'Metadata'     => array(
				'project' => Config::get('aws.project_id'),
				'environment' => env('APP_ENV', 'staging')
			)
		));

        // Wait till the object put is complete.
		$this->s3Client->waitUntil('ObjectExists', array(
			'Bucket' => $bucket,
			'Key'    => $destination
		));
       /* $this->s3Client->waitUntilObjectExists(array(
          'Bucket' => $bucket,
          'Key'    => $destination
        ));*/
    }

	public function download($file, $bucket) {
		$result = $this->s3Client->getObject(array(
			'Bucket'     => $bucket,
			'Key'        => $file,
			/*'file' => $source,*/
		));

		return $result;

        // Wait till the object put is complete.
		/*$this->s3Client->waitUntil('ObjectExists', array(
			'Bucket' => $bucket,
			'Key'    => $destination
		));*/
    }

	public function delete($bucket, $destination) {
        $this->s3Client->deleteObject(array(
			'Bucket'     => $bucket,
			'Key'        => $destination
		));
    }

    public function uploadWithContentType($source, $bucket, $destination, $contentType) {
        $result = $this->s3Client->putObject(array(
          'Bucket'     => $bucket,
          'Key'        => $destination,
          'SourceFile' => $source,
          'ContentType' => $contentType
          )
        );

        // Wait till the object put is complete.
		$this->s3Client->waitUntil('ObjectExists', array(
			'Bucket' => $bucket,
			'Key'    => $destination
		));
        /*$this->s3Client->waitUntilObjectExists(array(
          'Bucket' => $bucket,
          'Key'    => $destination
        ));*/
		
		return $result;
    }

	public function checkForExistingFile($bucket, $destination) {
		$this->s3Client->registerStreamWrapper();
		if($keyExists = file_exists("s3://$bucket/$destination")){
			return true;
		}else{
			return false;	
		}
    }
}