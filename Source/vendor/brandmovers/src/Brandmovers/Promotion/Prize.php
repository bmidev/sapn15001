<?php namespace Brandmovers\Promotion;

use Illuminate\Database\Eloquent\Model;
use Brandmovers\Promotion\Period;
use Carbon\Carbon;


class Prize extends Model {

	const PRIZE_711_GRAND = "7/11 Grand Prize";
	const PRIZE_711_SECONDARY = "7/11 $15 Gift Card";
	const PRIZE_SWEETARTS_DAILY = "SweeTarts Daily Winner";

	
	/**
	 * Get Prizes for Select Input Options
	 * 
	 * [
	 * 		[prize_id] = prize_name
	 * ]
	 * 
	 * @return array
	 */
	public static function getSelectOptions($canGenerate = false) {
		$options = [];
		
		if( $canGenerate ) {
			// Get only prizes you can generate
			$prizes = self::where('can_generate', true)->get();
		}
		else {
			// Get all prizes
			$prizes = self::all();
		}
		
		foreach($prizes as $prize) {
			$options[$prize->id] = $prize->name;
		}
		
		return $options;
	}

}	