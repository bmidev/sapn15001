<?php namespace App\Brandmovers\Charts;

use Illuminate\Support\Facades\View;

/**
 * This class generates a pie chart.
 *
 * @exception
 * @author Brad Estey
 * @package BMI Component
 */

class SortableTable {

	private $data = array();
	private $options;

	public function __construct($data = FALSE, $options = FALSE) {

		// Set default options.
		$this->options['chartId'] = 'my_sortable_table';
		$this->options['perPage'] = 8;
		$this->options['width'] = 314;
		$this->options['height'] = 295;

		// Set options.
		if (is_array($options)) {
			foreach($options as $key => $value) {
				if (array_key_exists($key, $this->options)) { $this->options[$key] = $value; }
			}
		}

		// Set data.
		if ($data) { $this->data = $data; }
	}

	// ==============================
	// ! Accessors and Manipulators
	// ==============================

	// Get Raw Chart Data
	public function getData() {
	    if (is_array($this->data)) { return $this->data; }
	    else { return FALSE; }
	}

	// Set Chart Data
	public function setData() {
		if ($data) { $this->data = $data; }
	}

	// Humanize numbers with commas and such.
	public function humanizedData() {
		$data = array();
		foreach($this->data as $key => $value) {
			if (is_numeric($value) && $value == intval($value)) {
				$data[$key] = number_format($value, 0, '.', ',');
			}
		}
		$data = $data + $this->data;
		return $data;
	}

	// Get Chart Display Options
	public function getOptions() {
	    if (is_array($this->options)) { return $this->options; }
	    else { return FALSE; }
	}

	// Set Chart Display Options
	public function setOptions($options = FALSE) {
		if (is_array($options)) {
			foreach($options as $key => $value) {
				if (array_key_exists($key, $this->options)) { $this->options[$key] = $value; }
			}
		}
	}

	// Get Chart Data by Key.
	public function __call($name, $parameters) {
		$name = preg_replace('/^get/','',$name);

		if (array_key_exists($name, $this->data)) {
			return $this->data[$name];
		}

		// lcfirst() doesn't work on all our servers.. :(
		$name = strtolower(substr($name, 0, 1)) . substr($name, 1);

		if (array_key_exists($name, $this->data)) {
			return $this->data[$name];
		}

		if (array_key_exists($name, $this->options)) {
			return $this->options[$name];
		}

		return FALSE;
	}

	// Count Total Rows in Data Array.
	public function getCount() {
	    if (is_array($this->data)) { return count($this->data); }
	    else { return FALSE; }
	}


	// Calculate Total of Values in Data Array.
	public function total() {
	    $total = 0;
	    foreach($this->data as $key => $value) {
	    	$total = $total + $value;
	    }
	    return $total;
	}

	// Generate HTML to show chart.
	public function generateChart() {
		return View::make('admin.charts.sortable_table', array('data' => $this));
	}

	// Encode Chart Labels into a pipe delimited string
	public function encodeChartLabels() {
		$data = '';
		foreach($this->data as $key => $value) {
			$data .= '|' . $key;
		}
		return substr($data, 1);
	}

	// Encode Chart Values into a comma delimited string
	public function encodeChartValues() {
		$data = '';
		foreach($this->data as $key => $value) {
			$data .= ',' . $value;
		}
		return substr($data, 1);
	}

	public function generateHead() {
		$alpha = range('A', 'Z');
		$count = 0;
		$raw_data = $this->getData();
		$str = '';

		foreach($raw_data[0] as $key => $value) {
			if (is_numeric($value)) { $type = 'number'; }
			else { $type = 'string'; }

			if ($type == 'string') { $key = addslashes($key); }

			$str .= ',{id:\'' . $alpha[$count] . '\',label:\'' . $key . '\',type:\'' . $type . '\'}';
			$count++;
		}

		return substr($str, 1);
	}

	public function getType($string) {
		if (is_numeric($string)) { return 'number'; }
		return 'string';
	}

	// Get Chart ID -- clean: convert to alphanumeric, lowercase and replace spaces with underscores.
	public function getChartId($clean = FALSE) {
	    if (!$clean) { return $this->options['chartId']; }
	    else { return $this->options['chartId']; }
	}

}