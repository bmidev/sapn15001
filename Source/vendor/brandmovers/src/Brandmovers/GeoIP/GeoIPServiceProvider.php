<?php namespace Brandmovers\GeoIP;

use Illuminate\Support\ServiceProvider;
use Brandmovers\GeoIP\GeoIP;

class GeoIPServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        //
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
		$this->app->bind('GeoIP', function ($app) {
			return new GeoIP;
		});
    }

}