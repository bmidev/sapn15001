To use this Service Provider simply copy the following into the config/app.php

Brandmovers\GeoIP\GeoIPServiceProvider::class,

then simply call

$geoip = new GeoIP();
$this->geoip = $geoip->Data();

It will then return an array:

