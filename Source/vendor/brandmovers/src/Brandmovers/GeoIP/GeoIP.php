<?php namespace Brandmovers\GeoIP;

use GuzzleHttp\Client;

class GeoIP extends GeoIPServiceProvider {

	function __construct(){
		
	}

    /**
     * Get the IP address of the current use.
     *
     * @return array
     */
    public function getIP()
    {
        static $ip;
        $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

        return $ip;
    }

    /**
     * Retrieve ip data from FreeGeoIP.
     *
     * @return array
     */
    public function getGeoDataFromFreeGeoIP()
    {
		//echo 'Get data from: http://freegeoip.net/json/'.$this->getIP().'<br />';
		$client = new Client();
		try {
			$request = $client->request('GET','http://freegeoip.net/json/'.$this->getIP());
			if($request->getStatusCode()==200){
				$json = json_decode($request->getBody(), true);
				if(isset($json['country_code']) && strlen($json['country_code'])>0){
					return $json;
				}
			}
		} catch (\GuzzleHttp\Exception\ClientException $e) {
			
		}
		return false;
    }

    /**
     * Retrieve ip data from IP2C.
     *
     * @return array
     */
    public function getGeoDataFromIP2C()
    {
		//echo 'Get data from: http://ip2c.org/'.$this->getIP().'<br />';
		$client = new Client();
		try {
		$request = $client->request('GET','http://ip2c.org/'.$this->getIP());
			if($request->getStatusCode()==200){
				$csv = str_getcsv($request->getBody(), ";");
				if(isset($csv[0]) && $csv[0]==1){
					return json_decode('{"ip":"'.$this->getIP().'","country_code":"'.$csv[1].'","country_name":"'.$csv[3].'","region_code":"","region_name":"","city":"","zip_code":"","time_zone":"","latitude":0,"longitude":0,"metro_code":0}', true);
				}
			}
		} catch (\GuzzleHttp\Exception\ClientException $e) {
			
		}
		return false;
    }

	/**
     * Retrieve the geoIp data.
     *
     * @return array
     */
    public function Data()
    {
		/*if(($data = $this->getGeoDataFromFreeGeoIP())!==false){
       		return $data;
		}elseif(($data = $this->getGeoDataFromIP2C())!==false){
			return $data;
		}elseif(($data = $this->getGeoDataFromDatabase())!==false){
			return $data;
		}*/
		return false;
    }

	/**
     * Retrieve the geoip data from a local database
     *
     * @return array
     */
    public function getGeoDataFromDatabase()
    {
		return false;
	}
}
