<?php namespace Brandmovers\Twitter;

use Illuminate\Database\Eloquent\Model;

class TwitterPost extends Model {

	// #########################################################################
	// PROPERTIES
	// #########################################################################

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'twitter_posts';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that can be mass assigned.
	 *
	 * @var array
	 */
	protected $fillable = [
		'twitter_id', 'user_id', 'user_name', 'user_display_name', 
		'message', 'retweet', 'geo_latitude', 'get_longitude',
		'image_url', 'data', 'theyretheone', 'hestheone', 'shestheone',
		'thebestest', 'xoxo', 'sofantastic', 'thefreshest', 'stuckinourheads',
		'youresofancy', 'sohappy', 'thatsmyjam', 'move', 'hittheroad',
		'musicalmashup', 'cool', 'a_5sos', 'a_arianagrande', 'a_taylorswift13',
		'h_5secondsofsummer', 'h_arianagrande', 'h_beamiller', 'h_beckyg',
		'h_charlixcx', 'h_demilovato', 'h_directioners', 'h_echosmith',
		'h_edenxo', 'h_edsheeran', 'h_fifthharmony', 'h_jacoblatimore',
		'h_harmonizers', 'h_maroon5', 'h_meghantrainor', 'h_mkto', 'h_nickjonas',
		'h_ollymurs', 'h_onedirection', 'h_pharrellwilliams', 'h_r5', 'h_r5family',
		'h_sabrinacarpenter', 'h_selenagomez', 'h_shawnmendes', 'h_sheppard',
		'h_taylorswift', 'h_thevamps', 'h_zendaya', 'day_posted', 'hour_posted'
	];

	// #########################################################################
	// ACCESSORS AND MUTATORS
	// #########################################################################

    // Populate the object using an Object from the Twitter Search Client.
	public function importFromTwitter($tweet) {
        $this->created_at = date('Y-m-d H:i:s', strtotime($tweet->created_at));
        $this->imported_at = date('Y-m-d H:i:s');
        $this->twitter_id = $tweet->id_str;
        $this->status = 'APPROVED';
        $this->entry = 'NO';
        $this->user_id = $tweet->user->id_str;
        $this->user_name = $this->replace4ByteCharacters($tweet->user->screen_name);
        $this->user_display_name = $this->replace4ByteCharacters($tweet->user->name);
        $this->message = $this->replace4ByteCharacters($tweet->text);
        $this->retweet = ($this->determine_if_retweet($tweet)) ? 'YES' : 'NO';
        $this->geo_latitude = (isset($tweet->coordinates->coordinates[1])) ? $tweet->coordinates->coordinates[1] : NULL;
        $this->geo_longitude = (isset($tweet->coordinates->coordinates[0])) ? $tweet->coordinates->coordinates[0] : NULL;
        $this->media_url = isset($tweet->entities->media[0]->media_url) ? $tweet->entities->media[0]->media_url : NULL;
        $this->data = $this->replace4ByteCharacters(json_encode($tweet));
        $this->day_posted = date('l', strtotime($tweet->created_at));
        $this->hour_posted = date('H', strtotime($tweet->created_at));


        // location clean up.
        if ($this->geo_latitude == 0 && $this->geo_longitude == 0) {
        	$this->geo_latitude = NULL;
        	$this->geo_longitude = NULL;
        }

        // Fill in hash tag optimization columns.
        $message = strtolower($tweet->text);
        $this->theyretheone = (strpos($message, '#theyretheone') !== FALSE) ? 'YES' : 'NO';
        $this->hestheone = (strpos($message, '#hestheone') !== FALSE) ? 'YES' : 'NO';
        $this->shestheone = (strpos($message, '#shestheone') !== FALSE) ? 'YES' : 'NO';
        $this->thebestest = (strpos($message, '#thebestest') !== FALSE) ? 'YES' : 'NO';
        $this->xoxo = (strpos($message, '#xoxo') !== FALSE) ? 'YES' : 'NO';
        $this->sofantastic = (strpos($message, '#sofantastic') !== FALSE) ? 'YES' : 'NO';
        $this->thefreshest = (strpos($message, '#thefreshest') !== FALSE) ? 'YES' : 'NO';
        $this->stuckinourheads = (strpos($message, '#stuckinourheads') !== FALSE) ? 'YES' : 'NO';
        $this->youresofancy = (strpos($message, '#youresofancy') !== FALSE) ? 'YES' : 'NO';
        $this->sohappy = (strpos($message, '#sohappy') !== FALSE) ? 'YES' : 'NO';
        $this->thatsmyjam = (strpos($message, '#thatsmyjam') !== FALSE) ? 'YES' : 'NO';
        $this->move = (strpos($message, '#move') !== FALSE) ? 'YES' : 'NO';
        $this->hittheroad = (strpos($message, '#hittheroad') !== FALSE) ? 'YES' : 'NO';
        $this->musicalmashup = (strpos($message, '#musicalmashup') !== FALSE) ? 'YES' : 'NO';
        $this->cool = (strpos($message, '#cool') !== FALSE) ? 'YES' : 'NO';
        $this->a_5sos = (strpos($message, '@5sos') !== FALSE) ? 'YES' : 'NO';
        $this->a_arianagrande = (strpos($message, '@arianagrande') !== FALSE) ? 'YES' : 'NO';
        $this->a_taylorswift13 = (strpos($message, '@taylorswift13') !== FALSE) ? 'YES' : 'NO';
        $this->h_5secondsofsummer = (strpos($message, '#5secondsofsummer') !== FALSE) ? 'YES' : 'NO';
        $this->h_arianagrande = (strpos($message, '#arianagrande') !== FALSE) ? 'YES' : 'NO';
        $this->h_beamiller = (strpos($message, '#beamiller') !== FALSE) ? 'YES' : 'NO';
        $this->h_beckyg = (strpos($message, '#beckyg') !== FALSE) ? 'YES' : 'NO';
        $this->h_charlixcx = (strpos($message, '#charlixcx') !== FALSE) ? 'YES' : 'NO';
        $this->h_demilovato = (strpos($message, '#demilovato') !== FALSE) ? 'YES' : 'NO';
        $this->h_directioners = (strpos($message, '#directioners') !== FALSE) ? 'YES' : 'NO';
        $this->h_echosmith = (strpos($message, '#echosmith') !== FALSE) ? 'YES' : 'NO';
        $this->h_edenxo = (strpos($message, '#edenxo') !== FALSE) ? 'YES' : 'NO';
        $this->h_edsheeran = (strpos($message, '#edsheeran') !== FALSE) ? 'YES' : 'NO';
        $this->h_fifthharmony = (strpos($message, '#fifthharmony') !== FALSE) ? 'YES' : 'NO';
        $this->h_jacoblatimore = (strpos($message, '#jacoblatimore') !== FALSE) ? 'YES' : 'NO';
        $this->h_harmonizers = (strpos($message, '#harmonizers') !== FALSE) ? 'YES' : 'NO';
        $this->h_maroon5 = (strpos($message, '#maroon5') !== FALSE) ? 'YES' : 'NO';
        $this->h_meghantrainor = (strpos($message, '#meghantrainor') !== FALSE) ? 'YES' : 'NO';
        $this->h_mkto = (strpos($message, '#mkto') !== FALSE) ? 'YES' : 'NO';
        $this->h_nickjonas = (strpos($message, '#nickjonas') !== FALSE) ? 'YES' : 'NO';
        $this->h_ollymurs = (strpos($message, '#ollymurs') !== FALSE) ? 'YES' : 'NO';
        $this->h_onedirection = (strpos($message, '#onedirection') !== FALSE) ? 'YES' : 'NO';
        $this->h_pharrellwilliams = (strpos($message, '#pharrellwilliams') !== FALSE) ? 'YES' : 'NO';
        $this->h_r5 = (strpos($message, '#r5') !== FALSE) ? 'YES' : 'NO';
        $this->h_r5family = (strpos($message, '#r5family') !== FALSE) ? 'YES' : 'NO';
        $this->h_sabrinacarpenter = (strpos($message, '#sabrinacarpenter') !== FALSE) ? 'YES' : 'NO';
        $this->h_selenagomez = (strpos($message, '#selenagomez') !== FALSE) ? 'YES' : 'NO';
        $this->h_shawnmendes = (strpos($message, '#shawnmendes') !== FALSE) ? 'YES' : 'NO';
        $this->h_sheppard = (strpos($message, '#sheppard') !== FALSE) ? 'YES' : 'NO';
        $this->h_taylorswift = (strpos($message, '#taylorswift') !== FALSE) ? 'YES' : 'NO';
        $this->h_thevamps = (strpos($message, '#thevamps') !== FALSE) ? 'YES' : 'NO';
        $this->h_zendaya = (strpos($message, '#zendaya') !== FALSE) ? 'YES' : 'NO';
	}

	// Determine if an image is present.
	public function hasImage() {
		// Return immediate if not media present.
		if (!$this->media_url) return FALSE;

		// Check the extension.
		$path = parse_url($this->media_url, PHP_URL_PATH);
		if (strtolower(substr($this->media_url, -3)) == 'jpg') return TRUE;
		if (strtolower(substr($this->media_url, -3)) == 'png') return TRUE;
		if (strtolower(substr($this->media_url, -3)) == 'gif') return TRUE;
		if (strtolower(substr($this->media_url, -4)) == 'jpeg') return TRUE;
		return FALSE;
	}

    // Make strings safe for storing in MySQL versions that don't properly handle UTF-8.
	protected function replace4ByteCharacters($string)
	{
		return preg_replace('%(?:'
			.'\xF0[\x90-\xBF][\x80-\xBF]{2}'	// planes 1-3
			.'| [\xF1-\xF3][\x80-\xBF]{3}'		// planes 4-15
			.'| \xF4[\x80-\x8F][\x80-\xBF]{2}'	// plane 16
			.')%xs', '?', $string);
	}

  // Determine if a tweet is a retweet or not.
  protected function determine_if_retweet($tweet)
  {
      return (
          $tweet->in_reply_to_status_id OR 
          strtoupper(substr($tweet->text, 0, 3)) == 'RT '
      );
  }

  // Is this a retweet?
  public function isRetweet() {
    return ($this->retweet == 'YES');
  }
}
