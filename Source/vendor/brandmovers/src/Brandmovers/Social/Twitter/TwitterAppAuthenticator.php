<?php namespace Brandmovers\Twitter;

class TwitterAppAuthenticator
{
	
	protected $consumerKey;
	protected $consumerSecret;

	// Constructor
	public function __construct($config)
	{
		$this->consumerKey = $config['consumer_key'];
		$this->consumerSecret = $config['consumer_secret'];
	}

	// This method returns an auth token.
	public function authToken()
	{
		// Attempt to get from the database.
		$authToken = TwitterAuthToken::orderBy('created_at', 'DESC')->first();

		if ($authToken != NULL) {
			return $authToken;
		}

		// Request an authentication token from Twitter.
		return $this->requestAuthToken();
	}

	// Request a new application token from Twitter.
	private function requestAuthToken()
	{
		// Construct the bearer token credentials.
		$tokenCredentials = base64_encode(urlencode($this->consumerKey) . ':' . urlencode($this->consumerSecret));

		// Construct request.
		$curl = curl_init();

		// Build the request headers.
		$requestHeaders = array(
			'Authorization: Basic ' . $tokenCredentials,
			'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
		);

		// Build the cURL options.
		// Note, CURLOPT_POST must be set before other headers or it will
		// break any headers set before it.
		$curlOptions = array(
			CURLOPT_URL => 'https://api.twitter.com/oauth2/token',
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
			CURLOPT_HTTPHEADER => $requestHeaders,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false
		);
		curl_setopt_array($curl, $curlOptions);

		$response = curl_exec($curl);
    	curl_close($curl);

    	// Evaluate the response.
    	$responseObject = @json_decode($response);
		
    	if ( ! isset($responseObject->access_token))
    	{
    		return NULL;
    	}
    	
    	$authToken = new TwitterAuthToken();
    	$authToken->token = $responseObject->access_token;
    	$authToken->save();
		return $authToken;
	}

	// This method invalidates an application token.
	public function invalidateAuthToken($token)
	{
		// Construct the bearer token credentials.
		$credentials = base64_encode(urlencode($this->consumerKey) . ':' . urlencode($this->consumerSecret));

		// Get the token value.
		$tokenValue = $token->token;

		// Remove token from the persistent storage.
		$token->delete();

		// Construct request.
		$curl = curl_init();

		// Build the request headers.
		$headers = array(
			'Authorization: Basic ' . $credentials,
			'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
		);

		// Build the cURL options.
		// Note, CURLOPT_POST must be set before other headers or it will
		// break any headers set before it.
		$options = array(
			CURLOPT_URL => 'https://api.twitter.com/oauth2/invalidate_token',
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => 'access_token='.$tokenValue,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false
		);
		curl_setopt_array($curl, $options);

		$response = curl_exec($curl);

    	curl_close($curl);
	}

}

