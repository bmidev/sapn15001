<?php

namespace Brandmovers\Social\Instagram;

use Illuminate\Support\Facades\Config;
use Brandmovers\Social\Instagram\InstagramRequestLog;
use Instagram\Instagram;
use Brandmovers\Social\Post;

class InstagramImporter {

	public function import() {

		// Create a new request log
		$lastId = InstagramRequestLog::getLastId();

		// Run initial import
		$results = $this->getMedia($lastId, null);

		if ($lastId) {
			// Check if we need query again for more posts (page through results until we get to our last import)
			while ($results && $results->getNextMaxTagId()) {
				// Get the next page of data (still above the original min, but after the last page)
				$results = $this->getMedia($lastId, $results->getNextMaxTagId());
			}
		}
	}

	public function getMedia($minId, $maxId) {

		echo "\n";
		echo "---------------------------------------------------------------\n";
		echo "Starting API Request\n";
		echo "---------------------------------------------------------------\n";

		// Create new Request Log (sets created_at and status of RUNNING as defaults)
		$requestLog = new InstagramRequestLog();


		// Setup Instagram Client
		$instagram = new Instagram();
		$instagram->setClientID(Config::get('instagram.client_id'));
		$tag = $instagram->getTag(Config::get('instagram.tag'));


		// Get Media from Instagram Tag within the ID ranges
		$media = $tag->getMedia([
			'min_tag_id' => $minId,
			'max_tag_id' => $maxId
		]);


		// Record number of records found
		$requestLog->records = count($media);


		// Check for results		
		if (count($media) == 0) {
			echo "No new media found, saving log and exiting\n\n";
			$requestLog->status = InstagramRequestLog::STATUS_COMPLETE;
			$requestLog->save();
			return;
		}


		//dd($media);
		// Save each post to the database
		foreach ($media as $gram) {

			$idStr = $gram->id;
			$postId = explode('_', $gram->id)['0'];
			// Sometimes the instagram API reports the caption as null, therefor there is no text.
			// Set it here to an empty string to avoid errors below
			if (!isset($gram->caption->text)) {
				$comments = "";
			} else {
				$comments = utf8_encode($gram->caption->text);
			}

			$video_url = '';
			$videos = $gram->videos;
			if (isset($videos)) {
				if (isset($videos->standard_resolution->url)) {
					$video_url = $videos->standard_resolution->url;
				} else if (isset($videos->low_resolution->url)) {
					$video_url = $videos->low_resolution->url;
				}
			}
			$post = new Post([
				'network' => Post::NETWORK_INSTAGRAM,
				'network_id' => $postId,
				'network_id_str' => $idStr,
				'post_date' => date('Y-m-d H:i:s', $gram->created_time),
				'url' => $gram->link,
				'image' => $gram->images->standard_resolution->url,
				'image_thumb' => $gram->images->thumbnail->url,
				'video_url' => $video_url,
				'comments' => $comments,
				'author_name' => $gram->user->full_name,
				'author_image' => $gram->user->profile_picture,
				'author_username' => $gram->user->username,
				'author_profile' => "http://instagram.com/" . urlencode($gram->user->username)
			]);

			// Validate this post
			$post->validate();

			try {
				$post->save();
				echo "Saved post $gram->id to DB\n";
			} catch (\Exception $e) {
				// Catch duplicate entry constraint and ignore
				if ($e->errorInfo[0] == "23000") {
					echo "Error inserting duplicate with gram $gram->id\n";
				} else {
					// Mark import as errored
					throw $e;
				}
			}
		}


		echo "*** COMPLETE ***\n";
		echo "Imported " . count($media) . " records\n\n";


		$minTagId = $media->getMinTagId();
		$maxTagId = $media->getNextMaxTagId();

		// Complete the Request Log
		$requestLog->status = InstagramRequestLog::STATUS_COMPLETE;
		$requestLog->min_tag_id = !empty($minTagId) ? $minTagId : 0;
		$requestLog->max_tag_id = !empty($maxTagId) ? $maxTagId : 0;
		$requestLog->save();

		return $media;
	}

}
