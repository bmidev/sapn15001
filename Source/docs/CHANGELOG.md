# Change Log

## 0.5.1

* Added `MY_Form_validation.php` with Brandmovers proprietary validation rules and english/spanish language files

## 0.5.0

* Added support for [Composer](http://getcomposer.org)
* Added support for [PHPDotEnv](https://github.com/vlucas/phpdotenv)
* Added an entities loader compatibility with Modular Extensions
* Added `cache.php` and `memcached.php` config files
* Added an `application/entities` directory
* Added a `deploy.sh` script
* Removed the `tools.php` controller (moved to the `ci/tools` module installable via Composer)
* Applied `realpath()` to the system and application directory paths so that they are absolute
* Set the log and cache directories to absolute paths
* Set the PHP `error_log` to `application/logs/php-YYYY-MM-DD.log`
* Set the `database.php` config file to use getenv()
* Set the `index_page` config setting to blank
* Improved documentation (CHANGELOG.md, THIRDPARTY.md, DEPLOY.md, INSTALL.md)
* Moved all documentation into a `docs` directory

## 0.4.1

* Updated to CodeIgniter 2.1.2
* Updated [HMVC Extension](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/wiki/Home)
  to revision f972fec54f21
* Updated [Phil Sturgeon's Template Library](https://github.com/philsturgeon/codeigniter-template)
  to revision 78525c19df0023908a1e560291e443b2cc5482cf

## 0.4.0

* Added migration tool
* Added file deployment tool
* Added data directories to application and htdocs for storing user uploaded files

## 0.3.1

* Removed the Sparks tool kit

## 0.3.0

* Implement Template system into base CI
* Include Sparks into base install
* Include FirePHP

## 0.2.0

* Environment settable using Apache and Shell environment variables
* Added HMVC extension. This provides support for module separation as well as hierarchical support

## 0.1.0

* Imported CodeIgniter 2.1.0
* Disabled CodeIgniter's error handler
* Set database setting pconnect to FALSE
