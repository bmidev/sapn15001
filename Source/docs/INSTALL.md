BMI CodeIgniter Framework
=========================

CodeIgniter Base for building projects from.

Install this package into the `Source/` directory at your repository root.

Installation
------------

[Composer](http://getcomposer.org/) is the recommended way to install the base
framework and any modules needed into a new (blank) project.

To get started, create a `composer.json` file in your project root directory with
the following contents and run `composer install`:

```
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://gitlab.brandmovers.net/packages"
        }
    ],
    "require": {
        "ci/base": "*",
        "ci/tools": "*",
        "ci/dashboard": "*",
        "vlucas/phpdotenv": "*"
    },
    "minimum-stability": "dev",
    "extra": {
        "installer-paths": {
            "./Source": ["ci/base"],
            "./Source/application/third_party": ["vlucas/phpdotenv"]
        },
        "codeigniter-application-dir": "Source/application",
        "codeigniter-sparks-dir": "Source/sparks"
    }
}
```

**IMPORTANT:** After Composer finishes downloading all the dependencies that you specified
for the project, you need to remove all the `.git` directories or Git will see them as
submodules and cause problems when you try to commit.

```
rm -Rf `find vendor -type d -name .git`
rm -Rf `find Source -type d -name .git`
```

