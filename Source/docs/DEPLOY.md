# Deploy Instructions

To deploy, run the following:

```
cd Source/
./deploy.sh user@hostname
```

Replace `user` and `hostname` with the actual username and server hostname you wish to deploy to.
