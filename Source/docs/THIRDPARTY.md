# Third-Party components included

* [FirePHP 0.3.2](http://www.firephp.org/)
* [Modular Extensions](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc)
* [Phil Sturgeon's Template Library](https://github.com/philsturgeon/codeigniter-template)
