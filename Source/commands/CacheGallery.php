<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Aws\Common\Enum\Region;
use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Brandmovers\Extension\SourceUploader;

class CacheGallery extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gallery:cache';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This will generate and save a cached version of the first page of the gallery. This is then saved to S3 for quick retrieval.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// Submissions per page.
		$limitPerPage = 9;

		// Get the first 9 submissions.
		$where = 'video_status = ? ORDER BY updated_at DESC LIMIT ? OFFSET ?;';
        $whereValues = array('APPROVED', ($limitPerPage + 1), 0);
        $data = Registration::whereRaw($where, $whereValues)->get();
		
		// Get the number of results returned.
        $count = count($data);

        // Iterate and build output.
        $gallery = array();
        $counter = 0;
        foreach ($data as $item) {
            $counter++;
            if ($counter <= $limitPerPage) {
                $gallery[] = (object)array(
                    'id' => $item->id,
                    'created_at' => (String)$item->created_at,
                    'first_name' => (String)$item->first_name,
                    'last_name' => (String)$item->last_name,
                    'conference' => (String)$item->conference,
                    'school' => (String)$item->school,
                    'video_name' => (String)$item->video_name,
                    'video_url' => (String)$item->video_url,
                    'video_source' => (String)$item->video_source,
                    'video_thumb_url' => (String)$item->video_thumb_url,
                    'vote' => Vote::whereRaw('registration_id = ? AND status >= ?', array($item->id, 'VALIDATED'))->count(),
                );
            }
        }

        // Generate the temporary file.
        $tempFileName = tempnam(sys_get_temp_dir(), 'gallery-list-');

        // Write the results as a JSON temporary file.
        file_put_contents($tempFileName, json_encode((object)array(
            'gallery' => $gallery,
            'next_page' => ($count > $limitPerPage) ? 2 : "",
            'previous_page' => "",
        )));
        
        // Generate the destination URI
        $mediaBucket = Config::get('aws.media-bucket');
        $destinationUri = strtolower(App::environment().'/gallery-list.json');

		// Upload the file to S3.
        $uploader = new SourceUploader( Config::get('aws.access_key'), Config::get('aws.secret_key') );
        $uploader->uploadWithContentType($tempFileName, $mediaBucket, $destinationUri, 'application/json');

        // Remove the temporary file.
        @unlink($tempFileName);

        $this->info('Complete');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// No arguments.
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// No options.
		);
	}

}
