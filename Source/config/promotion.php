<?php
return array(
    // The Brandmovers Project Id
    'projectId' => 'sapn15001',
	// The Brandmovers Project Id
    'projectName' => 'Nutella ',
    // The address that emails to users will originate from.
    'mailFromAddress' => 'promotions@brandmovers.net',
    // The name of the account that emails to users will originate from.
    'mailFromName' => 'Promotion Administrator',
    // The email of the Brandmovers developer managing the project.
    'mailToDeveloper' => 'ricky@brandmovers.co.uk',
    // The threshold that will trigger fraudulent rejects. Don't change if you don't know what this does.
    'threatThreshold' => 1000,
	// A list of valid states the promotion is valid in.
	'valid_states' => array(
		'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL',
		'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME',
		'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH',
		'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI',
		'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI',
		'WY',
	),
	// A list of valid states the promotion is valid in.
	'switchover_dates' => array(
		'launch' => '2015-09-21',
		'entry_ends' => '2015-10-11 23:59:59',
		'voting_starts' => '2015-11-16 09:00:00',
		'voting_ends' => '2015-11-29 17:00:00',
		'winner_announced' => '2016-01-15 09:00:00',
	),
	'version' => 2,
	/*
        |--------------------------------------------------------------------------
        | Application KeyCDN domains
        |--------------------------------------------------------------------------
        |
        | Specify different domains for your assets.
        |
        */
        'cdn' => array(
        	"local" => "https://bm-projects-public.s3.amazonaws.com/sapn15001/development/static-assets",
			"development" => "https://bm-projects-public.s3.amazonaws.com/sapn15001/development/static-assets",
			"staging" => "",
			"testing" => "",
			"production" => ""
        ),

);
