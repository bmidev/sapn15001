I) Play Instawin
	1a. user is NOT already signed in - show sign-in registration screen with signin link
	2	if user is NOT already registered, register him and take to thank you post registration screen
	3	if user is already registered, they will click on the signin link and be taken to the signin screen
	4	post sign-in, the user is taken to the instawin screen
	5		if user has exhausted max instawin plays for the day, we show a message modal telling them they have exhausted their instawin plays for the day
	6		else we allow them to choose from one of the treasure boxes
			
	7		if user is not an instawin winner, we show them the sorry modal
			
	8		if user is an instawin winner, we take them to the enter additional details screen with fisrt_name, last_name, email and other available details 	 prefilled.
	9		Post this, we take them to the thankyou screen for additional details capture
			
	1b.  user is already signed in, we follow 5,6,7,8,9 directly without showing the signin screen
	


II) Play Game
	10 if user finds all items in the given time, 
	11	if user is NOT already signed-in into our session - we show them the maze complete message with link saying "register to win a prize". (this register button takes to instawin itself i.e step I)
	12	if user is already signed-in into our session - we show them the maze complete message
	13		if user has exhausted max instawin game plays for the day, we do not show the instawin button
	14		else we show the link to play instawin
		
	15	if user does not find all items, we show the "play game again" and "play instawin" options. Play instawin will go to step I.