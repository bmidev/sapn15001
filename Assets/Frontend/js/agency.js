/*!
 * Start Bootstrap - Agency Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
		goto($(this).attr('href'));
        event.preventDefault();
    });

	$('.orbrowse').bind('click', function(event) {
		chooseFile();
	});

	/* If they change the desktop mobile settings */
	$('.filterbox').bind('click', function(event) {
		var video_type = '';
		var opt = '';
		var current_video_type = $('#data-video-type').val().split(",");

		if($(this).hasClass('off')){
			$(this).removeClass('off');
			opt = 'add';
			video_type += $(this).attr('data-type');
		}else{
			if(current_video_type.length>1){
				$(this).addClass('off');
				opt = 'rm';
			}
		}

		if(opt.length>0){
			for(var i = 0; i < current_video_type.length; i++){
				if(current_video_type[i] != $(this).attr('data-type')){
					if(video_type.length>0){ video_type += ','; }
					video_type += current_video_type[i];
				}
			}

			$('#data-video-type').val(video_type);
			load_data("dummy/more_videos.json",{ videolist: $('#morebtn').attr('data-video-list'), video_type: $('#data-video-type').val(), platform: 'desktop' }, reload_videos);
		}
	});

	/* If they change the mobile filter settings */
	$('#selectfilter').bind('change', function(event) {
		var video_type = '';
		var opt = '';
		var current_video_type = $('#data-video-type').val().split(",");
		var video_type = $('#selectfilter option:selected').val().length > 0 ? $('#selectfilter option:selected').val() : 'Friend,Artist,Adventurer,Gourmet,Purist';
		$('#data-video-type').val(video_type);

		// Now do a foreach and change the 
		load_data("dummy/more_videos.json",{ videolist: $('#morebtn').attr('data-video-list'), video_type: $('#data-video-type').val(), platform: 'mobile' }, reload_videos);
	});

	/* Set up custom checkboxes */
	$('input.radio').iCheck({
		radioClass: 'iradio_square'
	});

	/* Set up Accordian */
	$('.mobilebar').bind('click', function(event) {
		if($(window).width()<=768){
			if($('.accord'+$(this).attr('data-cell')).css('display')=='block'){
				$(this).children('span').html('+');
				$(this).parent().children('ul').slideUp();
			}else{
				$(this).children('span').html('-');
				$(this).parent().children('ul').slideDown();
			}
		}
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

// Styles select fields
$('.selectpicker').selectpicker();

// Vaildiate dob form
$(".submitbtn").click(function() {
	$("#dob_form select").not("[type=submit]").jqBootstrapValidation();
	if($('#enterform').css('display') == 'block'){return;}
	if($("#dob_mm option:selected").val().length==0){alert('error1');$("#dob_mm option:selected").addClass('err');return;}
	if($("#dob_dd option:selected").val().length==0){alert('error2');$("#dob_dd option:selected").addClass('err');return;}
	if($("#dob_yyyy option:selected").val().length==0){alert('error3');$("#dob_yyyy option:selected").addClass('err');return;}

	loading(true);
	$.getJSON( "dummy/dob_success.json", { dob_mm: $("#dob_form #dob_mm:selected").val(), dob_dd: $("#dob_form #dob_dd:selected").val(), dob_yyyy: $("#dob_form #dob_yyyy:selected").val() } )
	  .done(function( json ) {
		if(json.success === true){
			loading(false);
			$('#enterform').slideDown();
			//$('#enter').css('background-image','url(img/enter-arrows-open.png)').css('background-position','center center');
			$('#enter').addClass('down');
			goto('#enterform');
		}
	  })
	  .fail(function( jqxhr, textStatus, error ) {
		loading(false);
		var err = textStatus + ", " + error;
		alert( "Request Failed: " + err );
	});
});

// Vaildiate dob form
$(".morebtn").click(function() {			   
	load_data("dummy/more_videos.json",{ videolist: $(this).attr('data-video-list'), video_type: $('#data-video-type').val() }, load_more_videos);
});

// Gets content from the server{
function load_data(send_to,data_to_send,callback){
	loading(true);
	$.getJSON( send_to, data_to_send )
	  .done(function( json ) {
		if(json.success === true){
			loading(false);
			callback (json);
		}
	  })
	  .fail(function( jqxhr, textStatus, error ) {
		loading(false);
		var err = textStatus + ", " + error;
		alert( "Request Failed: " + err );
	});
}

// Callback for loading more videos
function load_more_videos(json){
	$(this).attr('data-video-list',json.videolist);
	for(var i=0; i<json.videos.length; i++){
		$( '<div class="col-xs-6 col-md-4 col-lg-5ths video"><img src="img/placeholders/'+json.videos[i]['video']+'" class="img-responsive" alt=""></div>' ).appendTo( ".gallery" );
	}
	if(json.more === false){
		$(".morebtn").fadeOut();
	}
}

// Callback for reloading video
function reload_videos(json){
	$(this).attr('data-video-list',json.videolist);
	var html = '';
	for(var i=0; i<json.videos.length; i++){
		html += '<div class="col-xs-6 col-md-4 col-lg-5ths video"><img src="img/placeholders/'+json.videos[i]['video']+'" class="img-responsive" alt=""></div>';
	}
	$(".gallery").html(html);
	if(json.more === false){
		$(".morebtn").fadeOut();
	}
}

// Resizes the menu to be the height of the current window view
function getVisible() {
	if($(window).width()<=990){
		var footer_height = $('footer').css('position') == 'fixed' ? $('footer').height() : 0; //Works if footer is fixed or not.
		$('.navbar-collapse').css('max-height',$(window).height()-$('.navbar').height()-footer_height);
	}
}

// Resets the accordian if greater than 
function resetAccordian(){
	if($(window).width()>768){
		$( "a.mobilebar" ).each(function( index ) {
			$(this).children('span').html('+');
			$(this).parent().children('ul').removeAttr('style');
		});
	}
}

/* scrolls the page to an element on the page */
function goto(id){
	var offset = $('.navbar').height();
	$('html, body').stop().animate({
		scrollTop: $(id).offset().top-offset
	}, 1500, 'easeInOutExpo');
}

/* Triggers a loading modal */
function loading(show){
	if(show == true){
		// Show loading screen
	}else{
		// Remove loading screen
	}
}

/* Triggers a standard image upload */
function chooseFile() {
  $("#fileInput").click();
}

/** opens a modal popup **/
function show_popup(div,closable){
	jQuery(function($) {
		if(div == undefined || div.length==0){ div='terms'; }
		if(typeof closable  === 'undefined' || closable.length==0 || closable != false){ closable = true; }

		var bg_div_id = closable === false ? 'fade2' : 'fade';
		if(closable === false && $('#'+div).find('a.close').length > 0){
			$('#'+div).find('a.close').remove();
		}else if(closable === true && $('#'+div).find('a.close').length == 0){
			$('#'+div).prepend('<a href="javascript:;" alt="Close" title="Close" class="close">X</a>');
		}
		
		$('#'+div).fadeIn();
	
		//Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
		var popMargLeft = (($( window ).width() - $('#'+div).width()) / 2) - 27;
	
		//Apply Margin to Popup
		$('#'+div).css({
			'left' : popMargLeft
		});
	
		//Fade in Background
		if ($("#"+bg_div_id).length == 0){
			//$('body').append('<div id="fade" style="z-index:200;"></div>');
			$('body').append('<div id="'+bg_div_id+'" style="z-index:99999;"></div>'); 
			$('#'+bg_div_id).css({'filter' : 'alpha(opacity=80)'}).fadeIn();
		}
	
		//Close Popups and Fade Layer event handle
		//$('a.close, #fade').on('mouseup', function() { 
		if(closable === true){
			$('a.close, button.closeNo, #fade').mouseup(function() {
				close_modal();
				return false;
			});
	
			$('a.closeYes').mouseup(function() {
				close_modal();
				var delay = 500;
				setTimeout(function(){ delay = 0; }, 500);
				$.post( base_url, { delinebtn: "true", jquery: "true" }).done(function( data ) {
					//alert( "Data Loaded: " + data );
					var obj = jQuery.parseJSON(data);
					if(obj.err){
						$('.errmsg').html(obj.err);
						setTimeout(function(){ show_popup('fatalerror'); }, delay);
					}else{
						setTimeout(function(){ show_popup('decline_success',false); }, delay);
					}
				}).fail(function(data) {
					//alert( "Due to a server issue we could not tell the service provider you declined the contract, please try again." );
					$('.errmsg').html('We could not process your request. Please try again');
					setTimeout(function(){ show_popup('fatalerror'); }, delay);
				});
				return false;
			});
		}

		$( window ).resize(function() {
			var popMargLeft = (($( window ).width() - $('#'+div).width()) / 2) - 27;
			//console.log(popMargLeft+' = '+(($( window ).width() - $('#'+div).width()) / 2)+' ('+$( window ).width()+' - '+$('#'+div).width()+') divide 2 - 27');
		
			//Apply Margin to Popup
			$('#'+div).css({
				'left' : popMargLeft
			});
		});
	});
	return false;
}

/** closes a modal popup **/
function close_modal(){
	jQuery(function($) {
		$('#fade, .modal_cont').fadeOut(function() {
			$('#fade').remove(); 
		});
	});
}


getVisible();
$( window ).resize(function() {
	getVisible();
	resetAccordian();
});